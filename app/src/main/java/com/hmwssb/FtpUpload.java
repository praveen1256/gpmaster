package com.hmwssb;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.hmwssb.utility.Utility;

public class FtpUpload extends Activity implements OnClickListener,LocationListener {


	private static Location loc;
	FTPClient client;
	static File   mediaStorageDir;
	/*********  work only for Dedicated IP ***********/
	static final String FTP_HOST= "202.65.157.176";

	/*********  FTP USERNAME ***********/
	static final String FTP_USER = "sunil";

	/*********  FTP PASSWORD ***********/
	static final String FTP_PASS  ="vajra@123";

	// Activity request codes
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";

	private Uri fileUri; // file url to store image/video

	static File mediaFile;
	Button btn,upload;
	Bitmap bitmp;
	public static String GBCCode;
	ImageView captureSnap;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verifymain);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		btn = (Button) findViewById(R.id.button1);
		upload = (Button) findViewById(R.id.upload);
		captureSnap =(ImageView) findViewById(R.id.captureSnap);

		Bundle bu = getIntent().getExtras();
		GBCCode = bu.getString("GBCCode");

		upload.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				uploadFile(mediaFile);
			}
		});
		btn.setOnClickListener(this);

	}   

	public void onClick(View v) {

		/********** Pick file from sdcard *******/

		captureImage();
		// Upload sdcard file


	}
	//camera open 

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
		if (resultCode == 0)  
			return;  
		if (requestCode == 100) {   
			Uri imgUri=Uri.fromFile(mediaFile);
			captureSnap.setImageURI(imgUri);  
		}  
	}
	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
//		return Uri.fromFile(getOutputMediaFile(type));
		if (Utility.isTargetSdkVersionGreaterThanNaugat()) {
			return FileProvider.getUriForFile(
					this,
					getApplicationContext()
							.getPackageName() + ".provider", getOutputMediaFile(type));
		} else {
			return Uri.fromFile(getOutputMediaFile(type));
		}
	}



	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}


		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ GBCCode + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}

	public void uploadFile(File fileName){

		Thread t = new Thread(new Runnable() {

			public void run() {
				try {
					client = new FTPClient();
					client.connect(FTP_HOST,21);
					client.login(FTP_USER, FTP_PASS);
					client.setType(FTPClient.TYPE_BINARY);
					client.changeDirectory("/");

					client.upload(mediaFile, new MyTransferListener());

				} catch (Exception e) {
					e.printStackTrace();
					try {
						client.disconnect(true);	
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
		});
		t.start();




	}

	/*******  Used to file upload and show progress  **********/

	public class MyTransferListener implements FTPDataTransferListener {

		public void started() {

			btn.setVisibility(View.GONE);
			// Transfer started
			// 		Toast.makeText(getBaseContext(), " Upload Started ...", Toast.LENGTH_SHORT).show();
			System.out.println(" Upload Started ...");
		}

		public void transferred(int length) {

			// Yet other length bytes has been transferred since the last time this
			// method was called
			//    		Toast.makeText(getBaseContext(), " transferred ..." + length, Toast.LENGTH_SHORT).show();
			System.out.println(" transferred ..." + length);
		}

		public void completed() {

			runOnUiThread(new Runnable() {
				public void run() {
					btn.setVisibility(View.VISIBLE);

				}
			});
			// Transfer completed

			//    		Toast.makeText(getBaseContext(), " completed ...", Toast.LENGTH_SHORT).show();
			System.out.println(" completed ..." );
		}

		public void aborted() {

			runOnUiThread(new Runnable() {
				public void run() {
					btn.setVisibility(View.VISIBLE);

				}
			});
			// Transfer aborted
			//    		Toast.makeText(getBaseContext()," transfer aborted , please try again...", Toast.LENGTH_SHORT).show();
			System.out.println(" aborted ..." );
		}

		public void failed() {
			runOnUiThread(new Runnable() {
				public void run() {
					btn.setVisibility(View.VISIBLE);

				}
			});
			// Transfer failed
			System.out.println(" failed ..." );
		}

	}

	public void onLocationChanged(Location location) {
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	public void onProviderEnabled(String provider) {

	}

	public void onProviderDisabled(String provider) {

	}


}