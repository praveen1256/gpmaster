package com.hmwssb;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;

import org.ksoap2.serialization.SoapObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NewConnection extends SuperActivity implements OnClickListener, AppConstants {


    private Button mBulidingphotoBtn, mDisconnectionDetailsBut, mContactBtn, mBoaringDetailsBtn, mMeterDetailsBtn, Submit_Gb_RemarksBtn, SubmissionBtn, mCanGeneration, mSelfConnectionbtn;
    private EditText RemarksET;
    public TextView cFilenumber, cAddress, cName;
    String filenumber, strAddress, strName, strSanctionedPipeSize, mobilenum;

    private BoringConnection bConn;
    StringBuilder ConnInfo = new StringBuilder();
    StringBuilder ConnInfo2 = new StringBuilder();
    private String ConnXml, selfconn;
    //
    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    private Uri fileUri; // file url to store image/video
    private GPSTracker gps;

    protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
    protected static final int CAMERA_REQUEST = 0;
    static File mediaStorageDir;
    Button b1;
    static File mediaFile;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    static ImageView iv, decodeIv;
    private static File DatePath;
    //	public TextView dateTv;
    private String date;
    private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";

    public Boolean Flag = true;
    private String longBoringdegree;
    private String longBoringminute;
    private String longBoringsecond;
    private String latBoringDegree;
    public byte[] byteArray;
    private byte[] handlePhoto;
    private String photo_Encoded;
    private String latgBoringMinute;
    private String latgBoringSecond;
    private String longBoringMilliSecond;
    private String latBoringMilliSecond;
    private Transactions mTransObj;
    public ImageView signout, home;

    int CONNECTION_TYPE = AppConstants.BORING_CONNECTION;
    Button mCustomerFeedbackDetails, mCustomerBuildingDetails;
    public boolean selfFlag;
    private String mSelfConnStatus;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.newconnection);
        //mTransObj = new Transactions(this);
        Typeface mCustomFont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Roboto-Regular_1.ttf");

        mTransObj = new Transactions();
        try {
            mTransObj.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mBulidingphotoBtn = (Button) findViewById(R.id.boaring_point_photo);
        mDisconnectionDetailsBut = (Button) findViewById(R.id.disconnection_boring_details);
        mDisconnectionDetailsBut.setTypeface(mCustomFont);
        mContactBtn = (Button) findViewById(R.id.contact_number);
        mBoaringDetailsBtn = (Button) findViewById(R.id.customer_boring_details);
        mMeterDetailsBtn = (Button) findViewById(R.id.meter_details_btn);
        Submit_Gb_RemarksBtn = (Button) findViewById(R.id.submit_gb_remarks);
        SubmissionBtn = (Button) findViewById(R.id.final_submit_btn);
        mCanGeneration = (Button) findViewById(R.id.new_can_generate);
        RemarksET = (EditText) findViewById(R.id.gb_remarks_capture);
        cFilenumber = (TextView) findViewById(R.id.customer_file_number);
        cAddress = (TextView) findViewById(R.id.customer_address);
        cName = (TextView) findViewById(R.id.customer_name);
        mSelfConnectionbtn = (Button) findViewById(R.id.btn_selfconnection);
        mCustomerFeedbackDetails = (Button) findViewById(R.id.customer_feedback_details);
        mCustomerBuildingDetails = (Button) findViewById(R.id.customer_building_details);
        home = (ImageView) findViewById(R.id.home);
        signout = (ImageView) findViewById(R.id.signout);

        mCustomerFeedbackDetails.setOnClickListener(this);
        mCustomerBuildingDetails.setOnClickListener(this);

        mBulidingphotoBtn.setOnClickListener(this);
        mDisconnectionDetailsBut.setOnClickListener(this);
        mContactBtn.setOnClickListener(this);
        mBoaringDetailsBtn.setOnClickListener(this);
        mMeterDetailsBtn.setOnClickListener(this);
        mCanGeneration.setOnClickListener(this);
        Submit_Gb_RemarksBtn.setOnClickListener(this);
        SubmissionBtn.setOnClickListener(this);


        RelativeLayout layout = (RelativeLayout) findViewById(R.id.new_connection);
        LayoutInflater inflater = getLayoutInflater();

        // inflate the header description layout and add it to the linear layout:
        View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
        layout.addView(descriptionLayout);

        // you can set text here too:
        TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
        textView.setText("Enter Boring Details");

        //Disable Button if received

        //		boaringdetailslist = mTransObj.getBoringDetailsInfo(mCustomerFileno);


        home.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                finish();
                startActivity(i);
            }
        });

        signout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //				logoutFromApp();
                finish();
                System.exit(0);
            }
        });

        mSelfConnectionbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewConnection.this);

                // set title
                alertDialogBuilder.setTitle(" Are you sure that you want to change this coonection to self connection to this file");

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                OTPSet.setStatus(1);
                                //SelfConnectionInfo();
                                CheckNetworkStatus(filenumber);
                                AsyncCallisSelfConnection task = new AsyncCallisSelfConnection();

                                task.execute();

                                ContentValues updatependingfile = new ContentValues();
                                updatependingfile.put("GBStatus", "Pending");
                                mTransObj.update_pendingfileconnection(updatependingfile, filenumber);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();


            }
        });
        //		mDisconnection_Details.setVisibility(View.INVISIBLE);


        Intent in = getIntent();
        bConn = (BoringConnection) in.getSerializableExtra(AppConstants.GB_DATA);
        //restartFirstActivity();
        filenumber = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.FILENO, "");
        BoringConnection mCanStatus = mTransObj.getBoringConnection(filenumber);
        if (mCanStatus.isGbAcceptedForCanGeneration()) {
            System.out.println("************************" + mCanStatus.isGbAcceptedForCanGeneration() + "this is can status");
            //			cangeneratebuttonstatus(filenumber);
            ContentValues cangenerate = new ContentValues();
            cangenerate.put("FileNo", filenumber);
            cangenerate.put("CanGenerateCANCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
            cangenerate.put("CanGenerateCANXMLString", filenumber);
            cangenerate.put("CanGenerateCANCreatedDtm", "02011991");
            cangenerate.put("CanGenerateCANWebServiceStatus", "1");
            cangenerate.put("CanGenerateCANServerStatus", "finished");
            mTransObj.insert_cangenerateInfo(cangenerate);
            cangeneratebuttonstatus(filenumber);
            //			Toast.makeText(getApplicationContext(), "Can Generated Successfuly", Toast.LENGTH_SHORT).show();
        }
        System.out.println("************************" + mCanStatus + "this is can status");

        System.out.println("*******filenumber**************filenumber" + filenumber);
        try {
            mSelfConnStatus = mTransObj.GetSelfConnStatus(filenumber);
            if (mSelfConnStatus == null || mSelfConnStatus.contains(" ")) {
                ContentValues PostSelfConnStatusValue = new ContentValues();
                PostSelfConnStatusValue.put("GetGBPendingFilesInfoCreateByUid", "9999");
                mTransObj.update_GbPendingFileInfo(PostSelfConnStatusValue, filenumber);
            }
            mSelfConnStatus = mTransObj.GetSelfConnStatus(filenumber);
            if (mSelfConnStatus.contains("1")) {

            } else {
                ContentValues PostSelfConnStatusValue = new ContentValues();
                PostSelfConnStatusValue.put("GetGBPendingFilesInfoCreateByUid", "9999");
                mTransObj.update_GbPendingFileInfo(PostSelfConnStatusValue, filenumber);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }


        //String filenumber=bConn.getFileNo();
        //****************************** button clickable code *******************************mDisconnectionDetailsBut


		/*mMeterDetailsBtn.setClickable(false);
		mBulidingphotoBtn.setClickable(false);
		mCanGeneration.setClickable(false);
		SubmissionBtn.setClickable(false);
		mCustomerFeedbackDetails.setClickable(false);
		mCustomerBuildingDetails.setClickable(false);*/
        //String remarks=	mTransObj.getPostGBRemarks(bConn.getFileNo());

        /******************************
         *  delete finished files
         */
        //************************************ get status value for get_Status_Disconnection getWebServiceCanGenerateGBBillStatus

        String type = mTransObj.get_Applicationtype(filenumber);
        if (type.contains("N")) {
            System.out.println("******************************get_Applicationtype" + type);
            textView.setText("New Connection Details");
            mBoaringDetailsBtn.setText("1.Boring Details");
            mMeterDetailsBtn.setText("2.Meter Details");
            mBulidingphotoBtn.setText("6.Building Photo");
            mCanGeneration.setText("3.Generate Can");
            SubmissionBtn.setText("6.Generate Bill");
            mCustomerFeedbackDetails.setText("4.Customer Feedback");
            mCustomerBuildingDetails.setText("5.Building Details");

            setButtonClick(mBoaringDetailsBtn, true);
            setButtonClick(mMeterDetailsBtn, false);
            setButtonClick(mBulidingphotoBtn, false);
            setButtonClick(mCanGeneration, false);
            setButtonClick(SubmissionBtn, false);
            setButtonClick(mCustomerFeedbackDetails, false);
            setButtonClick(mCustomerBuildingDetails, false);
            setButtonClick(mSelfConnectionbtn, true);
        } else {
            textView.setText("Enhancement Details");
            mDisconnectionDetailsBut.setText("1.Disconnection Details");
            mBoaringDetailsBtn.setText("2.Boring Details");
            mMeterDetailsBtn.setText("3.Meter Details");
            mBulidingphotoBtn.setText("7.Building Photo");
            mCanGeneration.setText("4.Enhancement Details Can");
            SubmissionBtn.setText("7.Generate Bill");
            mCustomerFeedbackDetails.setText("5.Customer Feedback");
            mCustomerBuildingDetails.setText("6.Building Details");
            setButtonClick(mDisconnectionDetailsBut, true);
            setButtonClick(mBoaringDetailsBtn, false);
            setButtonClick(mMeterDetailsBtn, false);
            setButtonClick(mBulidingphotoBtn, false);
            setButtonClick(mCanGeneration, false);
            setButtonClick(SubmissionBtn, false);
            setButtonClick(mCustomerFeedbackDetails, false);
            setButtonClick(mCustomerBuildingDetails, false);
            setButtonClick(mSelfConnectionbtn, true);

        }


        try {
            String status = mTransObj.get_gbremarks_Status(filenumber);
            System.out.println("******************************get_gbremarks_Status" + status);
            if (status.contains("finished")) {
                String type1 = mTransObj.get_Applicationtype(filenumber).trim();
                if (type1.contains("N")) {
                    System.out.println("******************************Kalyan***********:" + type);
                    textView.setText("New Connection Details");
                } else if (type1.contains("E")) {
                    System.out.println("******************************sudhir***********:" + type);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in get_gbremarks_Status--- " + e);
        }

        //************************************ get status value for get_Status_Disconnection
        try {
            String status = mTransObj.get_Status_Disconnection(filenumber);
            System.out.println("******************************get_Status_Disconnection" + status);
            if (status.contains("finished")) {
                //mBoaringDetailsBtn.setClickable(true);
                mDisconnectionDetailsBut.setBackgroundColor(Color.parseColor("#008000"));
                setButtonClick(mSelfConnectionbtn, false);
                mSelfConnectionbtn.setBackgroundColor(Color.parseColor("#008000"));
                setButtonClick(mBoaringDetailsBtn, true);
                ContentValues updatependingfile = new ContentValues();
                updatependingfile.put("GBStatus", "Pending");
                mTransObj.update_pendingfileconnection(updatependingfile, filenumber);

            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in get_Status_Disconnection--- " + e);
        }


        //************************************ get status value for get_Boaring_Status  get_gbremarks_Status(String FileNo)
        try {
            String status = mTransObj.get_Boaring_Status(filenumber);
            System.out.println("******************************get_Boaring_Status" + status);
            if (status.contains("finished")) {
                String meterstaus = mTransObj.getmeteravailable(filenumber);
                System.out.println("******************************meterstaus" + meterstaus);

                ContentValues updatependingfile = new ContentValues();
                updatependingfile.put("GBStatus", "Pending");
                mTransObj.update_pendingfileconnection(updatependingfile, filenumber);
                //Submit_Gb_RemarksBtn.setVisibility(View.GONE);
                if (meterstaus.contains("true")) {
                    mBoaringDetailsBtn.setBackgroundColor(Color.parseColor("#008000"));
                    setButtonClick(mSelfConnectionbtn, false);
                    mSelfConnectionbtn.setBackgroundColor(Color.parseColor("#008000"));
                    //mMeterDetailsBtn.setClickable(true);
                    setButtonClick(mMeterDetailsBtn, true);
                } else {
					/*mMeterDetailsBtn.setClickable(true);
				mCanGeneration.setClickable(true);*/
                    mBoaringDetailsBtn.setBackgroundColor(Color.parseColor("#008000"));
                    setButtonClick(mMeterDetailsBtn, true);
                    setButtonClick(mCanGeneration, true);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in get_Boaring_Status--- " + e);
        }


        //************************************ get status value for get_PostMeterDetails_Status
        try {
            String status = mTransObj.get_PostMeterDetails_Status(filenumber);
            System.out.println("******************************get_PostMeterDetails_Status" + status);
            if (status.contains("finished")) {
                mMeterDetailsBtn.setBackgroundColor(Color.parseColor("#008000"));
                setButtonClick(mCanGeneration, true);

            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in PostMeterDetails_Status--- " + e);
        }
        //************************************ get status value for get_CanGenerate_Status get_PostCustomerFeedBackDetails_Status

        cangeneratebuttonstatus(filenumber);
        //setButtonClick(mCanGeneration,true);

        //************************************ get status value for get_PostCustomerFeedBackDetails_Status get_PostBuildingDetails_Status
        try {
            String status = mTransObj.get_PostCustomerFeedBackDetails_Status(filenumber);
            System.out.println("**********get_PostCustomerFeedBackDetails_Status********status" + status);
            if (status.contains("finished")) {
                mCustomerFeedbackDetails.setBackgroundColor(Color.parseColor("#008000"));
                setButtonClick(SubmissionBtn, true);
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in get_PostCustomerFeedBackDetails_Status--- " + e);
        }

        //************************************ get status value for  get_PostBuildingDetails_Status
        try {
            String status = mTransObj.get_PostBuildingDetails_Status(filenumber);
            System.out.println("******************************status" + status);
            if (status.contains("finished")) {
                mCustomerBuildingDetails.setBackgroundColor(Color.parseColor("#008000"));
                setButtonClick(SubmissionBtn, true);
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in mCustomerBuildingDetails--- " + e);
        }
        try {
            ArrayList<String> filelist = mTransObj.getfiledetails(filenumber);
            System.out.println("******************************filelist" + filelist.size());
            String mAlloteded = mTransObj.GBAllotmentDate(filenumber);
            String[] mAllotedDate = mAlloteded.split("T", 2);
            for (int i = 0; i < filelist.size(); i++) {
                strAddress = filelist.get(1);
                if (filelist.get(6).contains("true")) {
                    cAddress.setText("Address:" + filelist.get(1) + "\n" + "SanctionedPipeSize:" + filelist.get(5) + "\n" + "Meter Available: Yes" + "\n" + "Allotment Date:" + mAllotedDate[0]);
                } else if (filelist.get(6).contains("false")) {
                    cAddress.setText("Address:" + filelist.get(1) + "\n" + "SanctionedPipeSize:" + filelist.get(5) + "\n" + "Meter Available: No" + "\n" + "Allotment Date:" + mAllotedDate[0]);

                }
                //				cAddress.setText("Address:"+filelist.get(1)+"\n"+"SanctionedPipeSize:"+filelist.get(5)+"\n"+"MeterAvailable:"+filelist.get(6));
                mobilenum = filelist.get(3);
                strSanctionedPipeSize = filelist.get(5);
                mContactBtn.setText(getString(R.string.customer_mobile_no, mobilenum));
                cName.setText("Name:" + filelist.get(4));
                strName = filelist.get(4);

            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in getfiledetails--- " + e);
        }
        //************************************ update webservice status

		/* mCustomerfeedbackStatus=mTransObj.getWebServiceCustomerFeedBackStatus(filenumber);
		 mBuildingStatus=mTransObj.getWebServicePostBuildingDetailsStatus(fileNum);*/
        //	get_CanGenerateGBBill_Status(String FileNo)
        try {
            String status = mTransObj.getWebServiceCustomerFeedBackStatus(filenumber);
            System.out.println("******************************status" + status);
            if (status.equals("1")) {
                ContentValues PostBoringDetailsvalues = new ContentValues();
                PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", 1);
                mTransObj.update_PostBoringDetails(PostBoringDetailsvalues, filenumber);
                ContentValues updatedisconnection = new ContentValues();
                updatedisconnection.put("MeterDetailsInfoWebServiceStatus", 1);
                mTransObj.update_PostMeterDetails(updatedisconnection, filenumber);
                ContentValues cangenerate = new ContentValues();
                cangenerate.put("CanGenerateCANWebServiceStatus", filenumber);
                mTransObj.update_CanGenerateCAN(cangenerate, fileNum);
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in getWebServiceCustomerFeedBackStatus--- " + e);
        }

        try {
            String status = mTransObj.getWebServicePostBuildingDetailsStatus(filenumber);
            System.out.println("******************************status" + status);
            if (status.contains("1")) {


                ContentValues PostBoringDetailsvalues = new ContentValues();
                PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", 1);
                mTransObj.update_PostBoringDetails(PostBoringDetailsvalues, filenumber);
                ContentValues updatedisconnection = new ContentValues();
                updatedisconnection.put("MeterDetailsInfoWebServiceStatus", 1);
                mTransObj.update_PostMeterDetails(updatedisconnection, filenumber);
                ContentValues cangenerate = new ContentValues();
                cangenerate.put("CanGenerateCANWebServiceStatus", filenumber);
                mTransObj.update_CanGenerateCAN(cangenerate, fileNum);
            }
        } catch (Exception e) {
            System.out.println("Exception Occurs in getWebServicePostBuildingDetailsStatus--- " + e);
        }

        cFilenumber.setText("FileNo:" + filenumber);

        if (type.contains("E")) {
            mDisconnectionDetailsBut.setVisibility(View.VISIBLE);
            //TODO check disconnect details filled or not and also check boring details filled and meter details filled
            //setButtonsStatus(DISCONNECTION_DETAILS);
        } else if (type.contains("N")) {
            mDisconnectionDetailsBut.setVisibility(View.GONE);
            //setButtonsStatus(BORING_CONNECTION);

        }
    }

    private void disableGbRemarksButtons() {
        if (bConn.isBoringDetailsReceived() || bConn.isDisConnectionDetailsReceived()) {
            //Submit_Gb_RemarksBtn.setVisibility(View.GONE);
        }
    }

    /*private void disableButtons() {
        setButtonClick(mDisconnectionDetailsBut, false);
        setButtonClick(mBulidingphotoBtn, false);
        setButtonClick(mBoaringDetailsBtn, false);
        setButtonClick(mMeterDetailsBtn, false);
        setButtonClick(mCanGeneration, false);
        setButtonClick(SubmissionBtn, false);
        //		setButtonClick(Submit_Gb_RemarksBtn, false);
    }
     */
    private void setButtonsStatus(int connectionType) {
        switch (connectionType) {
            case DISCONNECTION_DETAILS:
                enableDisconnectButton();
                break;
            case BORING_CONNECTION:
                enableBoringConnection();
                break;
            case METER_CONNECTION:
                enableMeterButton();
                break;
            case CAN_CONNECTION:
                enableCanButton();
                break;
            case CUSTOMER_FEEDBACK:
                enableFeedBackButton();
                break;
            case FINAL_SUBMIT_CONNECTION:
                //mCanGeneration.setClickable(true);
                setButtonClick(mCanGeneration, true);
                break;
            default:
                break;
        }
    }

    private void disableRemarksButton() {
        if (bConn.isDisConnectionDetailsReceived() || bConn.isBoringDetailsReceived()) {
            setButtonsStatus(REMARKS_DETAILS);

        }
    }

    private void enableDisconnectButton() {
        mDisconnectionDetailsBut.setClickable(true);
        setButtonClick(mDisconnectionDetailsBut, true);
        if (bConn.isDisConnectionDetailsReceived()) {
            setButtonsStatus(BORING_CONNECTION);

        }
    }

    private void enableBoringConnection() {
        mBoaringDetailsBtn.setClickable(true);
        setButtonClick(mBoaringDetailsBtn, true);
        if (bConn.isBoringDetailsReceived()) {
            setButtonsStatus(METER_CONNECTION);
        }
    }

    private void enableMeterButton() {
        if (bConn.isMeterMandatory()) {
            mMeterDetailsBtn.setClickable(true);
            setButtonClick(mMeterDetailsBtn, true);
            if (bConn.isMeterDetailsReceived()) {
                setButtonsStatus(CAN_CONNECTION);

            }
        } else {
            mMeterDetailsBtn.setClickable(true);
            setButtonClick(mMeterDetailsBtn, true);
            setButtonsStatus(CAN_CONNECTION);
        }
    }

    private void enableCanButton() {
        mCanGeneration.setClickable(true);
        setButtonClick(mCanGeneration, true);
    }

    private void enableFeedBackButton() {
        if (bConn.isGbAcceptedForCanGeneration()) {
            mCanGeneration.setClickable(true);
        }
        mCustomerFeedbackDetails.setClickable(true);
        mCustomerBuildingDetails.setClickable(true);

    }


    private void setButtonClick(Button button, boolean isClickable) {
        if (isClickable == true) {
            button.setEnabled(isClickable);

            button.setSelected(isClickable);
            button.setBackgroundColor(Color.parseColor("#2AFF55"));
            button.setTextColor(Color.BLACK);
            //button.setBackgroundDrawable(R.drawable.shapegreen);
			/*button.setBackgroundColor(color.black);
			button.setBackgroundDrawable(R.drawable.shapegreen);*/

        } else if (isClickable == false) {
            button.setEnabled(isClickable);

            button.setBackgroundColor(Color.parseColor("#FF0000"));
			/*	GradientDrawable backgroundShape = (GradientDrawable)button.getBackground();
			backgroundShape.setColor(Color.RED);*/

        }


    }


    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        getBoringCoordinates();
    }

    public String getFilenumber() {
        return filenumber;
    }

    @SuppressLint("SdCardPath")
    private void getBoringCoordinates() {
        gps = new GPSTracker(this);


        // check if GPS enabled
        if (gps.canGetLocation()) {


            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            String lat = String.valueOf(latitude);
            String longi = String.valueOf(longitude);

            String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
            String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);


            String[] dmsBoringLong = strLongitude.split(":");

            longBoringdegree = dmsBoringLong[0];
            longBoringminute = dmsBoringLong[1];
            longBoringsecond = dmsBoringLong[2];
            //			longBoringMilliSecond=dmsBoringLong[3];


            String[] dmsBoringLat = strLongitude.split(":");
            latBoringDegree = dmsBoringLat[0];
            latgBoringMinute = dmsBoringLat[1];
            latgBoringSecond = dmsBoringLat[2];
            //			latBoringMilliSecond=dmsBoringLat[3];
            try {
                //	if (filDate.length<4) {

                File logFile = new File("/sdcard/gps.txt");
                if (!logFile.exists()) {
                    try {
                        logFile.createNewFile();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                String dateNow = lat + longi;
                buf.append(dateNow + ";");
                buf.newLine();
                buf.close();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

			/*
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();	*/


        } else {
            gps.showSettingsAlert();
        }

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }


        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            DatePath = mediaFile;

        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
		/*else if (requestCode == CONECTION_REQUEST_TYPE) {

			if (resultCode == DISCONNECTION_DETAILS) {
				setButtonsStatus(BORING_CONNECTION);
			}else if (resultCode == BORING_CONNECTION) {
				setButtonsStatus(METER_CONNECTION);
			}else if (resultCode == CAN_CONNECTION ) {
				setButtonsStatus(FINAL_SUBMIT_CONNECTION);
			}

		}*/
    }

    private void previewCapturedImage() {
        try {

            ExifInterface exif = null;
            try {
                exif = new ExifInterface(DatePath.getPath());
                date = exif.getAttribute(ExifInterface.TAG_DATETIME);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            getPhotoToByteArray();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    public byte[] getPhotoToByteArray() {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                options);
        try {

            //Encode to Stringche
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            mBulidingphotoBtn.setBackground(drawable);
            byteArray = byteArrayOutputStream.toByteArray();
            photo_Encoded = android.util.Base64.encodeToString(byteArray, android.util.Base64.NO_WRAP);
            //				dateTv.setText(encoded);
            handlePhoto = byteArray;

        } catch (Exception e) {
            System.out.println("exception in byteArray error" + e);
        }
        return byteArray;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.submit_gb_remarks: {
                Intent i = new Intent(getApplicationContext(), GbRemarksDetailsActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                bundle.putString(KEY_Address, strAddress);
                bundle.putString(KEY_NAME, strName);
                i.putExtras(bundle);
                finish();
                startActivity(i);

            }
            break;

            case R.id.disconnection_boring_details: {
                Intent i = new Intent(getApplicationContext(), DisconnectionDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                bundle.putString(KEY_Address, strAddress);
                bundle.putString(KEY_NAME, strName);

                i.putExtras(bundle);
                finish();
                startActivity(i);


            }
            break;

            case R.id.customer_boring_details: {
                Log.v("BUTTON", "customer_boring_details");

                Intent i = new Intent(getApplicationContext(), Boaringdetails.class);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                bundle.putString(KEY_Address, strAddress);
                bundle.putString(KEY_NAME, strName);
                //			bundle.putSerializable(AppConstants.GB_DATA, map);
                i.putExtras(bundle);
                startActivity(i);
                finish();
                //startActivityForResult(i, CONECTION_REQUEST_TYPE);

            }
            break;
            case R.id.meter_details_btn: {
                Intent i = new Intent(getApplicationContext(), MeterDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                bundle.putString(KEY_Address, strAddress);
                bundle.putString(KEY_NAME, strName);
                bundle.putString(KEY_SANCTIONEDPIPE, strSanctionedPipeSize);
                i.putExtras(bundle);
                finish();
                startActivity(i);

            }
            break;
            case R.id.boaring_point_photo: {
                captureImage();
            }
            break;
            case R.id.new_can_generate: {
                //************** insert code for cangeneration CanGenerateCANXMLString
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewConnection.this);

                // set title
                alertDialogBuilder.setTitle(" Are you sure that you want to generate a can to this file ");

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
					/*
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
					String currentDateandTime = sdf.format(new Date());
					ContentValues cangenerate= new ContentValues();
					cangenerate.put("FileNo",filenumber);
					cangenerate.put("CanGenerateCANCreateByUid",  SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					cangenerate.put("CanGenerateCANXMLString", filenumber);
					cangenerate.put("CanGenerateCANCreatedDtm", currentDateandTime);
					cangenerate.put("CanGenerateCANServerStatus", "finished");
					mTransObj.insert_cangenerateInfo(cangenerate);
					 */
                                /***************************************get remarks for customer***************/

                                //					cangeneratebuttonstatus(filenumber);

//
                                boolean isServiceAvailable = AppUtils.isNetworkAvailable(getApplicationContext());
                                if (isServiceAvailable) {
                                    CheckNetworkStatus(filenumber);
                                    BeforeAsyncCallToCanGen task = new BeforeAsyncCallToCanGen();
                                    //Call execute
                                    task.execute();
                                } else {
                                    Toast.makeText(NewConnection.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();


                //restartFirstActivity();
			/*	Intent i = new Intent(getApplicationContext(), CanGenerationDetails.class);
			Bundle bundle = new Bundle();
			bundle.putString(KEY_FILENO, bConn.getFileNo()); //This is for a String
			bundle.putString(KEY_Address, bConn.getAddress());
			bundle.putString(KEY_NAME, bConn.getName());
			i.putExtras(bundle);
			startActivity(i);*/
            }
            break;
            case R.id.contact_number: {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobilenum));
                startActivity(intent);
            }
            break;
            case R.id.customer_feedback_details: {
                Intent i = new Intent(getApplicationContext(), CustomerFeedback.class);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                bundle.putString(KEY_Address, strAddress);
                bundle.putString(KEY_NAME, strName);
                i.putExtras(bundle);
                finish();
                startActivity(i);


            }
            break;

            case R.id.customer_building_details: {
                Intent i = new Intent(getApplicationContext(), BuildingDetails.class);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_FILENO, filenumber); //This is for a String
                i.putExtras(bundle);
                finish();
                startActivity(i);

            }
            break;

            case R.id.final_submit_btn: {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewConnection.this);

                // set title
                alertDialogBuilder.setTitle("Are you sure that you want to generate a bill to this file");

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                try {
                                    String status = mTransObj.get_PostCustomerFeedBackDetails_Status(filenumber);
                                    System.out.println("****final_submit_btn**************get_PostMeterDetails_Status***********:" + status);

                                    String status1 = mTransObj.get_PostBuildingDetails_Status(filenumber);
                                    System.out.println("****final_submit_btn****************get_PostBuildingDetails_Status***********:" + status1);

                                    if (status == null || status1 == null) {

                                        Toast.makeText(getApplicationContext(), "Please Enter All Details status ", Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    if (!(status.contains("finished") && status1.contains("finished"))) {
                                        System.out.println("************final_submit_btn***********:" + status1 + status);
                                        Toast.makeText(getApplicationContext(), "Please Enter All Details ", Toast.LENGTH_LONG).show();
                                        return;
                                    }

                                    //Toast.makeText(getApplicationContext(), "Success.....! ", Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    System.out.println("**********final_submit_btn***********:" + e);
                                }
					/*mDisconnectionDetailsBut.setClickable(false);
						mBoaringDetailsBtn.setClickable(false);
						mMeterDetailsBtn.setClickable(false);
						mBulidingphotoBtn.setClickable(false);
						mCanGeneration.setClickable(false);
						SubmissionBtn.setClickable(false);
						mCustomerFeedbackDetails.setClickable(false);
						mCustomerBuildingDetails.setClickable(false);
					 */
                                ArrayList<String> getgbbilllist = mTransObj.getgbbilllist(filenumber);
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                String currentDateandTime = sdf.format(new Date());
                                if (getgbbilllist.isEmpty()) {
                                    ContentValues cangeneratebill = new ContentValues();
                                    cangeneratebill.put("FileNo", filenumber);
                                    cangeneratebill.put("CanGenerateGBBillString", filenumber);
                                    cangeneratebill.put("CanGenerateGBBillCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                                    cangeneratebill.put("CanGenerateGBBillCreatedDtm", currentDateandTime);
                                    cangeneratebill.put("CanGenerateCANServerStatus", "finished");
                                    mTransObj.insert_CanGenerateGBBill(cangeneratebill);

                                } else {
                                    ContentValues cangeneratebill = new ContentValues();
                                    cangeneratebill.put("FileNo", filenumber);
                                    cangeneratebill.put("CanGenerateGBBillString", filenumber);
                                    cangeneratebill.put("CanGenerateGBBillCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                                    cangeneratebill.put("CanGenerateGBBillCreatedDtm", currentDateandTime);
                                    cangeneratebill.put("CanGenerateCANServerStatus", "finished");
                                    mTransObj.update_CanGenerateCAN(cangeneratebill, filenumber);
                                    ContentValues prndingfile = new ContentValues();
                                    prndingfile.put("GBStatus", "finished");
                                    mTransObj.update_pendingfileconnection(prndingfile, filenumber);
                                }

                                boolean isServiceAvailable = AppUtils.isNetworkAvailable(getApplicationContext());
                                if (isServiceAvailable) {
                                    CheckNetworkStatus(filenumber);
                                    BeforeAsyncCallToGbBillGen taskk = new BeforeAsyncCallToGbBillGen();
                                    taskk.execute();
                                } else {
                                    Toast.makeText(NewConnection.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                                }


                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();


            }
            break;

            case R.id.btn_selfconnection: {
                Toast.makeText(getApplicationContext(), "The Self Connection is clicked", 500).show();
            }
            break;
            default:
                break;
        }

    }


    private void PostConnectionInfo() {

		/*	ConnInfo.append("<ConnInfo>");
		ConnInfo.append("<FileNo>");
		ConnInfo.append(map.get(AppConstants.KEY_FILENO));
		ConnInfo.append("</FileNo>");
		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<CustomerRemarks>");
		ConnInfo.append("all is well eith him");
		ConnInfo.append("</CustomerRemarks>");

		ConnInfo.append("<CustomerStarRating>");
		ConnInfo.append("5");
		ConnInfo.append("</CustomerStarRating>");

		ConnInfo.append("</ConnInfo>");*/

        ConnInfo.append("<ConnInfo>");

        ConnInfo.append("<FileNo>");
        ConnInfo.append(bConn.getFileNo());
        ConnInfo.append("</FileNo>");

        ConnInfo.append("<GbRemarks>");
        ConnInfo.append(RemarksET.getText().toString());
        ConnInfo.append("</GbRemarks>");

        ConnInfo.append("<GBNO>");
        ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
        ConnInfo.append("</GBNO>");

        ConnInfo.append("</ConnInfo>");

        ConnXml = ConnInfo.toString();

        Log.v(ConnInfo.toString(), "XML");

        AsyncCallWS task = new AsyncCallWS();
        //Call execute
        task.execute();

    }


    private void SelfConnectionInfo() {


        ConnInfo2.append("<ConnInfo>");

        ConnInfo2.append("<fileNo>");

        System.out.println("The Self connection file no    " + bConn.getFileNo() + " GB NO" + SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
        ConnInfo2.append(bConn.getFileNo());
        ConnInfo2.append("</fileNo>");
        ConnInfo2.append("<gbNo>");
        ConnInfo2.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
        ConnInfo2.append("</gbNo>");

        ConnInfo2.append("</ConnInfo>");

        selfconn = ConnInfo2.toString();

        Log.v(ConnInfo2.toString(), "XML");

		/*AsyncCallWSselfconnection task = new AsyncCallWSselfconnection();
		//Call execute
		task.execute();*/

    }

    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        //	AE7654
        @Override
        protected Void doInBackground(Void... params) {
            try {
                GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostCustomerFeedBackDetailsInfo");
            } catch (Exception e) {
                // TODO: handle exception
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {

				/*
				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
				 */
            } catch (Exception e) {
                System.out.println("exception in PostGBRemarksInfo error" + e);
            }


        }

    }

    private class AsyncCallToGbBill extends AsyncTask<Void, Void, String> {


        private ProgressDialog mProgressDialog;
        //	AE7654
        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(NewConnection.this, "",
                    "Loading. Please wait...", true);
        }

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.invokeGbFileNo(filenumber, gbno, "CanGenerateGBBill");
                System.out.println("the status of generate bill" + result);
            } catch (Exception e) {
                // TODO: handle exception
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            String splitresult = null;
            try {

                //				Toast.makeText(getApplicationContext(), result, 100).show();
                mProgressDialog.dismiss();


                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                    ContentValues cangeneratebill = new ContentValues();
                    cangeneratebill.put("CanGenerateGBBillWebServiceStatus", result);
                    mTransObj.update_CanGenerateGBBill(cangeneratebill, filenumber);
                } else {
                    if (result.contains("CanGenerateGBBill staus updation cannot be acceptable")) {
                        String arr[] = result.split("|");
                        splitresult = arr[1];
                        System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);
                        ContentValues cangeneratebill = new ContentValues();
                        cangeneratebill.put("CanGenerateGBBillWebServiceStatus", result);
                        mTransObj.update_CanGenerateGBBill(cangeneratebill, filenumber);

                        ContentValues prndingfile = new ContentValues();
                        prndingfile.put("GBStatus", "finished");
                        mTransObj.update_pendingfileconnection(prndingfile, filenumber);
                        Toast.makeText(getApplicationContext(), "GB Bill Generated", Toast.LENGTH_LONG).show();

                        mDisconnectionDetailsBut.setClickable(false);
                        mBoaringDetailsBtn.setClickable(false);
                        mMeterDetailsBtn.setClickable(false);
                        mBulidingphotoBtn.setClickable(false);
                        mCanGeneration.setClickable(false);
                        SubmissionBtn.setClickable(false);
                        mCustomerFeedbackDetails.setClickable(false);
                        mCustomerBuildingDetails.setClickable(false);
                        mSelfConnectionbtn.setClickable(false);
                    }

                }


                if (splitresult.contains("1")) {

                    ContentValues prndingfile = new ContentValues();
                    prndingfile.put("GBStatus", "finished");
                    mTransObj.update_pendingfileconnection(prndingfile, filenumber);
                    Toast.makeText(getApplicationContext(), "GB Bill Generated", Toast.LENGTH_LONG).show();

                    mDisconnectionDetailsBut.setClickable(false);
                    mBoaringDetailsBtn.setClickable(false);
                    mMeterDetailsBtn.setClickable(false);
                    mBulidingphotoBtn.setClickable(false);
                    mCanGeneration.setClickable(false);
                    SubmissionBtn.setClickable(false);
                    mCustomerFeedbackDetails.setClickable(false);
                    mCustomerBuildingDetails.setClickable(false);
                    mSelfConnectionbtn.setClickable(false);

                }
                if (splitresult.contains("0")) {
                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

                }


                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                startActivity(i);
            } catch (Exception e) {
                System.out.println("exception in CanGenerateGBBill error" + e);
            }


        }

    }

    private class AsyncCallToCanGen extends AsyncTask<Void, Void, String> {
        String result;
        private ProgressDialog mProgressDialog;

        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(NewConnection.this, "",
                    "Can Generation. Please wait...", true);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno2 = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.invokeGbFileNo(filenumber, gbno2, "CanGenerateCAN");


            } catch (Exception e) {
            }


            return result;
        }


        @Override

        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();
            String splitresult = null;

            try {
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    ContentValues cangenerate = new ContentValues();
                    cangenerate.put("CanGenerateCANWebServiceStatus", result);
                    mTransObj.update_CanGenerateCAN(cangenerate, filenumber);

                } else {
                    if (/*result.contains("Unable to update status as CanGenerateCAN")||*/result.contains("CanGenerateCAN staus updation cannot be acceptable")) {
                        //0|At this satge, for FileNo: 2015-5-1594 CanGenerateCAN staus updation cannot be acceptable.

						/*ContentValues cangeneratebill= new ContentValues();
						cangeneratebill.put("CanGenerateGBBillWebServiceStatus", 1);
						mTransObj.update_CanGenerateCAN(cangeneratebill,fileNum);
						cangeneratebuttonstatus(filenumber);
						Toast.makeText(getApplicationContext(), "Can Generated Successfuly", Toast.LENGTH_SHORT).show();*/
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                        String currentDateandTime = sdf.format(new Date());
                        ContentValues cangenerate = new ContentValues();
                        cangenerate.put("FileNo", filenumber);
                        cangenerate.put("CanGenerateCANCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                        cangenerate.put("CanGenerateCANXMLString", fileNum);
                        cangenerate.put("CanGenerateCANCreatedDtm", currentDateandTime);
                        cangenerate.put("CanGenerateCANWebServiceStatus", "1");
                        cangenerate.put("CanGenerateCANServerStatus", "finished");
                        mTransObj.insert_cangenerateInfo(cangenerate);
                        cangeneratebuttonstatus(filenumber);

                    }

                    String arr[] = result.split("|");
                    splitresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);


                }
                if (splitresult.contains("0")) {
                    //					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_SHORT).show();
                }
                if (splitresult.contains("1")) {
                    Toast.makeText(getApplicationContext(), "Can Generated Successfuly", Toast.LENGTH_SHORT).show();
					/*ContentValues cangenerate= new ContentValues();
					cangenerate.put("CanGenerateCANWebServiceStatus", splitresult);
					mTransObj.update_CanGenerateCAN(cangenerate,filenumber);*/

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    String currentDateandTime = sdf.format(new Date());
                    ContentValues cangenerate = new ContentValues();
                    cangenerate.put("FileNo", filenumber);
                    cangenerate.put("CanGenerateCANCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                    cangenerate.put("CanGenerateCANXMLString", filenumber);
                    cangenerate.put("CanGenerateCANCreatedDtm", currentDateandTime);
                    cangenerate.put("CanGenerateCANWebServiceStatus", splitresult);
                    cangenerate.put("CanGenerateCANServerStatus", "finished");
                    mTransObj.insert_cangenerateInfo(cangenerate);

                    cangeneratebuttonstatus(filenumber);
                }

            } catch (Exception e) {
                System.out.println("exception in CanGenerateCANWebServiceStatus error" + e);

            }


        }

    }

    private class BeforeAsyncCallToCanGen extends AsyncTask<Void, Void, String> {
        SoapObject result;
        private ProgressDialog mProgressDialog;

        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(NewConnection.this, "",
                    "Checking Status. Please wait...", true);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno2 = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.getServersideStatus(filenumber, "IsDataExists");


            } catch (Exception e) {
            }
            return "";
        }


        @Override
        protected void onPostExecute(String res) {
            mProgressDialog.dismiss();
            String splitresult = null;
            try {
                if (result.toString() != "") {
                    String borestatus = result.getProperty(
                            "IsBoringDetailsExists").toString();
                    String meterstatus = result.getProperty(
                            "IsMeterDetailsExists").toString();
                    String canstatus = result.getProperty(
                            "IsCanGenerated").toString();
                    String feddbackstatus = result.getProperty(
                            "IsFeedBackDetailsExists").toString();
                    String buildingstatus = result.getProperty(
                            "IsBuildingPhotoExists").toString();
                    System.out.println("data came" + borestatus + meterstatus + canstatus + feddbackstatus + buildingstatus);
                    if (borestatus.equalsIgnoreCase("true") && meterstatus.equalsIgnoreCase("true")) {
                        AsyncCallToCanGen task = new AsyncCallToCanGen();
                        //Call execute
                        task.execute();
                    } else {
//                        Toast.makeText(NewConnection.this, "Bore and meter not sent", Toast.LENGTH_SHORT).show();
                        //Bore and meter not sent forcibly sent
                        sendBoreMeterDetails(filenumber);
                        AsyncCallToCanGen task = new AsyncCallToCanGen();
                        //Call execute
                        task.execute();
                    }
                }


            } catch (Exception e) {
                System.out.println("exception in getServiceStatus error" + e);

            }


        }

    }

    private class BeforeAsyncCallToGbBillGen extends AsyncTask<Void, Void, String> {
        SoapObject result;
        private ProgressDialog mProgressDialog;

        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(NewConnection.this, "",
                    "Checking Status. Please wait...", true);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno2 = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.getServersideStatus(filenumber, "IsDataExists");


            } catch (Exception e) {
            }
            return "";
        }


        @Override
        protected void onPostExecute(String res) {
            mProgressDialog.dismiss();
            String splitresult = null;
            try {
                if (result.toString() != "") {
                    String borestatus = result.getProperty(
                            "IsBoringDetailsExists").toString();
                    String meterstatus = result.getProperty(
                            "IsMeterDetailsExists").toString();
                    String canstatus = result.getProperty(
                            "IsCanGenerated").toString();
                    String feddbackstatus = result.getProperty(
                            "IsFeedBackDetailsExists").toString();
                    String buildingstatus = result.getProperty(
                            "IsBuildingPhotoExists").toString();
                    System.out.println("data came" + borestatus + meterstatus + canstatus + feddbackstatus + buildingstatus);
                    if (feddbackstatus.equalsIgnoreCase("true") && buildingstatus.equalsIgnoreCase("true")) {
                        AsyncCallToGbBill task = new AsyncCallToGbBill();
                        task.execute();
                    } else {
//                            Toast.makeText(NewConnection.this, "feed and build not sent", Toast.LENGTH_SHORT).show();
                        //feedback and building not sent forcibly sent
                        sendFeedBuilddetails(filenumber);
                        AsyncCallToGbBill task = new AsyncCallToGbBill();
                        task.execute();
                    }


                }


            } catch (Exception e) {
                System.out.println("exception in getServiceStatus error" + e);

            }


        }

    }

    private Boolean checkField() {
        boolean flag = false;
        if (RemarksET.getText().toString().length() < 10) {
            RemarksET.requestFocus();
            RemarksET.setError("Length should be more than 10");
            flag = true;
        } else {
            RemarksET.setError(null);
        }
        return flag;
    }

    private void restartFirstActivity() {
        Intent i = getApplicationContext().getPackageManager()
                .getLaunchIntentForPackage(getApplicationContext().getPackageName());

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void cangeneratebuttonstatus(String filenumber) {
        try {
            String status = mTransObj.get_CanGenerate_Status(filenumber);
            System.out.println("*****method ************cangeneratebuttonstatus" + status);
            if (status.contains("finished")) {
                setButtonClick(mBoaringDetailsBtn, false);
                setButtonClick(mMeterDetailsBtn, false);
                setButtonClick(mDisconnectionDetailsBut, false);
                setButtonClick(mCanGeneration, false);
                setButtonClick(mSelfConnectionbtn, false);
                setButtonClick(mCustomerFeedbackDetails, true);
                setButtonClick(mCustomerBuildingDetails, true);
            }
        } catch (Exception e) {
            Log.i("exceptoion", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(this, NewConnection.class);
        startActivity(setIntent);
        finish();
    }

    private class AsyncCallisSelfConnection extends AsyncTask<Void, Void, String> {
        String result;
        private ProgressDialog mProgressDialog;

        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(NewConnection.this, "",
                    "Updating SelfConnection Details. Please wait...", true);

        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");

                System.out.println("the filenumber in is self connection   " + filenumber + "GB Code" + gbno);

                result = GbPostPendingDataToServer.invokeGbFileNo(filenumber, gbno, "ConvertToSelfConnection");
                mProgressDialog.dismiss();
                SharedPreferenceUtils.savePreference(getApplicationContext(), IPreferencesConstants.SELF_STATUS, "1");

                selfFlag = true;

            } catch (Exception e) {
                // TODO: handle exception
            }


            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            String splitresult = null;
            try {
                String arr[] = result.split("|");
                splitresult = arr[1];
                System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);
                if (splitresult.contains("1") || result.contains("already updated as SelfConnection")) {
                    Toast.makeText(getApplicationContext(), "Self Connection Successfuly Send", Toast.LENGTH_SHORT).show();
                    ContentValues PostSelfConnStatusValue = new ContentValues();
                    PostSelfConnStatusValue.put("GetGBPendingFilesInfoCreateByUid", "1");
                    mTransObj.update_GbPendingFileInfo(PostSelfConnStatusValue, filenumber);
                }
            } catch (Exception e) {
                System.out.println("exception in PostGBRemarksInfo error" + e);
            }
        }

    }

	/*	private class AsyncCallWSselfconnection extends AsyncTask<Void, Void, Void> {

		//	AE7654
		@Override
		protected Void doInBackground(Void... params) {
			try {
				GbPostPendingDataToServer.invokePendingFiles(selfconn, "ConvertToSelfConnection");
				//				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostCustomerFeedBackDetailsInfo");
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			try {


				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
				 } catch (Exception e) {
					 System.out.println("exception in PostGBRemarksInfo error"+ e); 
				 }


		}

	}
	 */

}
