package com.hmwssb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

public class GbRemarksDetailsActivity extends SuperActivity implements OnClickListener , AppConstants{


	private Button Submit_Gb_RemarksBtn;
	StringBuilder ConnInfo = new StringBuilder();
	private String ConnXml;
	private EditText RemarksET;
	TextView mFileTv,mAddressTv;
	private String mFileno;
	private String mAddress;
	Transactions transobj;
	ArrayList<String>postremarkslist;
	public ImageView signout, home;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.gbremarks_activity);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		Submit_Gb_RemarksBtn=(Button)findViewById(R.id.submit_gb_remarks);
		RemarksET=(EditText)findViewById(R.id.gb_remarks_capture);
		mAddressTv = (TextView)findViewById(R.id.cus_address);
		mFileTv = (TextView)findViewById(R.id.cus_fileno);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);
		//transobj=new Transactions(this);
		transobj = new Transactions();
		try {
			transobj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.new_connection);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("Post Remarks");

		Bundle bundle = getIntent().getExtras();
		mFileno = bundle.getString(KEY_FILENO); 
		mAddress = bundle.getString(KEY_Address);
		mFileTv.setText(mFileno);
		mAddressTv.setText(mAddress);
		
		/****************************getPostGBRemarks********************************
		 * 
		 */
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				finish();
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//			    logoutFromApp();
				finish();
				System.exit(0);
			}
		});
		String result = null;
		postremarkslist=	transobj.getPostGBRemarks(mFileno);
		System.out.println("postremarkslistpostremarkslist"+postremarkslist.size());
		for(int i=0;i<postremarkslist.size();i++)
		{
			result=postremarkslist.get(0);
		}
		RemarksET.setText(result);
		Submit_Gb_RemarksBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//				if (!checkField()) {
				/*CREATE TABLE `PostGBRemarksInfo` (
						`FileNo`	TEXT,
						`GBNo`	TEXT,
						`GBREMARKS`	TEXT,
						`RemarksServerStatus`	TEXT,
						`GBRemarksInfoCreateByUid`	TEXT,
						`GBRemarksInfoCreatedDtm`	TEXT,
						PRIMARY KEY(FileNo)
					);*/
				String	result=RemarksET.getText().toString();
				if(result.equals("")|| result==null )
				{
					RemarksET.setError("Please Enter Remarks");
					return;
				}
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
				String currentDateandTime = sdf.format(new Date());
				if(postremarkslist.isEmpty())
				{
				PostConnectionInfo();
				ContentValues postremarksvalue=new ContentValues();
				postremarksvalue.put("FileNo", mFileno);
				postremarksvalue.put("GBREMARKS", RemarksET.getText().toString());
				postremarksvalue.put("GBRemarksInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
				postremarksvalue.put("GBRemarksInfoCreatedDtm", currentDateandTime);
				postremarksvalue.put("GBRemarksInfoXMLString", ConnXml);
				postremarksvalue.put("RemarksServerStatus", "finished");
				transobj.insert_PostGBRemarksInfo(postremarksvalue);
				
				ContentValues updatependingfile= new ContentValues();
				updatependingfile.put("GBStatus", "Remarks Captured");
				transobj.update_pendingfileconnection(updatependingfile,mFileno);
			
				Intent i = new Intent(GbRemarksDetailsActivity.this,NewConnection.class);
				startActivity(i);
				setResult(BORING_CONNECTION);
				finish();
				}
				else

				{
					PostConnectionInfo();
					ContentValues upostremarksvalue=new ContentValues();
				
					upostremarksvalue.put("GBREMARKS", RemarksET.getText().toString());
					upostremarksvalue.put("GBRemarksInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					upostremarksvalue.put("GBRemarksInfoCreatedDtm", currentDateandTime);
					upostremarksvalue.put("GBRemarksInfoXMLString", ConnXml);
					upostremarksvalue.put("RemarksServerStatus", "finished");
					transobj.update_PostGBRemarksInfo(upostremarksvalue,mFileno);
					
					Intent i1 = new Intent(GbRemarksDetailsActivity.this,NewConnection.class);
					startActivity(i1);
					setResult(BORING_CONNECTION);
					finish();
				}

			
				setResult(BORING_CONNECTION);
				finish();
							
			}
		});
	}

	protected void PostConnectionInfo() {
		ConnInfo.append("<ConnInfo>");

		ConnInfo.append("<FileNo>");
		ConnInfo.append(mFileno);
		ConnInfo.append("</FileNo>");

		ConnInfo.append("<GbRemarks>");
		ConnInfo.append(RemarksET.getText().toString());
		ConnInfo.append("</GbRemarks>");

		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("</ConnInfo>");

		ConnXml=ConnInfo.toString();

		Log.v(ConnInfo.toString(), "XML");
		

		AsyncCallWS task = new AsyncCallWS();
		//Call execute 
		task.execute();

	}

	private class AsyncCallWS extends AsyncTask<Void, Void, String> {
		String result;
		
		@Override          
		protected void onPreExecute() {
			super.onPreExecute();      
			Toast.makeText(getApplicationContext(), "Posting GBRemarks", Toast.LENGTH_LONG).show();
		}
		//	AE7654
		@Override
		protected String doInBackground(Void... params) {
			try {
				result=GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
				
			
			
				//				
			} catch (Exception e) {
				// TODO: handle exception
			}


			

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			
			String splitresult=null;
		
			try {
				if(result.contains("failed"))
				{
					ContentValues postremarksvalue=new ContentValues();
					postremarksvalue.put("GBRemarksInfoWebServiceStatus",result);
					transobj.update_PostGBRemarksInfo(postremarksvalue,mFileno);
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
					
				}
				else
				{
				String arr[]=result.split("|");
				splitresult=arr[1];
				System.out.println("the split result valuesa arr[0]:"+result+"******************the split values of arr[1]:"+arr[1]);
				
				
				}
				if(splitresult.contains("0"))
				{
					Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();	
				}

				
				if(splitresult.contains("1"))
				{
					Toast.makeText(getApplicationContext(), "Data Successfuly Send", Toast.LENGTH_SHORT).show();
					ContentValues postremarksvalue=new ContentValues();
					postremarksvalue.put("GBRemarksInfoWebServiceStatus",result);
					transobj.update_PostGBRemarksInfo(postremarksvalue,mFileno);
					
				}
				


			} catch (Exception e) {
				
			}


		}

	}
	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onBackPressed()
	{
		Intent setIntent = new Intent(this,HomeScreenTabActivity.class);
		startActivity(setIntent); 
		finish();
	} 
}
