package com.hmwssb;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;

public class MainActivity extends Activity {


	public static int curScrenHeit;
	public static int curScrenWidth;
	public static int defaultScrenWidth=480,defaultScrenHeit=800;

	public static int defTextSize;
	public static String defTextColor="#000000";
	public static String defshadowColor="#ffffff";
	public static float shadowRadius=10f;

	public static Typeface font,fontBold;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		DisplayMetrics displaymetrics = new DisplayMetrics();   
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics); 
		curScrenHeit = displaymetrics.heightPixels;  
		curScrenWidth = displaymetrics.widthPixels;    
	}
}
