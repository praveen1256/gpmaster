package com.hmwssb;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class ValidateOtp extends Activity{

	Button validateBtn,resendOtp;
	EditText checkOtp;
	ProgressBar pg;
	String otpData,GBCCode; 
	public static int curScrenHeit;
	public static int curScrenWidth;
	OTPActivity otpclass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_validateotp);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		DisplayMetrics displaymetrics = new DisplayMetrics();   
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics); 
		curScrenHeit = displaymetrics.heightPixels;  
		curScrenWidth = displaymetrics.widthPixels;    

		checkOtp=(EditText)findViewById(R.id.validOtpEt);
		final  AlertDialog ad=new AlertDialog.Builder(this).create();
		
		
		resendOtp=(Button)findViewById(R.id.resendOtp);
		validateBtn=(Button)findViewById(R.id.validateOtp);
		
		pg = (ProgressBar) findViewById(R.id.progressBar2);
		validateBtn.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {

				if (checkOtp.getText().length() != 0 && checkOtp.getText().toString() != "") {
					//Get the text control value
					otpData = checkOtp.getText().toString();
					//Create instance for AsyncCallWS
					AsyncCallWS task = new AsyncCallWS();
					//Call execute 
					task.execute();
					//If text control is empty
				} else {
					checkOtp.setText("INvalid otp");
				}
			} });
		
	}
	
	public ArrayList<String> fetchInbox()
	{
		ArrayList<String> sms = new ArrayList<String>();
		Uri uriSms = Uri.parse("content://sms/inbox");
		Cursor cursor = getContentResolver().query(uriSms, new String[]{"_id", "address", "date", "body"},null,null,null); 
		cursor.moveToFirst();
		while  (cursor.moveToNext()) 
		{
			String address = cursor.getString(1);
			String body = cursor.getString(3);
			if (body.contains("")) {
				System.out.println("======> Mobile number => "+address);
				System.out.println("=====> SMS Text => "+body);
				sms.add("Address=> "+address+"\n SMS => "+body);
			}


		}
		return sms;

	}
	private class AsyncCallWS extends AsyncTask<String, Void, Void> {
		String displayText;
	//	AE7654
		@Override
		protected Void doInBackground(String... params) {
			Bundle bu = getIntent().getExtras();
			GBCCode = bu.getString("OTP");
			if (displayText.equalsIgnoreCase("VALIDATED")) {
				Intent ic = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				Bundle dataToShow = new Bundle();
				dataToShow.putString("GBCCode",GBCCode );
				ic.putExtras(dataToShow);
				startActivity(ic);
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pg.setVisibility(View.INVISIBLE);
		}

		@Override
		protected void onPreExecute() {

			pg.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

	}


}