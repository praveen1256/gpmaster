package com.hmwssb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.widget.Toast;

import com.vajra.hmwsdatabase.SqlLiteDbHelper;
import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.UrlConstants;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.hmwssb.utility.Utility.PERMISSION_REQUEST_CODE;

public class SplashScreen extends Activity implements UrlConstants {
    protected static final String URLPATH = "URLPATH";
    protected static final String dateDelimitor = "-";
    private static final String SAMPLE_DB_NAME = "GBDatabase";
    private int soundDuration = 500;

    private AlertDialog builder;
    private char[] line;
    private StringBuffer str;

    String aBuffer = "";
    SQLiteDatabase db;
    String simno1, simno2, otpstatus, SIMSerialNumber;
    File appIDFile;
    Transactions transobj;
    private GPSTracker gps;
    boolean runapplication;
    String d = getdate();
    boolean check_permission;
    Boolean isSDPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
                getSimSerialNumber();
                SDPresent();
            } else {
                requestPermission();
            }
        } else {
            isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
            getSimSerialNumber();
            SDPresent();
        }
    }

    private void SDPresent() {
        if (isSDPresent) {

            // yes SD-card is present
            SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(this);

            try {
                dbHelper.createdatabase();
                //dbHelper.CopyDataBaseFromAsset();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            str = new StringBuffer();
            builder = new AlertDialog.Builder(this).create();
            builder.setTitle("ID's");

            final Handler handler = new Handler();
            Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    handler.post(new Runnable() {


                        public void run() {


                            File sd = Environment.getExternalStorageDirectory();

                            if (sd.exists() == true) {
                                //								String simno=BoringConectionServices.Checksimno();
                                String simno = null;
                                int r = exportDB();

                                if (r == 1) {
                                    if (simno != null) {
                                        try {
                                            System.out.println("This simno ! null**********************" + simno);

                                            transobj.getnewsimno(simno);
                                        } catch (Exception e) {
											/*String sim_no=BoringConectionServices.validateSimno(d);
											Toast.makeText(getApplicationContext(), sim_no , Toast.LENGTH_LONG).show();
											 */
                                            return;
                                        }


                                    } else {

                                        calForward();

                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(), "Please Insert SD Card", Toast.LENGTH_LONG).show();
                                    finish();
                                    System.exit(0);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "Please Insert SD Card", Toast.LENGTH_LONG).show();
                                finish();
                                System.exit(0);
                            }


                            //		checkTrail();

                        }
                    });
                }
            }, soundDuration);

        } else {
            Toast.makeText(getApplicationContext(), "Please Insert SD Card ", Toast.LENGTH_LONG).show();
            finish();
            System.exit(0);
        }

    }

    private void calForward() {
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            try {
                transobj = new Transactions();
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkPermission()) {
                            transobj.open();
                        } else {
                            requestPermission();
                        }
                    } else {
                        transobj.open();
                    }

                    //					String simno=BoringConectionServices.Checksimno();
                    String simno = null;

                    if (simno != null) {
                        try {
                            System.out.println("This simno ! null**********************" + simno);

                            transobj.getnewsimno(simno);
                        } catch (Exception e) {
							/*							String sim_no=BoringConectionServices.validateSimno(d);
							Toast.makeText(getApplicationContext(), sim_no , Toast.LENGTH_LONG).show();
							 */
                            return;
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    simno1 = transobj.simnumberfromSWC_Licence();
                    simno2 = transobj.simnumberfromSWC_LicenceOTP();
                    otpstatus = transobj.status(simno2);
                } catch (Exception e) {

                }
				/*	if((SIMSerialNumber.equals(simno2)))
			{
				Intent i=new Intent(getApplicationContext(),HomeScreenTabActivity.class);
				startActivity(i);
				finish();
            }

			else 
			{
				Toast.makeText(getApplicationContext(), "  Insert Your Valid Sim Card", 200).show();
				Intent i=new Intent(getApplicationContext(),OTPActivity.class);
				startActivity(i); 
				finish();
			}*/
                if (SIMSerialNumber != null) {


                    if ((simno1 == null)) {
                        Intent i = new Intent(getApplicationContext(), OTPActivity.class);
                        startActivity(i);
                        finish();
                    } else if (!(simno1 == null) && (otpstatus == "0") || (otpstatus == null)) {
                        Intent i = new Intent(getApplicationContext(), GetotpActivity.class);
                        startActivity(i);
                        finish();
                    } else if ((otpstatus.equals("1")) && (simno2.equals(simno1))) {

                        Getversioncontrol disasyn = new Getversioncontrol();
                        disasyn.execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "  Invalid User ", Toast.LENGTH_LONG).show();

                        Intent i = new Intent(getApplicationContext(), OTPActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Insert SD Card", Toast.LENGTH_LONG).show();
                    finish();
                    System.exit(0);
                }

            } catch (Exception e) {
                Intent i = new Intent(getApplicationContext(), OTPActivity.class);
                startActivity(i);
                finish();

            }

        } else {
            gps.showSettingsAlert();
        }
    }

    private int exportDB() {
        int i = 0;
        File sd = Environment.getExternalStorageDirectory();
        if (sd.exists() == true) {
            //Toast.makeText(this, "DB exists!", Toast.LENGTH_LONG).show();

            File data = Environment.getDataDirectory();
            FileChannel source = null;
            FileChannel destination = null;
            String currentDBPath = "/data/" + "com.hmwssb" + "/databases/" + SAMPLE_DB_NAME;
            String backupDBPath = SAMPLE_DB_NAME;
            File currentDB = new File(data, currentDBPath);
            File backupDB = new File(sd, backupDBPath);
            if (backupDB.exists()) {


            } else {
                try {
                    source = new FileInputStream(currentDB).getChannel();
                    destination = new FileOutputStream(backupDB).getChannel();
                    destination.transferFrom(source, 0, source.size());
                    source.close();
                    destination.close();


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            i = 1;
        } else {

            i = 2;
        }


        String simno = null;
        if (simno != null) {
            try {
                System.out.println("This simno ! null**********************" + simno);

                transobj.getnewsimno(simno);
            } catch (Exception e) {/*
				String sim_no=BoringConectionServices.validateSimno(d);
				Toast.makeText(getApplicationContext(), sim_no , Toast.LENGTH_LONG).show();
			 */
            }


        }

        return i;
    }

    @Override
    public void recreate() {
        // TODO Auto-generated method stub
        super.recreate();
    }

    private class Getversioncontrol extends AsyncTask<Void, String, String> {
        String result;

        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.GetVersioncontrol("GetLatestVersionNo");
                System.out.println("****************************Getversioncontrol " + result);

                //				String simno=BoringConectionServices.Checksimno();
                String simno = null;

                if (simno != null) {
                    try {
                        System.out.println("This simno ! null**********************" + simno);

                        transobj.getnewsimno(simno);
                    } catch (Exception e) {
                        //						String sim_no=BoringConectionServices.validateSimno(d);
                        //Toast.makeText(getApplicationContext(), sim_no , Toast.LENGTH_LONG).show();

                    }


                }


            } catch (Exception e) {
                System.out.println("exception in Getversioncontrol error" + e);
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {

            try {

                System.out.println("****************************result  Getversioncontrol" + result);
                if (result.contains("failed")) {
                    //Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
                if (APP_VERSION.contains(result) || APP_VERSION.equals(result)) {
                    Toast.makeText(getApplicationContext(), " Present version :" + result, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                    startActivity(i);
                    finish();
                } else {

                    ArrayList<String> pendingfileslist = transobj.getfailedfiles();
                    System.out.println("pending files list :" + pendingfileslist.toString());
                    pendingfileslist.remove("Select One");
                    System.out.println("pending files list :" + pendingfileslist.toString());
                    ArrayList<BoringConnection> failedfiles = transobj.getPendingdetails();
                    failedfiles.trimToSize();
                    System.out.println(" failedfiles list :" + failedfiles.toString());

                    if ((pendingfileslist.isEmpty() || (failedfiles == null))) {
                        showalertdialog();
                    } else {
                        String simno = null;
                        if (simno != null) {
                            try {
                                System.out.println("This simno ! null**********************" + simno);

                                transobj.getnewsimno(simno);
                            } catch (Exception e) {
                                //								String sim_no=BoringConectionServices.validateSimno(d);
                                //Toast.makeText(getApplicationContext(), sim_no , Toast.LENGTH_LONG).show();
                                return;

                            }


                        }
                        transobj.delete_newfiles();
                        AlertDialog.Builder alerdialog = new AlertDialog.Builder(SplashScreen.this);
                        alerdialog.setTitle("Update Version  Dialog");
                        alerdialog.setMessage("Please Complete all Pending and Failed Files " + "\n" + " Then Update With New Version");
                        alerdialog.setCancelable(false);
                        alerdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                                i.putExtra("version", "version not matched");
                                startActivity(i);


                            }
                        });
                        alerdialog.show();
                    }
                }

            } catch (Exception e) {
                System.out.println("Exception Occurs Getversioncontrol " + e);
            }


        }

    }


    /*

    public void getnetworkdate()
    {
        System.out.println("in client date method");
        try {
            TimeTCPClient client = new TimeTCPClient();
            try {
                // Connecting to time server
                // Other time servers can be found at : http://tf.nist.gov/tf-cgi/servers.cgi#
                // Make sure that your program NEVER queries a server more frequently than once every 4 seconds
                client.connect("nist.time.nosc.us");
                System.out.println("in client date client");
                System.out.println("client date "+client.getDate());
            } finally {
                client.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
     *//*	public class getcurrentdateasync extends AsyncTask<Void, String, String>
	{

		@Override
		protected String doInBackground(Void... params) {
			getnetworkdate();
			return null;
		}



	}*/
    private void showalertdialog() {
        new AlertDialog.Builder(this).setTitle("Update")
                .setMessage("Please Update GB APP  from playstore")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = getPackageName(); // Can
                        // also
                        // use
                        // getPackageName(),
                        // as
                        // below
//						startActivity(new Intent(Intent.ACTION_VIEW, Uri
//								.parse("market://details?id=" + appPackageName)));
//						finish();
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(marketIntent);
                        finishAffinity();

                    }
                }).create().show();
    }

    public String getdate() {

        Random r = new Random();
        int Low = 1;
        int High = 38;
        int R = r.nextInt(High - Low) + Low;
        String s = String.valueOf(R);
        return s;
    }
	/*@TargetApi(Build.VERSION_CODES.M)
	private boolean getDeviceIdPermission() {

		int result = ActivityCompat.checkSelfPermission(this,
				Manifest.permission.READ_PHONE_STATE) + ActivityCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION)+ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
		return result == PackageManager.PERMISSION_GRANTED;
	}
	private void requestPermission() {

//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
//
//        }

		//And finally ask for the permission
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
	}*/


    /*Method used to check the runtime permissions for device id and read sms*/
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result4 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result5 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED &&
                result2 == PackageManager.PERMISSION_GRANTED &&
                result3 == PackageManager.PERMISSION_GRANTED &&
                result4 == PackageManager.PERMISSION_GRANTED &&
                result5 == PackageManager.PERMISSION_GRANTED;
    }

    /*Requesting for the required permissions*/
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{READ_PHONE_STATE, ACCESS_FINE_LOCATION, CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean readPhoneStateAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean fineLocationAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean readExternalStorageAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean writeExternalStorageAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (readPhoneStateAccepted && fineLocationAccepted && cameraAccepted && readExternalStorageAccepted && writeExternalStorageAccepted) {
                        isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
                        getSimSerialNumber();
                        SDPresent();
                    } else {
                        requestPermission();
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(CAMERA)) {
                            showMessageOKCancel("You need to allow access the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.M)
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermissions(new String[]{READ_PHONE_STATE, ACCESS_FINE_LOCATION, CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE},
                                                    PERMISSION_REQUEST_CODE);
                                        }
                                    });
                            return;
                        }
                    }

                }
                break;
        }
    }

    private void getSimSerialNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(getBaseContext().TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                SubscriptionManager subsManager = (SubscriptionManager) getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                List<SubscriptionInfo> subsList = subsManager.getActiveSubscriptionInfoList();
                if (subsList != null) {
                    for (SubscriptionInfo subsInfo : subsList) {
                        if (subsInfo != null) {
                            SIMSerialNumber = subsInfo.getIccId();
                        }
                    }
                }
            } else {
                SIMSerialNumber = telephonyManager.getSimSerialNumber();
            }
        }
        SharedPreferenceUtils.savePreference(getApplicationContext(), AppConstants.IPreferencesConstants.KEY_SIMSerialNumber, SIMSerialNumber);
    }

    /*showing alert message to accept the permissions*/
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(SplashScreen.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
