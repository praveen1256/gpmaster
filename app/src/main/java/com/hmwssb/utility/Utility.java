package com.hmwssb.utility;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

public class Utility {
    public static final int PERMISSION_REQUEST_CODE = 200;
    public static final int PERMISSION_REQUEST_CODE_CALL = 100;

    /*Method used to show the Toast message*/
    private static void showToastMessage(Context context, String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    /*Method used to show the log message*/
    private static void showLogMessage(String tag, String message) {
        if (tag != null && message != null) {
            Log.e(tag, message);
        }

    }

    /*Method used to find the the target sdk version greater than Naugat OS*/
    public static boolean isTargetSdkVersionGreaterThanNaugat() {
        return Build.VERSION.SDK_INT >= 24;
    }
}
