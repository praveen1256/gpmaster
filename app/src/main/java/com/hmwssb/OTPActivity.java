package com.hmwssb;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.HmwssbApplication;
import com.vajra.service.OperationCompleteListener;
import com.vajra.service.UrlConstants;
import com.vajra.service.model.GbContractorDetails;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;

public class OTPActivity extends SuperActivity implements AppConstants {

    private Button submitOtp;
    private EditText otpET;
    private ProgressDialog dialog;
    GbContractorDetails gbContractorDetails;
    private HmwssbApplication app;
    Transactions transobj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        gbContractorDetails = new GbContractorDetails();
        //transobj= new Transactions(this);

        transobj = new Transactions();
        try {
            transobj.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        otpET = (EditText) findViewById(R.id.otpEt);
        submitOtp = (Button) findViewById(R.id.SubmitOtp);
        Getversioncontrol versionasyn = new Getversioncontrol();
        versionasyn.execute();


        submitOtp.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                    if (otpET.getText().toString().length() != 0 && otpET.getText().toString() != "") {
                        getData();
                    } else {
                        //     otpET.setText("Please enter GBNO");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No NetWork", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void getData() {

        //TODO Show prgress bar
        final ProgressDialog mProgressDialog = ProgressDialog.show(OTPActivity.this, "", getString(R.string.progress_message),
                true, false);
        //getapp()
        getApp().getBoringServices().getGbContractorDetails(otpET.getText().toString(), "GetGBInfo", new OperationCompleteListener() {
            @Override
            public void operationComplete(boolean status, int code, String message) {
                //Cancel progress bar
                mProgressDialog.dismiss();

                Intent i = new Intent(getApplicationContext(), GetotpActivity.class);
                startActivity(i);
//				 Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
//               startActivity(i);
                //	unable to reach webservice please contact administrator
                //   getContractorDetails();
            }
        });
    }

    private void getContractorDetails() {
        GbContractorDetails gbContractorDetails = new GbContractorDetails();
        getApp().getBoringServices().getConnectionDetails(otpET.getText().toString(), "GetGBPendingFilesInfo", new OperationCompleteListener() {
            @Override
            public void operationComplete(boolean status, int code, String message) {
                //Cancel progress bar
                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                startActivity(i);
            }
        });

    }

    private class Getversioncontrol extends AsyncTask<Void, String, String> {
        String result;

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.GetVersioncontrol("GetLatestVersionNo");
                System.out.println("****************************Getversioncontrol " + result);


            } catch (Exception e) {
                System.out.println("exception in Getversioncontrol error" + e);
            }
            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                System.out.println("****************************result  Getversioncontrol" + result);
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
//                if (UrlConstants.APP_VERSION.contains(result) || UrlConstants.APP_VERSION.equals(result)) {
////                    Toast.makeText(getApplicationContext(), "The otpactivity  Version:" + result, Toast.LENGTH_LONG).show();
//					/*Intent i=new Intent(getApplicationContext(),HomeScreenTabActivity.class);
//						startActivity(i);
//						finish();*/
//                } else {
//
//                    AlertDialog.Builder alerdialog = new AlertDialog.Builder(OTPActivity.this);
//                    alerdialog.setTitle("Update Version Dialog");
//                    alerdialog.setMessage("Please Download New Version");
//                    alerdialog.setCancelable(false);
//                    alerdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//
//                        }
//                    });
//                    alerdialog.show();
//
//
//                }
            } catch (Exception e) {
                System.out.println("Exception Occurs Getversioncontrol " + e);
            }

        }
    }


}