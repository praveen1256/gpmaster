package com.hmwssb;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MeterDetailsActivity extends SuperActivity implements OnClickListener , AppConstants, LocationListener{


	Spinner mMeterMake,Meter_Size;
	TextView mFileNo,mAddress;
	EditText LatLanET,mSerialno,Initial_RadingET,WarrantyET;
	Button saveBtn,mMeterPhotoBtn;
	public ImageView signout, home;

	//
	private GPSTracker gps;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

	private Uri fileUri; // file url to store image/video

	protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
	protected static final int CAMERA_REQUEST = 0;
	static File   mediaStorageDir;
	Button b1;
	static File mediaFile;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	static ImageView iv,decodeIv;
	private static File DatePath;
	private Transactions transobj;
	//	public TextView dateTv;
	private String date;
	private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";
	StringBuilder ConnInfo = new StringBuilder();
	public Boolean Flag=true;
	//	private String mLongMeterdegree;
	private String longBoringminute;
	private String latBoringDegree;
	public byte[] byteArray;
	private byte[] handlePhoto;
	private String photo_Encoded;
	private String latgBoringMinute;
	private String latgBoringSecond;
	private String longBoringMilliSecond;
	private String latBoringMilliSecond;

	private String mLongMeterDegree;
	private String mLongMeterMinute;
	private String longMetersecond;
	private String mLatMeterDegree;
	private String mLatMeterMinute;
	private String latMeterSecond;
	//
	private String longMeterMilliSeconds;
	private String mLongMeterMilliSeconds;
	private String mLongMeterSeconds;
	private String mLatMeterSeconds;
	private String latMeterMilliSeconds;
	private String mLatMeterMilliSeconds;
	private String mFileno;
	private String ConnXml;
	private String mMeterDatetoServer;
	private String mMeterSanctionedPipeSize;
	private ProgressDialog mProgressDialog;
	ProgressDialog prog;
	Location loc; 
	LocationManager locationManager;
	private String compareValue;
	ArrayList<String> meterlist;
	String matermake;
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.meterdetails);
		Meter_Size=(Spinner) findViewById(R.id.Meter_Size);

		findViewsById();
		//transobj = new Transactions(this);
		transobj = new Transactions();
		try {
			transobj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}



		RelativeLayout layout = (RelativeLayout)findViewById(R.id.new_connection);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("Enter Meter Details");
		//


		//


		Bundle bundle = getIntent().getExtras();
		mFileno = bundle.getString(KEY_FILENO);  
		String Name = bundle.getString(KEY_NAME);  
		String Address = bundle.getString(KEY_Address);  
		mMeterSanctionedPipeSize=bundle.getString(KEY_SANCTIONEDPIPE);  
		mFileNo.setText("Fileno:"+mFileno+System.getProperty("line.separator")+"Name:"+Name);
		mAddress.setText("Address:"+Address);

		//
		Log.i("mFileno",mFileno);
		/*****************************get meter details*********************************
		 * sudhir
		 */
		meterlist = transobj.getmeter_details(mFileno);

		Log.i("meterlist",meterlist.toString());

		for (int i = 0; i < meterlist.size(); i++) {
			matermake=meterlist.get(2);

			mSerialno.setText(meterlist.get(3));
			Initial_RadingET.setText(meterlist.get(1));
			mLatMeterDegree=meterlist.get(4);
			mLatMeterMinute=meterlist.get(5);
			mLatMeterSeconds=meterlist.get(6);
			mLatMeterMilliSeconds=meterlist.get(7);
			mLongMeterDegree=meterlist.get(8);
			mLongMeterMinute=meterlist.get(9);
			mLongMeterSeconds=meterlist.get(10);
			mLongMeterMilliSeconds=meterlist.get(11);
			mMeterDatetoServer=meterlist.get(12);
			LatLanET.setText(mLatMeterDegree+mLatMeterMinute+mLatMeterSeconds+mLatMeterMilliSeconds+","+mLongMeterDegree+mLongMeterMinute+mLongMeterSeconds+mLongMeterMilliSeconds);

			WarrantyET.setText(mMeterDatetoServer);

			int sdk = android.os.Build.VERSION.SDK_INT;
			photo_Encoded=(meterlist.get(13));
			byte[] decodedString = Base64.decode(photo_Encoded, Base64.DEFAULT);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
			Drawable drawable = new BitmapDrawable(getResources(), decodedByte);
			mMeterPhotoBtn.setBackground(drawable);
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				mMeterPhotoBtn.setBackground(drawable);

			} else {
				mMeterPhotoBtn.setBackground(drawable);
			}


		}




		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {



			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				if (position == getCount()) {
					((TextView)v.findViewById(android.R.id.text1)).setText("");
					((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
				}


				return v;
			}       

			@Override
			public int getCount() {
				return super.getCount()-1; // you dont display last item. It is used as hint.
			}
		};
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		adapter.add("B-Meters GSD5");
		adapter.add("Zenner- Mino Mess ETX");
		adapter.add("Elster-S100");
		adapter.add("ITRON-Unimag");
		adapter.add("AMR- Multimag");
		adapter.add("AMR- Jalsonic");
		adapter.add("AMR- Woltex");
		adapter.add("Krescent");
		adapter.add("Chambel Magnetic");
		adapter.add("Zenner");
		adapter.add("Global");
		adapter.add("Select Meter Make");
		mMeterMake.setAdapter(adapter);
		mMeterMake.setSelection(adapter.getCount()); //display hint
		int position=0;
		while(position<adapter.getCount()){

			if(adapter.getItem(position).equals(matermake)){
				mMeterMake.setSelection(position);
				break;
			}
			position++;
		}

		ArrayAdapter<String> aa=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item){
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				if (position == getCount()) {
					((TextView)v.findViewById(android.R.id.text1)).setText("");
					((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
				}

				return v;
			}       

			@Override
			public int getCount() {
				return super.getCount()-1; // you dont display last item. It is used as hint.
			}
		};
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		if(mMeterSanctionedPipeSize!=null)
		{
			aa.add(mMeterSanctionedPipeSize);
		}

		aa.add("Enter Meter Size");
		Meter_Size.setAdapter(aa);

		Meter_Size.setSelection(0);

		mMeterPhotoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				captureImage();
			}
		});

	}

	public String getFileno() {
		return mFileno;
	}
	private boolean checkEnteredInput() {
		boolean flag=false;
		try
		{

			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo ", Toast.LENGTH_LONG).show();
				flag=true;
			}


			if(mSerialno.getText().toString().length()==0)
			{
				mSerialno.requestFocus();
				mSerialno.setError("FIELD CANNOT BE EMPTY");
				flag=true;
			}else {
				mSerialno.setError(null);
			}
			if(Initial_RadingET.length()==0)
			{
				Initial_RadingET.requestFocus();
				Initial_RadingET.setError("FIELD CANNOT BE EMPTY");
				flag=true;
			}

			else {
				Initial_RadingET.setError(null);
			}
			if( (mMeterMake.getSelectedItem().toString()).contains("Select Meter Make"))
			{
				Toast.makeText(getApplicationContext(), "Please Select Meter Make ", Toast.LENGTH_LONG).show();
				flag=true;
			}
			if(WarrantyET.getText().toString().length()==0  )
			{
				WarrantyET.requestFocus();

				WarrantyET.setError("Please Select Date");
				flag=true;
			}
			if(mMeterDatetoServer==null)
			{
				Toast.makeText(getApplicationContext(), "Please Select Date ", Toast.LENGTH_LONG).show();
				flag=true;
			}

		}

		catch(Exception e)
		{
			System.out.println("exception in meter details checkEnteredInput"+ e);  
		}

		return flag;
	}
	@Override
	public void onClick(View v) {


		switch (v.getId()) {
		case R.id.SAVE:
		{
			//if (!checkEnteredInput()) {
			//sudhir
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			String currentDateandTime = sdf.format(new Date());

			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo ", Toast.LENGTH_LONG).show();
				return ;
			}

			//				getBoringCoordinates();




			System.out.println("mLongMeterDegree:"+mLongMeterDegree+"mLongMeterMinute:"+mLongMeterMinute+"longMetersecond:"+longMetersecond);

			if(mLongMeterDegree==null||mLongMeterDegree.length()==0 || mLongMeterDegree.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Meterdetails longitudes and latitiudes is manditaory  ", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongMeterMinute==null||mLongMeterMinute.length()==0 || mLongMeterMinute.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Invalid Meterdetails longitudes Minute  ", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongMeterSeconds==null||mLongMeterSeconds.length()==0|| mLongMeterSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "  Invalid Meterdetails longitudes seconds  ", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongMeterMilliSeconds==null||mLongMeterMilliSeconds.length()==0 || mLongMeterMilliSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "  Invalid Meterdetails latitiudes Milliseconds  ", Toast.LENGTH_LONG).show();
				return;
			}


			System.out.println("mLatMeterDegree:"+mLatMeterDegree+"mLatMeterMinute:"+mLatMeterMinute+"latMeterSecond:"+latMeterSecond);


			if(mLatMeterDegree==null||mLatMeterDegree.length()==0||mLatMeterDegree.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Meterdetails longitudes and latitiudes is manditaory  ", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLatMeterMinute==null||mLatMeterMinute.length()==0|| mLatMeterMinute.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Invalid Meterdetails latitiudes Minutes  ", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLatMeterSeconds==null||mLatMeterSeconds.length()==0 ||mLatMeterSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "  Invalid Meterdetails latitiudes seconds  ", Toast.LENGTH_LONG).show();
				return;
			}

			//				if(mLatMeterMilliSeconds!=null||mLatMeterMilliSeconds.length()!=0 )
			if(mLatMeterMilliSeconds.contains("null") || mLatMeterMilliSeconds==null||mLatMeterMilliSeconds.length()==0 )
			{
				Toast.makeText(getApplicationContext(), "  Invalid Meterdetails latitiudes Milliseconds  ", Toast.LENGTH_LONG).show();
				return;
			}

			if(mSerialno.length()==0||mSerialno.equals("0"))
			{
				Toast.makeText(getApplicationContext(), "  Enter Serial Number ", Toast.LENGTH_LONG).show();
				return;
			}

			if(Initial_RadingET.length()==0||Initial_RadingET.equals("0")||Integer.parseInt(Initial_RadingET.getText().toString())>99)
			{
				Toast.makeText(getApplicationContext(), "  Invalid meter reading  must be greater than  zero and less than 100 ", Toast.LENGTH_LONG).show();
				return;
			}

			if( (mMeterMake.getSelectedItem().toString()).contains("Select Meter Make"))
			{
				Toast.makeText(getApplicationContext(), "Please Select Meter Make ", Toast.LENGTH_LONG).show();
				return ;
			}
			if(WarrantyET.getText().toString().length()==0)
			{
				WarrantyET.requestFocus();
				Toast.makeText(getApplicationContext(), "Please Select Warranty Date ", Toast.LENGTH_LONG).show();
				return ;


			}


			if(mMeterDatetoServer==null)
			{
				Toast.makeText(getApplicationContext(), "Please Select Date ", Toast.LENGTH_LONG).show();
				return;
			}



			/*mLongMeterDegree=LatLanET.getText().toString();

				if(mLongMeterDegree==null)
				{
					Toast.makeText(getApplicationContext(), "Lat And Long Values Null", Toast.LENGTH_LONG).show();
					return;	
				}

				if(mLongMeterDegree=="0.0" || mLongMeterDegree=="0")
				{
					Toast.makeText(getApplicationContext(), "Lat And Long Values Null", Toast.LENGTH_LONG).show();
					return;	
				}*/
			try
			{
				if(meterlist.isEmpty())
				{
					/************************insert MeterDetailsInfoCreateByUid********************
					 * sudhir
					 */

					PostConnectionInfo();
					ContentValues PostMeterDetailsvalues = new ContentValues();
					PostMeterDetailsvalues.put("FileNo", mFileno);
					PostMeterDetailsvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostMeterDetailsvalues.put("MeterInitialReading", Initial_RadingET.getText().toString());
					PostMeterDetailsvalues.put("MeterLatitudeDegrees", mLatMeterDegree);
					PostMeterDetailsvalues.put("MeterLatitudeMilliSeconds", mLatMeterMilliSeconds);
					PostMeterDetailsvalues.put("MeterLatitudeMinutes", mLatMeterMinute);
					PostMeterDetailsvalues.put("MeterLatitudeSeconds", mLatMeterSeconds);
					PostMeterDetailsvalues.put("MeterLongitudeDegrees", mLongMeterDegree);
					PostMeterDetailsvalues.put("MeterLongitudeMilliSeconds", mLongMeterMilliSeconds);
					PostMeterDetailsvalues.put("MeterLongitudeMinutes", mLongMeterMinute);
					PostMeterDetailsvalues.put("MeterLongitudeSeconds", mLongMeterSeconds);
					PostMeterDetailsvalues.put("MeterMake", mMeterMake.getSelectedItem().toString());
					PostMeterDetailsvalues.put("MeterNo", mSerialno.getText().toString());
					PostMeterDetailsvalues.put("MeterPhoto", photo_Encoded);
					PostMeterDetailsvalues.put("MeterWarrantyValidUpto", mMeterDatetoServer);
					PostMeterDetailsvalues.put("MeterDetailsInfoXMLString",ConnXml);
					PostMeterDetailsvalues.put("MeterDetailsInfoCreatedDtm", currentDateandTime);
					PostMeterDetailsvalues.put("MeterDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostMeterDetailsvalues.put("MeterServerStatus", "finished");
					transobj.insert_PostMeterDetailsInfo(PostMeterDetailsvalues);

					Intent i = new Intent(MeterDetailsActivity.this, NewConnection.class);

					startActivity(i);
					setResult(METER_CONNECTION);
					finish();
				}
				else
				{
					/********************************update meter details**********************
					 * sudhir
					 */
					PostConnectionInfo();
					ContentValues PostMeterDetailsvalues = new ContentValues();
					PostMeterDetailsvalues.put("FileNo", mFileno);
					PostMeterDetailsvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostMeterDetailsvalues.put("MeterInitialReading", Initial_RadingET.getText().toString());
					PostMeterDetailsvalues.put("MeterLatitudeDegrees", mLatMeterDegree);
					PostMeterDetailsvalues.put("MeterLatitudeMilliSeconds", mLatMeterMilliSeconds);
					PostMeterDetailsvalues.put("MeterLatitudeMinutes", mLatMeterMinute);
					PostMeterDetailsvalues.put("MeterLatitudeSeconds", mLatMeterSeconds);
					PostMeterDetailsvalues.put("MeterLongitudeDegrees", mLongMeterDegree);
					PostMeterDetailsvalues.put("MeterLongitudeMilliSeconds", mLongMeterMilliSeconds);
					PostMeterDetailsvalues.put("MeterLongitudeMinutes", mLongMeterMinute);
					PostMeterDetailsvalues.put("MeterLongitudeSeconds", mLongMeterSeconds);
					PostMeterDetailsvalues.put("MeterMake", mMeterMake.getSelectedItem().toString());
					PostMeterDetailsvalues.put("MeterNo", mSerialno.getText().toString());
					PostMeterDetailsvalues.put("MeterPhoto", photo_Encoded);
					PostMeterDetailsvalues.put("MeterWarrantyValidUpto", mMeterDatetoServer);
					PostMeterDetailsvalues.put("MeterDetailsInfoXMLString",ConnXml );
					PostMeterDetailsvalues.put("MeterDetailsInfoCreatedDtm", currentDateandTime);
					PostMeterDetailsvalues.put("MeterDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostMeterDetailsvalues.put("MeterServerStatus", "finished");
					transobj.update_PostMeterDetails(PostMeterDetailsvalues, mFileno);

					Intent i = new Intent(MeterDetailsActivity.this, NewConnection.class);
					i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
					startActivity(i);
					setResult(METER_CONNECTION);
					finish();
				}
			}
			catch(Exception e)
			{
				System.out.println("exception in meter details insert button"+ e);
			}

			setResult(METER_CONNECTION);
			finish();
		}

		//}
		break;

		default:
			break;
		}
	}

	private void findViewsById(){
		mFileNo=(TextView) findViewById(R.id.cust_fileno);
		mAddress=(TextView) findViewById(R.id.cust_address);
		mMeterMake=(Spinner) findViewById(R.id.meter_make);
		LatLanET=(EditText) findViewById(R.id.lat_long_point_photo);
		mSerialno=(EditText) findViewById(R.id.serial_number);
		Initial_RadingET=(EditText) findViewById(R.id.Initial_Reading);
		WarrantyET=(EditText) findViewById(R.id.Warranty);
		mMeterPhotoBtn=(Button) findViewById(R.id.meter_photo_point);
		saveBtn=(Button)findViewById(R.id.SAVE);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				finish();
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//			    logoutFromApp();
				finish();
				System.exit(0);
				//android.os.Process.killProcess(android.os.Process.myPid());



			}
		});

		saveBtn.setOnClickListener(this);
		WarrantyET.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DatePickerDialog(MeterDetailsActivity.this, date1, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});


	}

	private void updateLabel() {

		/*	String myFormat = "dd/MM/yyyy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);*/
		String myFormat = "dd/MM/yyyy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
		Calendar cal = Calendar.getInstance();
		String currenttime = new String(sdf.format(cal.getTime()));
		try {
			Date currentDate = sdf.parse(currenttime);
			String userBoringDate = new String(sdf.format(myCalendar.getTime()));
			Date boringDateset=sdf.parse(userBoringDate);
			//		mMeterDatetoServer=sdf.format(myCalendar.getTime());
			if (System.currentTimeMillis() < boringDateset.getTime()) {
				mMeterDatetoServer=sdf.format(myCalendar.getTime());

			}else {
				mMeterDatetoServer=sdf.format(cal.getTime());
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		WarrantyET.setText(mMeterDatetoServer);
	}

	Calendar myCalendar = Calendar.getInstance();

	DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}

	};
	private String lat;
	private String longi;
	private Location oldLocation;

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

		//		getBoringCoordinates();
	}
	private void getBoringCoordinates() {


		gpsFinding();
		/*
		gps = new GPSTracker(this);


		// check if GPS enabled
		if(gps.canGetLocation()){



			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();

			lat=  String.valueOf(latitude);
			longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);


			try {

				String[] dmsMeterLong = strLongitude.split(":");

				mLongMeterDegree = dmsMeterLong[0];
				mLongMeterMinute = dmsMeterLong[1];
				longMetersecond = dmsMeterLong[2];

				String[] longMeterMilliSec = longMetersecond.split("\\.");

				mLongMeterSeconds = longMeterMilliSec[0];
				longMeterMilliSeconds = longMeterMilliSec[1];
				mLongMeterMilliSeconds=longMeterMilliSeconds.substring(0, 2);
				//			longBoringMilliSecond=dmsBoringLong[3];

				//latitude split
				String[] dmsMeterLat = strLatitude.split(":");
				mLatMeterDegree = dmsMeterLat[0];
				mLatMeterMinute = dmsMeterLat[1];
				latMeterSecond = dmsMeterLat[2];

				String[] latMeterMilliSec = latMeterSecond.split("\\.");

				mLatMeterSeconds = latMeterMilliSec[0];
				latMeterMilliSeconds = latMeterMilliSec[1];
				mLatMeterMilliSeconds=latMeterMilliSeconds.substring(0, 2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try
			{
				//	if (filDate.length<4) {

				File logFile = new File("/sdcard/gpstest.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				String dateNow = lat + longi;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();

			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();


		}else{
			gps.showSettingsAlert();
		}

		 */}
	public void gpsFinding() {
		try {
			loc = null;
			prog = new ProgressDialog(this);
			prog.setTitle("Please Wait...!");
			prog.setMessage("Searching for GPS Coordinates...");
			prog.setCancelable(false);
			prog.show();

			locationManager = (LocationManager)getSystemService("location");

			//			LocationListener locationListener = new MyLocationListener();
						locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1L, 1F, this);
			//locationManager.requestLocationUpdates("gps", 1L, 1F, this);


			if (locationManager != null)
			{
				oldLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}


	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}


		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
			DatePath= mediaFile;

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	private void previewCapturedImage() {
		try {

			ExifInterface exif = null ;
			try {
				exif = new ExifInterface(DatePath.getPath());
				date=exif.getAttribute(ExifInterface.TAG_DATETIME);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			getPhotoToByteArray();
			getBoringCoordinates();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public byte [] getPhotoToByteArray() {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(mediaFile.getPath(),
				options);
		bitmap=fixOrientation(bitmap);
		try {

			//Encode to Stringche
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
			Drawable drawable = new BitmapDrawable(getResources(), bitmap);
			int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				mMeterPhotoBtn.setBackgroundDrawable(drawable);
				mMeterPhotoBtn.setText(" ");
				//				boringPointPhoto.setText(" ");
			} else {
				mMeterPhotoBtn.setBackgroundDrawable(drawable);
				mMeterPhotoBtn.setText(" ");
				//				boringPointPhoto.setText(" ");
			}
			//mMeterPhotoBtn.setBackground(drawable);
			byteArray = byteArrayOutputStream .toByteArray();
			photo_Encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
			//				dateTv.setText(encoded);
			handlePhoto=byteArray;

		} catch (Exception e) {
			System.out.println("exception in meter details byte error"+ e);
		}
		return byteArray;

	}

	public Bitmap fixOrientation(Bitmap mBitmap) {
		if (mBitmap.getWidth() > mBitmap.getHeight()) {
			Matrix matrix = new Matrix();
			matrix.postRotate(90);
			mBitmap = Bitmap.createBitmap(mBitmap , 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		}
		return mBitmap;
	}

	private class AsyncCallWS extends AsyncTask<Void, Void, String> {

		String result;

		//		private ProgressDialog mProgressDialog;
		//	AE7654
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*mProgressDialog = ProgressDialog.show(MeterDetailsActivity.this, "",
					"Loading. Please wait...", true);*/
		}

		//	AE7654

		@Override
		protected String doInBackground(Void... params) {
			try {
				System.out.println("in PostMeterDetailsInfo ConnXml"+ConnXml);
				result= GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostMeterDetailsInfo");

			} catch (Exception e) {
				// TODO: handle exception
			}


			//GbPostPendingDataToServer.invokePendingFiles();
			//		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			//			mProgressDialog.dismiss();
			String splitresult=null;

			try {
				ContentValues PostMeterDetailsvalues = new ContentValues();
				if(result.contains("failed"))
				{
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
					PostMeterDetailsvalues.put("MeterDetailsInfoWebServiceStatus", result);
					System.out.println("*******mFileno**************filenumber"+mFileno);
					transobj.update_PostMeterDetails(PostMeterDetailsvalues, mFileno);


				}
				else
				{
					String arr[]=result.split("|");
					splitresult=arr[1];
					System.out.println("the split result valuesa arr[0]:"+result+"******************the split values of arr[1]:"+arr[1]);

				}

				if(result.contains("0"))
				{
					Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
				}


				if(result.contains("1"))
				{


					PostMeterDetailsvalues.put("MeterDetailsInfoWebServiceStatus", splitresult);
					System.out.println("*******mFileno**************filenumber"+mFileno);
					transobj.update_PostMeterDetails(PostMeterDetailsvalues, mFileno);
					Toast.makeText(getApplicationContext(), "Meter Details Successfuly Sent", Toast.LENGTH_SHORT).show();
				}


			} catch (Exception e) {
				System.out.println("exception in AsyncCallWS meter details byte error"+ e);

			}


		}

	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	private void PostConnectionInfo() {
		ConnInfo.append("<ConnInfo>");

		ConnInfo.append("<FileNo>");
		ConnInfo.append(mFileno);
		ConnInfo.append("</FileNo>");

		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<MeterLatitudeDegrees>");
		ConnInfo.append(mLatMeterDegree);
		ConnInfo.append("</MeterLatitudeDegrees>");

		ConnInfo.append("<MeterLatitudeMinutes>");
		ConnInfo.append(mLatMeterMinute);
		ConnInfo.append("</MeterLatitudeMinutes>");


		ConnInfo.append("<MeterLatitudeSeconds>");
		ConnInfo.append(mLatMeterSeconds);
		ConnInfo.append("</MeterLatitudeSeconds>");

		ConnInfo.append("<MeterLatitudeMilliSeconds>");
		ConnInfo.append(mLatMeterMilliSeconds);
		ConnInfo.append("</MeterLatitudeMilliSeconds>");


		ConnInfo.append("<MeterLongitudeDegrees>");
		ConnInfo.append(mLongMeterDegree);
		ConnInfo.append("</MeterLongitudeDegrees>");

		ConnInfo.append("<MeterLongitudeMilliSeconds>");
		ConnInfo.append(mLongMeterMilliSeconds);
		ConnInfo.append("</MeterLongitudeMilliSeconds>");

		ConnInfo.append("<MeterLongitudeMinutes>");
		ConnInfo.append(mLongMeterMinute);
		ConnInfo.append("</MeterLongitudeMinutes>");

		ConnInfo.append("<MeterLongitudeSeconds>");
		ConnInfo.append(mLongMeterSeconds);
		ConnInfo.append("</MeterLongitudeSeconds>");

		ConnInfo.append("<MeterMake>");
		ConnInfo.append(mMeterMake.getSelectedItem().toString());
		ConnInfo.append("</MeterMake>");

		ConnInfo.append("<MeterInitialReading>");
		ConnInfo.append(Initial_RadingET.getText().toString());
		ConnInfo.append("</MeterInitialReading>");

		ConnInfo.append("<MeterNo>");
		ConnInfo.append(mSerialno.getText().toString());
		ConnInfo.append("</MeterNo>");

		ConnInfo.append("<MeterPhoto>");
		ConnInfo.append(photo_Encoded);
		ConnInfo.append("</MeterPhoto>");

		ConnInfo.append("<MeterSize>");
		ConnInfo.append(Meter_Size.getSelectedItem().toString());
		ConnInfo.append("</MeterSize>");

		ConnInfo.append("<MeterWarrantyValidUpto>");
		ConnInfo.append(mMeterDatetoServer);
		ConnInfo.append("</MeterWarrantyValidUpto>");


		ConnInfo.append("</ConnInfo>");

		ConnXml=ConnInfo.toString();

		Log.v(ConnInfo.toString(), "XML");

		CheckNetworkStatus(mFileno);

		AsyncCallWS task = new AsyncCallWS();
		//Call execute 
		task.execute();
	}

	@Override
	public void onLocationChanged(Location location) {
		Toast.makeText(getApplicationContext(), "accuracy"+location.getAccuracy(), 0).show();

		// check if GPS enabled		
		if (location != null/*&&location.getAccuracy()<14*/) {

			loc = location;

			double latitude = loc.getLatitude(); 
			double longitude = loc.getLongitude();



			lat=  String.valueOf(latitude);
			longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);


			try {

				String[] dmsMeterLong = strLongitude.split(":"); 

				mLongMeterDegree = dmsMeterLong[0];
				mLongMeterMinute = dmsMeterLong[1];
				longMetersecond = dmsMeterLong[2];

				String[] longMeterMilliSec = longMetersecond.split("\\."); 

				mLongMeterSeconds = longMeterMilliSec[0];
				longMeterMilliSeconds = longMeterMilliSec[1];
				mLongMeterMilliSeconds=longMeterMilliSeconds.substring(0, 2);
				//			longBoringMilliSecond=dmsBoringLong[3];

				//latitude split
				String[] dmsMeterLat = strLatitude.split(":"); 
				mLatMeterDegree = dmsMeterLat[0];
				mLatMeterMinute = dmsMeterLat[1];
				latMeterSecond = dmsMeterLat[2];

				String[] latMeterMilliSec = latMeterSecond.split("\\."); 

				mLatMeterSeconds = latMeterMilliSec[0];
				latMeterMilliSeconds = latMeterMilliSec[1];
				mLatMeterMilliSeconds=latMeterMilliSeconds.substring(0, 2);
			} catch (Exception e) {
				// TODO: handle exception
			}
			try
			{
				//	if (filDate.length<4) {

				File logFile = new File("/sdcard/gps.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();                    
					} 
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}         

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
				String dateNow = lat + longi;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();              

				LatLanET.setText(strLatitude+","+strLongitude);
				Toast.makeText(this, "GPS Coordinates Received....", 0).show(); 
				locationManager.removeUpdates(this);
				prog.dismiss();

			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/*
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();	
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();	
			 */

		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
}

