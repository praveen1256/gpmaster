//***************************************************************************************************
//***************************************************************************************************
//      Project name                    		: Sharp ePop
//      Class Name                              : SuperActivity
//      Author                                  : PurpleTalk, Inc.
//***************************************************************************************************
//      Description: Base class for all Activities.
//***************************************************************************************************
//***************************************************************************************************
package com.hmwssb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.HmwssbApplication;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;

/**
 * Base class for all Activities.
 *
 * @author PurpleTalk, Inc.
 */
public class SuperActivity extends Activity {
    private AlertDialog alertDialog;
    Transactions mTransObj;
    String mDisconnectionxml, mBoringxml, mMeterxml, mCangeratexml, fileNum, mCangeneratexml, mBuildingxml, mcustomerfeedbackxxml, mbillxml;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //mTransObj = new Transactions(getApplicationContext());

        mTransObj = new Transactions();
        try {
            mTransObj.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
//		fileNum=	SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.FILENO, "");
        System.out.println("*******filenumber**************filenumber" + fileNum);
    }


    public HmwssbApplication getApp() {
        return (HmwssbApplication) getApplication();
    }

    public void showDialog(String msgToShow) {
        alertDialog.setMessage(msgToShow);
        alertDialog.show();
    }

    @SuppressWarnings("unused")
    private void cancelDialog() {
        alertDialog.cancel();
    }

    protected void launchActivity(Class<?> activityClass, Bundle mBundle) {
        Intent intent = new Intent(SuperActivity.this, activityClass);
        if (mBundle != null) {
            intent.putExtras(mBundle);
        }
        startActivity(intent);
    }

    public String CheckNetworkStatus(String fileNum) {
        try {
            //Toast.makeText(getApplicationContext(), "in super activity", Toast.LENGTH_LONG).show();
            System.out.println("*****************Sudhir****************" + fileNum);

            this.fileNum = fileNum;

            boolean isServiceAvailable = AppUtils.isNetworkAvailable(getApplicationContext());
            if (isServiceAvailable) {
                String mDisConnFileStatus, mMeterFileStatus, mCangenerateStatus, mCustomerfeedbackStatus, mBuildingStatus, mCangeneratebillStatus;
                String mBoringFileStatus;

                mDisConnFileStatus = mTransObj.getWebServiceStatusDisconnection(fileNum);
                System.out.println("getting disconnection details status" + mDisConnFileStatus);
                mMeterFileStatus = mTransObj.getWebServicePostMeterDetailsStatus(fileNum);
                System.out.println("getting mMeterFileStatus details status" + mMeterFileStatus);
                mCangenerateStatus = mTransObj.getWebServiceCanGenerateStatus(fileNum);
                System.out.println("getting mCangenerateStatus details status" + mCangenerateStatus);
                mCustomerfeedbackStatus = mTransObj.getWebServiceCustomerFeedBackStatus(fileNum);
                System.out.println("getting mCustomerfeedbackStatus details status" + mCustomerfeedbackStatus);
                mBuildingStatus = mTransObj.getWebServicePostBuildingDetailsStatus(fileNum);
                mCangeneratebillStatus = mTransObj.getWebServiceCanGenerateGBBillStatus(fileNum);
                String type = mTransObj.get_Applicationtype(fileNum);
                mBoringFileStatus = mTransObj.getWebServiceBoaringStatus(fileNum);
                System.out.println("getting mBoringFileStatus details status" + mBoringFileStatus);
                try {
                    if (type.contains("E")) {
                        System.out.println("******************************get_Applicationtype" + type);

                        mDisconnectionxml = mTransObj.getdisconnectionxml(fileNum);
                        System.out.println("*********************mDisconnectionxml" + mDisconnectionxml);
                        if (mDisConnFileStatus.contains("0") || mDisConnFileStatus.contains("failed")) {
                            DisAsyncCallWS disasyn = new DisAsyncCallWS();
                            disasyn.execute();
                        }
                        if (mDisconnectionxml.contains("1") && mBoringFileStatus.contains("0") || mDisconnectionxml.contains("1") && mBoringFileStatus.contains("failed")) {

                            mBoringxml = mTransObj.getboringxml(fileNum);
                            System.out.println("*********************mBoringxml" + mBoringxml);
                            BoaringAsyncCallWS borasynobj = new BoaringAsyncCallWS();
                            borasynobj.execute();
                        }

                    } else if (mBoringFileStatus.contains("0") || mBoringFileStatus.contains("failed")) {

                        mBoringxml = mTransObj.getboringxml(fileNum);
                        System.out.println("*********************mBoringxml" + mBoringxml);
                        BoaringAsyncCallWS borasynobj = new BoaringAsyncCallWS();
                        borasynobj.execute();
                    }


                    String meterstaus = mTransObj.getmeteravailable(fileNum);


                    if (meterstaus.contains("true")) {
                        //send meter if failed
                        if (mBoringFileStatus.contains("1") && mMeterFileStatus.contains("0") || mMeterFileStatus.contains("failed")) {
                            mMeterxml = mTransObj.getmeterxml(fileNum);
                            System.out.println("*********************mMeterxml" + mMeterxml);
                            MeterAsyncCallWS meterasyn = new MeterAsyncCallWS();
                            meterasyn.execute();

                            // 2013-8-1774
                        }
                        //generate can
                        if (mBoringFileStatus.contains("1") && mMeterFileStatus.contains("1") && mCangenerateStatus.contains("0") || mBoringFileStatus.contains("1") && mMeterFileStatus.contains("1") && mCangenerateStatus.contains("failed") || mBoringFileStatus.contains("1") && mMeterFileStatus.contains("1") && mCangenerateStatus == null || mBoringFileStatus.contains("1") && mMeterFileStatus.contains("1") && mCangenerateStatus.length() == 0) {

//						mCangeratexml = mTransObj.cangeneratexml(fileNum);
                            CanGenAsyncCallTo canasyn = new CanGenAsyncCallTo();
                            canasyn.execute();

//						mCangeratexml=mTransObj.getmeterxml(fileNum);


                        }
                        //send customer feeb back
                        mCangenerateStatus = mTransObj.getWebServiceCanGenerateStatus(fileNum);
                        if (mCustomerfeedbackStatus == null || mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("0") || mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("Customer Feed Back Details Info was not posted") || mCustomerfeedbackStatus.contains("failed")) {


                            mcustomerfeedbackxxml = mTransObj.customerfeedbackxml(fileNum);
                            CustomerAsyncCallWS custmerasyn = new CustomerAsyncCallWS();
                            custmerasyn.execute();
                            //send building details

                        }
                        if (mCustomerfeedbackStatus == null || mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("failed") || mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("Building Details Info was not posted")) {
                            mBuildingxml = mTransObj.buildingxml(fileNum);
                            //						Toast.makeText(getApplicationContext(), "in bill asyntask", 100).show();
                            BuildingAsyncCallWS buildingasyn = new BuildingAsyncCallWS();
                            buildingasyn.execute();

                        }


					/*else if(mCangenerateStatus.contains("1") && mBuildingStatus.contains("1") ||mCustomerfeedbackStatus.contains("failed"))
					{
						mcustomerfeedbackxxml=mTransObj.customerfeedbackxml(fileNum);
						CustomerAsyncCallWS custmerasyn= new CustomerAsyncCallWS();
						custmerasyn.execute();

					}*/
					/*	else if(mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("1") ||mBuildingStatus.contains("failed"))
					{
						mBuildingxml=mTransObj.buildingxml(fileNum);
						BuildingAsyncCallWS buildingasyn=new BuildingAsyncCallWS();
						buildingasyn.execute();

					}*/
                        // send GB Bill
                        if (mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("1") && mCangeneratebillStatus == null || mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("1") && mCangeneratebillStatus.contains("failed")) {
                            mbillxml = mTransObj.cangbbillxml(fileNum);

                            AsyncCallToGbBill buildingasyn = new AsyncCallToGbBill();
                            buildingasyn.execute();

                        }
					/*else if(mCangeneratebillStatus.contains("failed"))

					{
						AsyncCallToGbBill  buildingasyn=new  AsyncCallToGbBill ();
						buildingasyn.execute();
					}*/


                    } else {
                        if (meterstaus.contains("false")) {
                            if (mMeterFileStatus == null) {

                                System.out.println("mMeterFileStatus==null **********************************************");

                                if (mBoringFileStatus.contains("1")) {
                                    if (mCangenerateStatus.contains("1")) {

                                    } else {
                                        CanGenAsyncCallTo canasyn = new CanGenAsyncCallTo();
                                        canasyn.execute();
                                    }

                                }
                                //generate Can

                                if (mBoringFileStatus.contains("1") && mCangenerateStatus.contains("0") || mBoringFileStatus.contains("1") && mCangenerateStatus.contains("failed") || mBoringFileStatus.contains("1") && mCangenerateStatus == null) {

                                    CanGenAsyncCallTo canasyn = new CanGenAsyncCallTo();
                                    canasyn.execute();
                                }
                                //send customer feeb back
                                if (mCustomerfeedbackStatus == null || mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("0") || mCustomerfeedbackStatus.contains("failed")) {
                                    mcustomerfeedbackxxml = mTransObj.customerfeedbackxml(fileNum);
                                    CustomerAsyncCallWS custmerasyn = new CustomerAsyncCallWS();
                                    custmerasyn.execute();

                                }
                                if (mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("failed") || mCustomerfeedbackStatus == null) {
                                    mBuildingxml = mTransObj.buildingxml(fileNum);
                                    //						Toast.makeText(getApplicationContext(), "in bill asyntask", 100).show();
                                    BuildingAsyncCallWS buildingasyn = new BuildingAsyncCallWS();
                                    buildingasyn.execute();

                                }


                                // send GB Bill
                                if (mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("1") && mCangeneratebillStatus == null) {

                                    Toast.makeText(getApplicationContext(), " send GB Bill", Toast.LENGTH_LONG).show();
                                    mbillxml = mTransObj.cangbbillxml(fileNum);
                                    Log.e("mbillxml", mbillxml);
                                    AsyncCallToGbBill buildingasyn = new AsyncCallToGbBill();
                                    buildingasyn.execute();

                                }
							/*else if(mCangeneratebillStatus.contains("failed"))

							{
								AsyncCallToGbBill  buildingasyn=new  AsyncCallToGbBill ();
								buildingasyn.execute();
							}*/

							/*else if (mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("0") ||mCustomerfeedbackStatus.contains("failed")||mBuildingStatus==null) {


								mcustomerfeedbackxxml=mTransObj.customerfeedbackxml(fileNum);
								CustomerAsyncCallWS custmerasyn= new CustomerAsyncCallWS();
								custmerasyn.execute();


							}



							else if(mCangenerateStatus.contains("1")  ||mBuildingStatus.contains("failed")||mCustomerfeedbackStatus==null)
							{
								mBuildingxml=mTransObj.buildingxml(fileNum);
								BuildingAsyncCallWS buildingasyn=new BuildingAsyncCallWS();
								buildingasyn.execute();

							}
							else if(mCangenerateStatus.contains("1") && mBuildingStatus.contains("1") ||mCustomerfeedbackStatus.contains("failed"))
							{
								mcustomerfeedbackxxml=mTransObj.customerfeedbackxml(fileNum);
								CustomerAsyncCallWS custmerasyn= new CustomerAsyncCallWS();
								custmerasyn.execute();

							}
							else if(mCangenerateStatus.contains("1") && mCustomerfeedbackStatus.contains("1") ||mBuildingStatus.contains("failed"))
							{
								mBuildingxml=mTransObj.buildingxml(fileNum);
								BuildingAsyncCallWS buildingasyn=new BuildingAsyncCallWS();
								buildingasyn.execute();

							}
							else if(mCustomerfeedbackStatus.contains("1") && mBuildingStatus.contains("1"))
							{

								AsyncCallToGbBill  buildingasyn=new  AsyncCallToGbBill ();
								buildingasyn.execute();

							}
							else if(mCustomerfeedbackStatus.contains("1")||mBuildingStatus.contains("1"))
							{
								Toast.makeText(getApplicationContext(), "in bill asyntask", 100).show();
								AsyncCallToGbBill  buildingasyn=new  AsyncCallToGbBill ();
								buildingasyn.execute();

							}
							else if(mCangeneratebillStatus.contains("failed"))

							{
								AsyncCallToGbBill  buildingasyn=new  AsyncCallToGbBill ();
								buildingasyn.execute();
							}*/

                            }
                        }
                        // if meter is false

					/*	else
					{
						if (mBoringFileStatus.contains("1") && mMeterFileStatus.contains("0") ||mMeterFileStatus.contains("failed")) {
							mMeterxml=mTransObj.getmeterxml(fileNum);
							MeterAsyncCallWS meterasyn=new MeterAsyncCallWS();
							meterasyn.execute();
						}
						if ( mCangenerateStatus.contains("0") ||mCangenerateStatus.contains("failed")) {
							mCangeratexml=mTransObj.getmeterxml(fileNum);
						}
					}*/
                    }
                } catch (Exception e) {
                    System.out.print(e.toString());
                }







				/*
				if (mMeterFileStatus.contains("1") && mCangenerateStatus.contains("0") ||mCangenerateStatus.contains("failed")) {
					mMeterxml=mTransObj.getmeterxml(fileNum);
					MeterAsyncCallWS meterasyn=new MeterAsyncCallWS();
					meterasyn.execute();
				}
				else if (mMeterFileStatus.contains("1") && mCangenerateStatus.contains("0") ||mCangenerateStatus.contains("failed")) {
					mCangeneratexml=mTransObj.cangeneratexml(fileNum);
					CanGenAsyncCallTo canasyn=new CanGenAsyncCallTo();
					canasyn.execute();
				}
				 */


            } else {
                Toast.makeText(getApplicationContext(), "Please On Internet", 100).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception Occurs CheckNetworkStatus in superactivity" + e);
        }
        return "hai";


    }


    public String sendBoreMeterDetails(String fileno) {
        try {
            String meterstaus = mTransObj.getmeteravailable(fileNum);
            if (meterstaus.contains("true")) {
                mBoringxml = mTransObj.getboringxml(fileno);
                System.out.println("*********************mBoringxml" + mBoringxml);
                BoaringAsyncCallWS borasynobj = new BoaringAsyncCallWS();
                borasynobj.execute();
                mMeterxml = mTransObj.getmeterxml(fileno);
                System.out.println("*********************mMeterxml" + mMeterxml);
                MeterAsyncCallWS meterasyn = new MeterAsyncCallWS();
                meterasyn.execute();
            } else {
                mBoringxml = mTransObj.getboringxml(fileno);
                System.out.println("*********************mBoringxml" + mBoringxml);
                BoaringAsyncCallWS borasynobj = new BoaringAsyncCallWS();
                borasynobj.execute();
            }


        } catch (Exception e) {
            System.out.print(e.toString());
        }
        return "";
    }

    public String sendFeedBuilddetails(String fileno) {
        try {
            mcustomerfeedbackxxml = mTransObj.customerfeedbackxml(fileno);
            CustomerAsyncCallWS custmerasyn = new CustomerAsyncCallWS();
            custmerasyn.execute();
            mBuildingxml = mTransObj.buildingxml(fileno);
            BuildingAsyncCallWS buildingasyn = new BuildingAsyncCallWS();
            buildingasyn.execute();

        } catch (Exception e) {
            System.out.print(e.toString());
        }
        return "";
    }

    public void checkstatus() {

    }

    /***************************Disconnection*********************************************
     *
     * @author
     *
     */
    private class DisAsyncCallWS extends AsyncTask<Void, String, String> {
        String result;

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.invokePendingFiles(mDisconnectionxml, "PostDisconnectionDetailsInfo");
                System.out.println("****************************result values of PostDisconnectionDetailsInfo" + result);
                onPostExecute(result);
                System.out.println("****************************result values of ConnXml" + mDisconnectionxml);
            } catch (Exception e) {
                System.out.println("exception in DisAsyncCallWS error" + e);
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            String spliteresult = null;
            try {
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet", Toast.LENGTH_LONG).show();
                    ContentValues updatedisconnection = new ContentValues();
                    updatedisconnection.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                    updatedisconnection.put("webservicestatus", result);
                    mTransObj.update_PostDisconnectionDetails(updatedisconnection, fileNum);
                } else {
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);
                }
                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();
                }
                if (spliteresult.contains("1")) {
                    Toast.makeText(getApplicationContext(), "Disconnection details SuccessFully Sent", 500).show();
                    ContentValues updatedisconnection = new ContentValues();
                    // PostDisconnectionDetailsvalues.put("FileNo", mCustomerFileno);
                    updatedisconnection.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                    updatedisconnection.put("DisconnectionDetailsInfoWebServiceStatus", spliteresult);
                    mTransObj.update_PostDisconnectionDetails(updatedisconnection, fileNum);
                }

            } catch (Exception e) {
                System.out.println("Exception Occurs PostDisconnectionDetailsInfo in superactivity" + e);
            }


        }

    }

    /*************************************************BOARING*********************************
     *
     */
    private class BoaringAsyncCallWS extends AsyncTask<Void, Void, String> {
        String result;

        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			/*	mProgressDialog = ProgressDialog.show(Boaringdetails.this, "", 
					"Loading. Please wait...", true);*/
        }

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                System.out.println("********************* in  super activity Boaring xml" + "," + mBoringxml);
                result = GbPostPendingDataToServer.invokePendingFiles(mBoringxml, "PostBoringDetailsInfo");
                System.out.println("********************* in  super activity result xml:" + "," + result);

            } catch (Exception e) {
            }


            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //			mProgressDialog.dismiss();
            String spliteresult = null;
            try {
                if (result.contains("failed")) {
                    ContentValues PostBoringDetailsvalues = new ContentValues();
                    PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", result);
                    mTransObj.update_PostBoringDetails(PostBoringDetailsvalues, fileNum);
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();

                } else {
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);

                }

                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();	

                }

                if (spliteresult.contains("1")) {
                    Toast.makeText(getApplicationContext(), "Boring details Successfuly Send", Toast.LENGTH_SHORT).show();
                    ContentValues PostBoringDetailsvalues = new ContentValues();
                    PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", spliteresult);
                    mTransObj.update_PostBoringDetails(PostBoringDetailsvalues, fileNum);
                }


            } catch (Exception e) {
                System.out.println("Exception Occurs BoringDetailsInfoWebServiceStatus in superactivity" + e);
            }


        }

    }

    /*************************************************MeterAsyncCallWS Details*********************************
     *
     */
    private class MeterAsyncCallWS extends AsyncTask<Void, Void, String> {

        String result;

        //		private ProgressDialog mProgressDialog;
        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			/*mProgressDialog = ProgressDialog.show(MeterDetailsActivity.this, "", 
					"Loading. Please wait...", true);*/
        }

        //	AE7654

        @Override
        protected String doInBackground(Void... params) {


            try {
                System.out.println("********************* in  super activity mMeterxml xml" + "," + mMeterxml);
                result = GbPostPendingDataToServer.invokePendingFiles(mMeterxml, "PostMeterDetailsInfo");

            } catch (Exception e) {
                System.out.println("Exception Occurs PostMeterDetailsInfo in superactivity" + e);
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //			mProgressDialog.dismiss();
            String spliteresult = null;

            try {
                ContentValues PostMeterDetailsvalues = new ContentValues();
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    PostMeterDetailsvalues.put("MeterDetailsInfoWebServiceStatus", result);
                    System.out.println("*******mFileno**************filenumber" + fileNum);
                    mTransObj.update_PostMeterDetails(PostMeterDetailsvalues, fileNum);

                } else {
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);
                }
                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();


                }

                if (spliteresult.contains("1")) {
                    PostMeterDetailsvalues.put("MeterDetailsInfoWebServiceStatus", spliteresult);
                    System.out.println("*******mFileno**************filenumber" + fileNum);
                    mTransObj.update_PostMeterDetails(PostMeterDetailsvalues, fileNum);
                    Toast.makeText(getApplicationContext(), "Meter details Successfuly Sent", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception e) {

                System.out.println("Exception Occurs PostMeterDetailsInfo in superactivity" + e);
            }


        }
    }

    /***********************************************************cangeneration ASync
     *
     */

    private class CanGenAsyncCallTo extends AsyncTask<Void, Void, String> {
        String result;

        //private ProgressDialog mProgressDialog;
        //	AE7654
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			/*mProgressDialog = ProgressDialog.show(SuperActivity.this, "", 
					"Loading. Please wait...", true)*/
            ;

            System.out.println("******************************can generating please wait");
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                String gbno = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.invokeGbFileNo(fileNum, gbno, "CanGenerateCAN");
                //************************************ get status value for get_CanGenerate_Status get_PostCustomerFeedBackDetails_Status
				/*try{
				String status=mTransObj.get_CanGenerate_Status(filenumber);
				System.out.println("******************************doInBackground"+status);
				if(status.equals("finished"))
				{

					setButtonClick(mBoaringDetailsBtn,false);
					setButtonClick(mMeterDetailsBtn,false);
					setButtonClick(mDisconnectionDetailsBut,false);
					setButtonClick(mCanGeneration,false);
					setButtonClick(mCustomerFeedbackDetails,true);
					setButtonClick(mCustomerBuildingDetails,true);
					System.out.println("*********After completion *filenumber"+status);
				}
				System.out.println("*************outside***********get_CanGenerate_Status"+status);
				}
				catch(Exception e)
				{

				}
				 */

            } catch (Exception e) {
                System.out.println("Exception Occurs CanGenerateCAN in superactivity" + e);
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @Override

        protected void onPostExecute(String result) {
            //			mProgressDialog.dismiss();
            String spliteresult = null;

            try {
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    ContentValues cangenerate = new ContentValues();
                    cangenerate.put("CanGenerateCANWebServiceStatus", result);
                    mTransObj.update_CanGenerateCAN(cangenerate, fileNum);
                } else {
                    if (result.contains("Unable to update status as CanGenerateCAN")) {
                        ContentValues cangenerate = new ContentValues();
                        cangenerate.put("CanGenerateCANWebServiceStatus", 1);
                        mTransObj.update_CanGenerateCAN(cangenerate, fileNum);
                        Toast.makeText(getApplicationContext(), "CAN Generated Successfuly Intalled", Toast.LENGTH_SHORT).show();

                    }
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);


                }
                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();	

                }
                if (spliteresult.contains("1")) {
                    ContentValues cangenerate = new ContentValues();
                    cangenerate.put("CanGenerateCANWebServiceStatus", spliteresult);
                    mTransObj.update_CanGenerateCAN(cangenerate, fileNum);
                    Toast.makeText(getApplicationContext(), "CAN Generated Successfuly Intalled", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception e) {
                System.out.println("Exception Occurs CanGenerateCAN in superactivity" + e);
            }


        }


    }

    /************************************************************************customer  asyn
     *
     */
    private class CustomerAsyncCallWS extends AsyncTask<Void, Void, String> {
        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
			/*mProgressDialog = ProgressDialog.show(CustomerFeedback.this, "", 
					"Loading. Please wait...", true);*/
        }

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.invokePendingFiles(mcustomerfeedbackxxml, "PostCustomerFeedBackDetailsInfo");
                System.out.println("the  customer feedback result is" + result + "******************the split values of arr[1]:" + result);
                Log.v(result, "customerfeedback result is");
            } catch (Exception e) {
                // TODO: handle exception
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            String spliteresult = null;
            try {
                if (result.contains("Failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    ContentValues customerfeedbackvalues = new ContentValues();
                    customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                    customerfeedbackvalues.put("CustomerFeedBackWebServiceStatus", result);
                    mTransObj.update_PostCustomerFeedBackDetails(customerfeedbackvalues, fileNum);
                } else {
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);


                }
                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();	

                }

                if (spliteresult.contains("1")) {
                    ContentValues customerfeedbackvalues = new ContentValues();
                    customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                    customerfeedbackvalues.put("CustomerFeedBackWebServiceStatus", spliteresult);
                    mTransObj.update_PostCustomerFeedBackDetails(customerfeedbackvalues, fileNum);
                    Toast.makeText(getApplicationContext(), "Customer FeedBack Successfuly sent", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                System.out.println("Exception Occurs PostCustomerFeedBackDetailsInfo in superactivity" + e);
            }


        }


    }

    /*************************************building asyn details
     *
     */
    private class BuildingAsyncCallWS extends AsyncTask<Void, Void, String> {
        String result;

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.invokePendingFiles(mBuildingxml, "PostBuildingDetailsInfo");
            } catch (Exception e) {
                // TODO: handle exception
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            String spliteresult = null;
            try {
                if (result.contains("failed")) {
                    ContentValues PostBuildingDetailsInfo = new ContentValues();


                    PostBuildingDetailsInfo.put("BuildingDetailsInfoWebServiceStatus", result);

                    mTransObj.update_PostBuildingDetails(PostBuildingDetailsInfo, fileNum);
                } else {
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);


                }
                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();	

                }
                if (spliteresult.contains("1")) {
                    ContentValues PostBuildingDetailsInfo = new ContentValues();
                    PostBuildingDetailsInfo.put("BuildingDetailsInfoWebServiceStatus", spliteresult);

                    mTransObj.update_PostBuildingDetails(PostBuildingDetailsInfo, fileNum);
                    Toast.makeText(getApplicationContext(), "Building Details Successfuly Sent", Toast.LENGTH_SHORT).show();
                }


            } catch (Exception e) {
                System.out.println("Exception Occurs BuildingDetailsInfoWebServiceStatus in superactivity" + e);
            }


        }

    }

    private class AsyncCallToGbBill extends AsyncTask<Void, Void, String> {


        ProgressDialog mProgressDialog;
        //	AE7654
        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(SuperActivity.this, "",
                    "Loading. Please wait...", true, false);
        }

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                //				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostGBRemarksInfo");
                System.out.println("the status of CanGenerateGBBill");
                String gbno2 = SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "");
                result = GbPostPendingDataToServer.invokeGbFileNo(fileNum, gbno2, "CanGenerateGBBill");
                System.out.println("the status of generate bill" + result);
            } catch (Exception e) {
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            String spliteresult = null;

            try {

                mProgressDialog.dismiss();


                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                    ContentValues cangeneratebill = new ContentValues();
                    cangeneratebill.put("CanGenerateGBBillWebServiceStatus", result);
                    mTransObj.update_CanGenerateGBBill(cangeneratebill, fileNum);
                } else {
                    if (result.contains("CanGenerateGBBill staus updation cannot be acceptable")) {
                        ContentValues cangeneratebill = new ContentValues();
                        cangeneratebill.put("CanGenerateGBBillWebServiceStatus", 1);
                        mTransObj.update_CanGenerateCAN(cangeneratebill, fileNum);
                        Toast.makeText(getApplicationContext(), "GBBill Already Generated", Toast.LENGTH_SHORT).show();
                    }
                    String arr[] = result.split("|");
                    spliteresult = arr[1];
                    System.out.println("the split result valuesa arr[0]:" + result + "******************the split values of arr[1]:" + arr[1]);


                }

                if (spliteresult.contains("0")) {
//					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();
                }
                if (spliteresult.contains("1")) {
                    ContentValues cangeneratebill = new ContentValues();
                    cangeneratebill.put("CanGenerateGBBillWebServiceStatus", spliteresult);
                    mTransObj.update_CanGenerateGBBill(cangeneratebill, fileNum);
                    Toast.makeText(getApplicationContext(), "GB Bill Received", Toast.LENGTH_LONG).show();
                }
                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                startActivity(i);
            } catch (Exception e) {
                System.out.println("Exception Occurs CanGenerateGBBill in superactivity" + e);
            }


        }
    }


    public void logoutFromApp() {

		/*Intent intent = new Intent(Intent.ACTION_MAIN);
		  intent.addCategory(Intent.CATEGORY_HOME);
		  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); 
		  startActivity(intent);*/

        finish();
        System.exit(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


}



