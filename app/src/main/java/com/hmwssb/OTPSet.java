package com.hmwssb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.os.Environment;

public class OTPSet {
	private static final String DATABASE_NAME = "GBDatabase";

	public String otpno;
	 public static  String Otp;
	 public static  int status;
	 
	 public String getOtpno() {
		return otpno;
	}
	public static int getStatus() {
		return status;
	}
	public static void setStatus(int status) {
		OTPSet.status = status;
	}
	public void setOtpno(String otpno) {
		this.otpno = otpno;
	}
	public static String getOtp() {
		return Otp;
	}
	public static void setOtp(String otp) {
		Otp = otp;
	}
	

	 public int exportDB(){
			int i=0;
			File sd = Environment.getExternalStorageDirectory();
			if(sd.exists())
		       {
		    	   //Toast.makeText(this, "DB exists!", Toast.LENGTH_LONG).show();
		         	
			File data = Environment.getDataDirectory();
		       FileChannel source=null;
		       FileChannel destination=null;
		       String currentDBPath = "/data/"+ "com.vajra.hmwssb" +"/databases/"+DATABASE_NAME;
		       String backupDBPath = DATABASE_NAME;
		       File currentDB = new File(data, currentDBPath);
		       File backupDB = new File(sd, backupDBPath);
		       if(backupDB.exists())
		       {
		    	   //Toast.makeText(context, "DB exists!", Toast.LENGTH_LONG).show();
		    	  
		       }
		       else{
		       try {
		            source = new FileInputStream(currentDB).getChannel();
		            destination = new FileOutputStream(backupDB).getChannel();
		            destination.transferFrom(source, 0, source.size());
		            source.close();
		            destination.close();
		            //Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
		           System.out.println("DB Exported ");
		        } catch(IOException e) {
		        	e.printStackTrace();
		        }
		       }
		       i=1;
		       }
			else{
				//Toast.makeText(this, "Please Insert SD Card", Toast.LENGTH_LONG).show();
				i=2;
			}
			return i;
		}

	
}
