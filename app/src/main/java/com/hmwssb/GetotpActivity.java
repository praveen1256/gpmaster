package com.hmwssb;


import java.util.ArrayList;

import org.ksoap2.serialization.PropertyInfo;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.OperationCompleteListener;
import com.vajra.service.Sms_service;
import com.vajra.service.model.GbContractorDetails;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;


public class GetotpActivity extends SuperActivity implements AppConstants, OnClickListener, SMSReceiver.OTPReceiveListener {

    public EditText et_otp;
    TextView tv_status, tv_warn;
    Button btn_getotp, btn_generate_otp, btn_back;
    Transactions transobj;
    String mobno;
    int otpno;
    //    SmsBroadcastReceiver smsBroadcastReceiver;
    private String gbCode;
    GbContractorDetails gb;
    String SIMSerialNumber;
    Context context;
    Intent intent;
    public static final String SMS_BUNDLE = "pdus";
    public String smsBody;
    PropertyInfo smsProperty = new PropertyInfo();
    IntentFilter filter;
    AppSignatureHashHelper appSignatureHashHelper;

    SMSReceiver otpReceiver;
    String uniqueCode;
//    = "[#] "+AppUtils.generateRandomDigits()+"is yout OTP. Code is Valid for 5 mins only for one time use."+"\n"+"HnaHLMtqQac";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_otp_activity);
        gb = new GbContractorDetails();
        et_otp = (EditText) findViewById(R.id.et_text);
        btn_getotp = (Button) findViewById(R.id.btn_get_val);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_generate_otp = (Button) findViewById(R.id.btn_generate_otp);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_warn = (TextView) findViewById(R.id.tv_warn);
        tv_warn.setTextColor(Color.BLUE);
        tv_warn.setVisibility(View.INVISIBLE);
        btn_getotp.setEnabled(true);
        btn_getotp.setVisibility(View.INVISIBLE);
        et_otp.setEnabled(true);
        btn_back.setVisibility(View.INVISIBLE);
        TelephonyManager tm = (TelephonyManager) getSystemService(getBaseContext().TELEPHONY_SERVICE);
        SIMSerialNumber = tm.getSimSerialNumber();
        appSignatureHashHelper = new AppSignatureHashHelper(this);
        Log.d("Hashvalue", "Apps Hash Key: " + appSignatureHashHelper.getAppSignatures().get(0));
        //Toast.makeText(getApplicationContext(), gb.getGbCode(), 100).show();
        filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        uniqueCode = "[#] " + AppUtils.generateRandomDigits() + " is yout OTP. Code is Valid for 5 mins only for one time use." + "\n" + appSignatureHashHelper.getAppSignatures().get(0);
//        registerReceiver(mBroadcastReceiver, filter);

        transobj = new Transactions();
        try {
            transobj.open();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> gbcodedatalist = mTransObj.getgbcode();
        System.out.println("**********************The gb codedatalist his" + gbcodedatalist.size());
        try {
            for (int i = 0; i < gbcodedatalist.size(); i++) {
                gbCode = gbcodedatalist.get(1).toString();
                SharedPreferenceUtils.savePreferenceGBCODE(getApplicationContext(), IPreferencesConstants.GB_CODE, gbcodedatalist.get(1).toString());
                //			mGbContractorName.setTextSize(size);
            }
        } catch (Exception e) {
            System.out.println("exception in gbcodedatalist" + e);
        }

        btn_generate_otp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                btn_getotp.setVisibility(View.VISIBLE);


                mobno = transobj.mobilenumber(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
//                smsBroadcastReceiver = new SmsBroadcastReceiver();

                if ((mobno == "") || mobno == null) {
                    Toast.makeText(getApplicationContext(), "Invalid User Please Enter Valid GB Code", 1000).show();
                    Intent i = new Intent(getApplicationContext(), OTPActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    startSMSListener();
                    btn_generate_otp.setEnabled(false);
                    btn_back.setVisibility(View.VISIBLE);
                    tv_warn.setVisibility(View.VISIBLE);
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
//                    pendingfile();
                }

            }
        });

        btn_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(), "  Please Check Your GB Code ", 200).show();
                String s = transobj.deleterecode(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
                if (s == null) {
                    Intent i = new Intent(getApplicationContext(), OTPActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
        btn_getotp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = et_otp.getText().toString();

                if ((OTPSet.getOtp().contains(s)) && s.length() > 2) {
                    getContractorDetails();
                } else {

                    Toast.makeText(getApplicationContext(), "please enter the valid OTP", 500).show();
                }
            }
        });

    }

    private void startSMSListener() {
        try {
            otpReceiver = new SMSReceiver();
            otpReceiver.setOTPListener(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(otpReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // API successfully started
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Fail to start API
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOTPReceived(String otp) {

        String lastotp = otp.substring(4, 9);
        showToast("OTP Received: " + lastotp);
//                otp.replace("<#> Thank you for registering with HMWSSB. Your OTP is ","");
        String generatedOtp = (OTPSet.getOtp());
        if (generatedOtp.contains(lastotp)) {
            AppUtils.showToast(GetotpActivity.this, lastotp);
            btn_back.setVisibility(View.INVISIBLE);
            et_otp.setText(lastotp);
            insert();

        } else {
            Intent i = new Intent();
            i.setClassName("com.hmwssb", "com.hmwssb.OTPActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
        if (otpReceiver != null) {
            unregisterReceiver(otpReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {
        showToast("OTP Time out");
    }

    @Override
    public void onOTPReceivedError(String error) {
        showToast(error);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        try {
            if (otpReceiver != null) {
                unregisterReceiver(otpReceiver);
            }
        } catch (Exception e) {
        }
        //  unregisterReceiver(deliveryBroadcastReceiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {

        try {
            if (otpReceiver != null) {
                unregisterReceiver(otpReceiver);
            }
        } catch (Exception e) {
        }

        super.onDestroy();
    }

    private void getContractorDetails() {


        //getapp()
        final ProgressDialog mProgressDialog = ProgressDialog.show(GetotpActivity.this, "", "Loading Pending Files",
                true, false);
        getApp().getBoringServices().getConnectionDetails(gbCode, "GetGBPendingFilesInfo", new OperationCompleteListener() {
            @Override
            public void operationComplete(boolean status, int code, String message) {

                mProgressDialog.dismiss();
                Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
                finish();
                startActivity(i);
            }
        });

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        //	AE7654
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Sms_service.pullSmsFromServer(mobno, uniqueCode, "SENDTESTSMS");

            } catch (Exception e) {
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            try {/*
				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
			 */
            } catch (Exception e) {
                System.out.println("exception in AsyncCallWS" + e);
            }


        }

    }

//    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(final Context context, Intent intent) {
//            Bundle intentExtras = intent.getExtras();
//
//            if (intentExtras != null) {
//                Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
//                String smsMessageStr = "";
//                for (int i = 0; i < sms.length; ++i) {
//
//                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
//
//                    smsBody = smsMessage.getMessageBody().toString();
//                    String address = smsMessage.getOriginatingAddress();
//
//                    smsMessageStr += "SMS From: " + address + "\n";
//                    smsMessageStr += smsBody + "\n";
//                }
//
//
//                int generatedOtp = Integer.parseInt(OTPSet.getOtp());
//                Log.i("root", "generatedOtp :" + generatedOtp);
//                if (smsMessageStr.contains(generatedOtp + "")) {
//                    AppUtils.showToast(context, smsMessageStr);
//                    btn_back.setVisibility(View.INVISIBLE);
//                    et_otp.setText(OTPSet.getOtp());
//                    insert();
//
//                } else {
//                    Intent i = new Intent();
//                    i.setClassName("com.hmwssb", "com.hmwssb.OTPActivity");
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(i);
//                }
//
//
//            }
//
//
//        }
//
//    };


    public void insert() {
        btn_back.setVisibility(View.INVISIBLE);

        transobj = new Transactions();
        try {
            transobj.open();
        } catch (Exception e) {
            e.printStackTrace();
        }


        ContentValues consumablesValues = new ContentValues();
        consumablesValues.put("swcl_otp", uniqueCode);
        consumablesValues.put("swcl_simno", SIMSerialNumber);
        consumablesValues.put("swcl_otp_status", "1");
        //consumablesValues.put("swcl_mobileno", mobno);
        long l = transobj.insert_swc_licence_otp(consumablesValues);

        tv_status.setText("Licence Successfully Activated");
        tv_status.setTextColor(Color.RED);
        btn_getotp.setEnabled(true);
        btn_getotp.setVisibility(View.VISIBLE);
    }


    public void pendingfile() {
        btn_getotp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                String s = et_otp.getText().toString();

                if ((OTPSet.getOtp().contains(s))) {
                    getContractorDetails();
                } else {

                    Toast.makeText(getApplicationContext(), "please enter the valid OTP", 500).show();
                }
            }
        });
    }


    private class GetSystemDate extends AsyncTask<Void, String, String> {
        String result;

        //	AE7654
        @Override
        protected String doInBackground(Void... params) {
            try {
                result = GbPostPendingDataToServer.GetSystemDate("GetSystemDate");
                System.out.println("****************************GetSystemDate " + result);
            } catch (Exception e) {
                System.out.println("exception in GetSystemDate error" + e);
            }


            //GbPostPendingDataToServer.invokePendingFiles();
            //		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

            return result;
        }

        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(String result) {

            try {

                Toast.makeText(getApplicationContext(), "sytemDate:" + result, Toast.LENGTH_LONG).show();
                System.out.println("****************************result  GetSystemDate" + result);
                if (result.contains("failed")) {
                    Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

            } catch (Exception e) {
                System.out.println("Exception Occurs Getversioncontrol " + e);
            }


        }

    }

}












