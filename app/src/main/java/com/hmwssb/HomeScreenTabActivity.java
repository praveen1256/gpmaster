package com.hmwssb;


import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.hmwssb.fragments.HomeFragment;
import com.vajra.hmwssb.fragments.NewConnFragment;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.HmwssbApplication;
import com.vajra.service.OperationCompleteListener;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppConstants.IPreferencesConstants;
import com.vajra.utils.SharedPreferenceUtils;
/*import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;*/

public class HomeScreenTabActivity extends FragmentActivity  {

	private TextView mGbContractorName;

	private EditText mSearchEt;
	private TextView tv_count,headerTv;
	private ImageView mSearchBut;
	private Button refresh, getLocalData,upload;
	BoringConnection boaring;

	private HomeFragment fragment;
	private NewConnFragment homeFragment;
	private HmwssbApplication app;
	private ViewPager mViewPager;
	private ProgressDialog mProgressDialog;
	private Transactions mmTransObj;
	ArrayList<String> gbcodedatalist;
	String enhancementcount,newconnectioncount;
	ArrayList<BoringConnection> boaringlist;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_tab_home);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		refresh = (Button) findViewById(R.id.refresh);
		upload=(Button)findViewById(R.id.upload);
		mSearchEt = (EditText) findViewById(R.id.search);
		headerTv=(TextView)findViewById(R.id.headerTv);
		tv_count=(TextView)findViewById(R.id.tv_count);
		mGbContractorName = (TextView) findViewById(R.id.gb_contractor_name);

		Bundle extras = getIntent().getExtras(); 
		if(extras!=null)
		{
			String result=extras.getString("version");
			Toast.makeText(getApp(), "the result values:"+result, Toast.LENGTH_LONG).show();
			refresh.setVisibility(View.INVISIBLE);
		}
		headerTv.setText("List of Files Pending for Release Version:V7.1");

		//mmTransObj = new Transactions(this);
		mmTransObj = new Transactions();
		try {
			mmTransObj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		homeFragment= new NewConnFragment();
		String Errors="Error";
		app = (HmwssbApplication) getApplication();
		if (savedInstanceState == null) {
			fragment = new HomeFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.home_content_layout, fragment).commit();
		}

		String contractorDetails = getString(R.string.contractor_name, app.getBoringServices().getGbContractorDetails().getGbName() , app.getBoringServices().getGbContractorDetails().getGbCode());

		//		String contractorDetails = getString(R.string.contractor_name, app.getBoringServices().getGbContractorDetails().getGbName() , app.getBoringServices().getGbContractorDetails().getGbCode());
		mGbContractorName.setText(contractorDetails);

		System.out.println("********HOMESCREENNAME**"+contractorDetails);
		//		String contractorDetails = getString(R.string.contractor_name, app.getBoringServices().getGbContractorDetails().getGbName(), app.getBoringServices().getGbContractorDetails().getGbCode());
		gbcodedatalist=mmTransObj.getgbcode();
		System.out.println("**********************The gb codedatalist his"+gbcodedatalist.size());
		boaringlist=new ArrayList<BoringConnection>();
		try
		{
			newconnectioncount=	mmTransObj.get_Count_Files("N");
			enhancementcount=	mmTransObj.get_Count_Files("E");
			/*String type= fragment.getpostion();
			if(type.contains("N"))
			{
				Toast.makeText(getApplicationContext(), "in newconnectioncount"+newconnectioncount, 100).show();
				tv_count.setText(newconnectioncount);	
			}
			if(type.contains("E"))
			{
				Toast.makeText(getApplicationContext(), "in newconnectioncount"+newconnectioncount, 100).show();
				tv_count.setText(enhancementcount);	
			}
			else
			{
				tv_count.setText("in else"+newconnectioncount);	
			}*/



			/*if(mViewPager.getCurrentItem() == 0)
			{
				System.out.println("**********************in newconnectioncount"+newconnectioncount);
				tv_count.setText(newconnectioncount);	
			}
			else
			{
				System.out.println("**********************in enhancementcount"+enhancementcount);
				tv_count.setText(enhancementcount);	
			}*/


			//Toast.makeText(getApplicationContext(), "Ncount:"+newconnectioncount+"***Ecount:"+enhancementcount, 100).show();

			/**************************************Delteing finished files enhancementcount************************************/
			ArrayList<String>finishedfiles= mmTransObj.getfilenumbers();

			System.out.println("***************finished files in list id*******************:"+finishedfiles);
			if(!finishedfiles.isEmpty())
			{
				for(int i=0;i<finishedfiles.size();i++)
				{
					mmTransObj.delete_CanGenerateCAN(finishedfiles.get(i));
					mmTransObj.delete_CanGenerateGBBill(finishedfiles.get(i));
					mmTransObj.delete_GetGBInfo(finishedfiles.get(i));
					mmTransObj.delete_PostBoringDetailsInfo(finishedfiles.get(i));
					mmTransObj.delete_PostBuildingDetailsInfo(finishedfiles.get(i));;
					mmTransObj.delete_PostCustomerFeedBackDetailsInfo(finishedfiles.get(i));
					mmTransObj.delete_PostDisconnectionDetailsInfo(finishedfiles.get(i));
					mmTransObj.delete_PostGBRemarksInfo(finishedfiles.get(i));
					mmTransObj.delete_PostMeterDetailsInfo(finishedfiles.get(i));
					mmTransObj.delete_GetGBPendingFilesInfo(finishedfiles.get(i)); 
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("the exception  finishedfiles"+e);
		}


		try
		{
			for(int i=0;i<gbcodedatalist.size();i++)

			{
				mGbContractorName.setText(gbcodedatalist.get(3).toString()+"\n" +" GBCode:"+gbcodedatalist.get(1).toString());
				SharedPreferenceUtils.savePreferenceGBCODE(getApplicationContext(), IPreferencesConstants.GB_CODE, gbcodedatalist.get(1).toString());
				//			mGbContractorName.setTextSize(size);
			}
		}
		catch(Exception e)
		{
			System.out.println("the exception  gbcodedatalist"+e);
		}

		upload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boaringlist =mmTransObj.getPendingdetails();
				boaring= new BoringConnection();

				for(BoringConnection boaring:boaringlist )
				{
					String filenum=boaring.getFileNo();

					SuperActivity activityobj = new SuperActivity();

					activityobj.CheckNetworkStatus(filenum);

				}



			}
		});
		mSearchBut = (ImageView) findViewById(R.id.search_but);
		mSearchBut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fragment.searchForTheText(mSearchEt.getText().toString());
				//	fragment.getConnectionDetaials();

				//				fragment.searchForTheText(mSearchEt.getText().toString());

			}
		});
		refresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Getversioncontrol version = new Getversioncontrol();
				version.execute();
				getContractorDetails();
				fragment.getConnectionDetaials();



			}
		});


	}
	public HmwssbApplication getApp() {
		return (HmwssbApplication) getApplication();
	}
	private void getContractorDetails() {

		final ProgressDialog mProgressDialog = ProgressDialog.show(HomeScreenTabActivity.this, "", getString(R.string.progress_message),
				true, false);

		getApp().getBoringServices().getConnectionDetails(SharedPreferenceUtils.getPreference(this, AppConstants.IPreferencesConstants.GB_CODE, ""), "GetGBPendingFilesInfo", new OperationCompleteListener() {
			@Override
			public void operationComplete(boolean status, int code, String message) {
				mProgressDialog.dismiss();
				fragment.getConnectionDetaials();

			}
		});

	}

	private class Getversioncontrol extends AsyncTask<Void, String, String> {
		String result;
		//	AE7654
		@Override
		protected String doInBackground(Void... params) {
			try {
				result=	GbPostPendingDataToServer.GetVersioncontrol("GetLatestVersionNo");
				System.out.println("****************************Getversioncontrol "+result);
			} catch (Exception e) {
				System.out.println("exception in Getversioncontrol error"+ e);
			}


			//GbPostPendingDataToServer.invokePendingFiles();
			//		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			try {
				System.out.println("****************************result  Getversioncontrol"+result);

			} catch (Exception e) {
				System.out.println("Exception Occurs Getversioncontrol " + e);
			}
		}

	}




}