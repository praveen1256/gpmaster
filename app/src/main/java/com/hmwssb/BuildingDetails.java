package com.hmwssb;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognitionListener;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BuildingDetails extends SuperActivity implements OnClickListener, AppConstants, RecognitionListener, LocationListener{

	private Button mSaveBuildingDetailsButton;
	StringBuilder ConnInfo = new StringBuilder();
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

	private Uri fileUri; // file url to store image/video
	private GPSTracker gps;
	protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
	protected static final int CAMERA_REQUEST = 0;
	static File   mediaStorageDir;
	Button b1;
	static File mediaFile;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	static ImageView iv,decodeIv;
	private static File DatePath;
	//	public TextView dateTv;
	private String date;
	private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";

	ProgressDialog prog;
	Location loc; 
	LocationManager locationManager;

	public Boolean Flag=true;
	private String longBoringdegree;
	private String longBoringminute;
	private String longBoringsecond;
	private String latBoringDegree;
	public byte[] byteArray;
	private Transactions mTransObj;
	private byte[] handlePhoto;
	private String photo_Encoded;
	private String latgBoringMinute;
	private String latgBoringSecond;
	private String longBoringMilliSecond;
	private String latBoringMilliSecond;
	private Button mBuildingPhotoButton;

	private String mLongBoringDegree;
	private String mLongBoringMinute;
	private String mLatBoringDegree;
	private String mLatBoringMinute;
	private String latBoringSecond;
	private String ConnXml;
	private String mLongBoringSeconds;
	private String longBoringMilliSeconds;
	private String mLongBoringMilliSeconds;
	private String mLatBoringSeconds;
	private Object mLatBoringMilliSeconds;
	private String latBoringMilliSeconds;
	private String mFileno;
	ArrayList<String> buildinglist;
	EditText latlong;
	public ImageView signout, home;
	private Location oldLocation;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.building_photo_activity);



		latlong=(EditText)findViewById(R.id.et_lat);

		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);

		//
		mSaveBuildingDetailsButton=(Button)findViewById(R.id.savedata);
		mBuildingPhotoButton=(Button)findViewById(R.id.building_photo);


		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				logoutFromApp();
			}
		});
		//mTransObj = new Transactions(this);
		mTransObj = new Transactions();
		try {
			mTransObj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.new_connection);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("Building Photo");

		Bundle bundle = getIntent().getExtras();
		mFileno = bundle.getString(KEY_FILENO); 
		buildinglist=mTransObj.getBuildingdetails(mFileno);
		System.out.println("buildinglist"+buildinglist.size());
		if(!buildinglist.isEmpty())
		{
			for(int i=0; i<buildinglist.size();i++)
			{
				int sdk = android.os.Build.VERSION.SDK_INT;

				photo_Encoded=buildinglist.get(0);

				byte[] decodedString = Base64.decode(photo_Encoded, Base64.DEFAULT);
				Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
				Drawable drawable = new BitmapDrawable(getResources(), decodedByte);
				mBuildingPhotoButton.setBackground(drawable);
				mLatBoringDegree=buildinglist.get(1);
				mLatBoringMinute=buildinglist.get(2);
				mLatBoringSeconds=buildinglist.get(3);
				mLatBoringMilliSeconds=buildinglist.get(4);
				mLongBoringDegree=buildinglist.get(5);
				mLongBoringMinute=buildinglist.get(6);
				mLongBoringSeconds=buildinglist.get(7);
				mLongBoringMilliSeconds=buildinglist.get(8);


				latlong.setText(mLatBoringDegree+mLatBoringMinute+mLatBoringSeconds+mLatBoringMilliSeconds+","+mLongBoringDegree+mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);
			}

		}
		mBuildingPhotoButton.setOnClickListener(this);
		mSaveBuildingDetailsButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.savedata:
		{
			try
			{
				if(photo_Encoded==null)
				{
					Toast.makeText(getApplicationContext(), "please Capture Photo", Toast.LENGTH_LONG).show();
					return;
				}

				//				getBoringCoordinates();



				System.out.println("mLongBoringDegree"+mLongBoringDegree+"mLongBoringMinute"+mLongBoringMinute+"longBoringsecond"+longBoringsecond);



				if(mLatBoringDegree==null||mLatBoringDegree.length()==0 )
				{
					Toast.makeText(getApplicationContext(), " BuildingDetails longitudes and latitiudes is manditaory  ", Toast.LENGTH_LONG).show();
					return;
				}



				if(mLatBoringMinute==null||mLatBoringMinute.length()==0 )
				{
					Toast.makeText(getApplicationContext(), " Invalid BuildingDetails latitude mintues", Toast.LENGTH_LONG).show();
					return;
				}
				if(mLatBoringSeconds==null||mLatBoringSeconds.length()==0)
				{
					Toast.makeText(getApplicationContext(), "Invalid BuildingDetails latitude Seconds", Toast.LENGTH_LONG).show();
					return;
				}



				System.out.println("mLatBoringMilliSeconds:"+mLatBoringMilliSeconds+"mLongBoringDegree:"+mLongBoringDegree+"mLongBoringMinute:"+mLongBoringMinute);
				if(mLongBoringDegree==null||mLongBoringDegree.length()==0)
				{
					Toast.makeText(getApplicationContext(), "Invalid Boaringdetails longitudes and", Toast.LENGTH_LONG).show();
					return;
				}

				if(mLongBoringMinute==null||mLongBoringMinute.length()==0 )
				{
					Toast.makeText(getApplicationContext(), " Invalid Boaringdetails longitudes mintues", Toast.LENGTH_LONG).show();
					return;
				}
				if(mLatBoringSeconds==null||mLatBoringSeconds.length()==0)
				{
					Toast.makeText(getApplicationContext(), "Invalid Boaringdetails longitudes Seconds", Toast.LENGTH_LONG).show();
					return;
				}



				//************************Post Building details*******************
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
				String currentDateandTime = sdf.format(new Date());
				if(buildinglist.isEmpty())
				{
					PostConnectionInfo();
					ContentValues PostBuildingDetailsInfo = new ContentValues();
					PostBuildingDetailsInfo.put("FileNo", mFileno);
					PostBuildingDetailsInfo.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostBuildingDetailsInfo.put("BuildingPhoto", photo_Encoded);
					PostBuildingDetailsInfo.put("BuildingLatitudeDegrees", mLatBoringDegree);
					PostBuildingDetailsInfo.put("BuildingLatitudeMilliSeconds",mLatBoringMilliSeconds.toString());
					PostBuildingDetailsInfo.put("BuildingLatitudeMinutes", mLatBoringMinute);
					PostBuildingDetailsInfo.put("BuildingLatitudeSeconds",mLatBoringSeconds);
					PostBuildingDetailsInfo.put("BuildingLongitudeDegrees", mLongBoringDegree);
					PostBuildingDetailsInfo.put("BuildingLongitudeMilliSeconds", mLatBoringMilliSeconds.toString());
					PostBuildingDetailsInfo.put("BuildingLongitudeMinutes", mLongBoringMinute);
					PostBuildingDetailsInfo.put("BuildingLongitudeSeconds", mLongBoringSeconds);


					PostBuildingDetailsInfo.put("BuildingDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostBuildingDetailsInfo.put("BuildingDetailsInfoCreatedDtm",currentDateandTime );
					PostBuildingDetailsInfo.put("BuildingServerStatus", "finished");
					PostBuildingDetailsInfo.put("BuildingDetailsInfoXMLString", ConnXml);
					//PostBuildingDetailsInfo.put("BuildingServerStatus", "suceess");
					/*//`BuildingDetailsInfoCreateByUid` TEXT,
			   `BuildingDetailsInfoCreatedDtm`*/
					mTransObj.insert_PostBuildingDetailsInfo(PostBuildingDetailsInfo);

					Intent i = new Intent(BuildingDetails.this, NewConnection.class);
					startActivity(i);
					finish();
				}
				else
				{
					PostConnectionInfo();
					ContentValues PostBuildingDetailsInfo = new ContentValues();

					PostBuildingDetailsInfo.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostBuildingDetailsInfo.put("BuildingPhoto", photo_Encoded);
					PostBuildingDetailsInfo.put("BuildingLatitudeDegrees", mLatBoringDegree);
					PostBuildingDetailsInfo.put("BuildingLatitudeMilliSeconds", mLatBoringMilliSeconds.toString());

					PostBuildingDetailsInfo.put("BuildingLatitudeMinutes", mLatBoringMinute);
					PostBuildingDetailsInfo.put("BuildingLatitudeSeconds",mLatBoringSeconds);
					PostBuildingDetailsInfo.put("BuildingLongitudeDegrees", mLongBoringDegree);
					PostBuildingDetailsInfo.put("BuildingLongitudeMilliSeconds", mLatBoringMilliSeconds.toString());
					PostBuildingDetailsInfo.put("BuildingLongitudeMinutes", mLongBoringMinute);
					PostBuildingDetailsInfo.put("BuildingLongitudeSeconds", mLongBoringSeconds);

					PostBuildingDetailsInfo.put("BuildingDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostBuildingDetailsInfo.put("BuildingDetailsInfoCreatedDtm",currentDateandTime );
					PostBuildingDetailsInfo.put("BuildingServerStatus", "finished");

					PostBuildingDetailsInfo.put("BuildingDetailsInfoXMLString", ConnXml);
					/*//`BuildingDetailsInfoCreateByUid` TEXT,
				   `BuildingDetailsInfoCreatedDtm`BuildingDetailsInfoXMLString*/
					mTransObj.update_PostBuildingDetails(PostBuildingDetailsInfo, mFileno);

					Intent i = new Intent(BuildingDetails.this, NewConnection.class);
					startActivity(i);
					finish();
				}
			}
			catch(Exception e)
			{
				System.out.println("exception in save data"+ e);
			}
			//PostConnectionInfo();
			finish();
		}
		break;
		case R.id.building_photo:
		{
			captureImage();
		}
		break;


		default:
			break;
		}
	}

	private void PostConnectionInfo() {
		ConnXml="";
		ConnInfo.append("<ConnInfo>");

		ConnInfo.append("<FileNo>");
		ConnInfo.append(mFileno);
		ConnInfo.append("</FileNo>");

		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<BuildingPhoto>");
		ConnInfo.append(photo_Encoded);
		ConnInfo.append("</BuildingPhoto>");

		ConnInfo.append("<BuildingLatitudeDegrees>");
		ConnInfo.append(mLatBoringDegree);
		ConnInfo.append("</BuildingLatitudeDegrees>");

		ConnInfo.append("<BuildingLatitudeMinutes>");
		ConnInfo.append(mLatBoringMinute);
		ConnInfo.append("</BuildingLatitudeMinutes>");

		ConnInfo.append("<BuildingLatitudeSeconds>");
		ConnInfo.append(mLatBoringSeconds);
		ConnInfo.append("</BuildingLatitudeSeconds>");

		ConnInfo.append("<BuildingLatitudeMilliSeconds>");
		ConnInfo.append(mLatBoringMilliSeconds);
		ConnInfo.append("</BuildingLatitudeMilliSeconds>");

		ConnInfo.append("<BuildingLongitudeDegrees>");
		ConnInfo.append(mLongBoringDegree);
		ConnInfo.append("</BuildingLongitudeDegrees>");

		ConnInfo.append("<BuildingLongitudeMinutes>");
		ConnInfo.append(mLongBoringMinute);
		ConnInfo.append("</BuildingLongitudeMinutes>");

		ConnInfo.append("<BuildingLongitudeSeconds>");
		ConnInfo.append(mLongBoringSeconds);
		ConnInfo.append("</BuildingLongitudeSeconds>");

		ConnInfo.append("<BuildingLongitudeMilliSeconds>");
		ConnInfo.append(mLongBoringMilliSeconds);
		ConnInfo.append("</BuildingLongitudeMilliSeconds>");

		ConnInfo.append("</ConnInfo>");

		ConnXml=ConnInfo.toString();

		Log.v(ConnXml, "XML");

		CheckNetworkStatus(mFileno);

		AsyncCallWS task = new AsyncCallWS();
		//Call execute
		task.execute();


	}

	private class AsyncCallWS extends AsyncTask<Void, Void, String> {
		String result;
		//	AE7654
		@Override
		protected String doInBackground(Void... params) {
			try {
				result= GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostBuildingDetailsInfo");
			} catch (Exception e) {
				System.out.println("Exception Occurs PostBuildingDetailsInfo" + e);
			}


			//GbPostPendingDataToServer.invokePendingFiles();
			//		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			String splitresult=null;

			try {
				if(result.contains("failed"))
				{
					ContentValues PostBuildingDetailsInfo = new ContentValues();
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();

					PostBuildingDetailsInfo.put("BuildingDetailsInfoWebServiceStatus", result);

					mTransObj.update_PostBuildingDetails(PostBuildingDetailsInfo, mFileno);
				}

				else
				{
					String arr[]=result.split("|");
					splitresult=arr[1];
					System.out.println("the split result valuesa arr[0]:"+result+"******************the split values of arr[1]:"+arr[1]);



				}

				if(splitresult.contains("0"))
				{
					//					Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
				}

				if(splitresult.contains("1"))
				{

					ContentValues PostBuildingDetailsInfo = new ContentValues();
					PostBuildingDetailsInfo.put("BuildingDetailsInfoWebServiceStatus", splitresult);

					mTransObj.update_PostBuildingDetails(PostBuildingDetailsInfo, mFileno);
					Toast.makeText(getApplicationContext(), "Building details send successfully", Toast.LENGTH_SHORT).show();
				}


			} catch (Exception e) {
				System.out.println("Exception Occurs PostBuildingDetailsInfo" + e);
			}


		}

	}
	private void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

		//		getBoringCoordinates();


	}

	@SuppressLint("SdCardPath")
	private void getBoringCoordinates() {/*


		gps = new GPSTracker(this);


		// check if GPS enabled
		if(gps.canGetLocation()){



			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();

			String lat=  String.valueOf(latitude);
			String longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);


			try {

				String[] dmsBoringLong = strLongitude.split(":");
				System.out.println("dmsBoringLong"+dmsBoringLong[0]+dmsBoringLong[1]+ dmsBoringLong[2]);
				mLongBoringDegree = dmsBoringLong[0];
				mLongBoringMinute = dmsBoringLong[1];
				longBoringsecond = dmsBoringLong[2];


				String[] longBoringMilliSec = longBoringsecond.split("\\.");

				mLongBoringSeconds = longBoringMilliSec[0];
				longBoringMilliSeconds = longBoringMilliSec[1];
				mLongBoringMilliSeconds=longBoringMilliSeconds.substring(0, 2);


				//latitude split
				String[] dmsBoringLat = strLatitude.split(":");

				mLatBoringDegree = dmsBoringLat[0];
				mLatBoringMinute = dmsBoringLat[1];
				latBoringSecond = dmsBoringLat[2];

				String[] latBoringMilliSec = latBoringSecond.split("\\.");

				mLatBoringSeconds = latBoringMilliSec[0];
				latBoringMilliSeconds = latBoringMilliSec[1];
				mLatBoringMilliSeconds=latBoringMilliSeconds.substring(0, 2);

			} catch (Exception e) {
				System.out.println("Exception Occurs strLongitude spliting" + e);
			}
			try
			{


				File logFile = new File("/sdcard/gps.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("Exception Occurs /sdcard/gps.t " + e);
					}
				}

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				String dateNow = lat + longi;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();

			}
			catch (IOException e)
			{

				e.printStackTrace();
			}


			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
			latlong.setText(strLatitude+ ","  +strLongitude);


		}else{
			gps.showSettingsAlert();
		}
	 */
		gpsFinding();
	}


	public void gpsFinding() {
		try {
			loc = null;
			prog = new ProgressDialog(this);
			prog.setTitle("Please Wait...!");
			prog.setMessage("Searching for GPS Coordinates...");
			prog.setCancelable(false);
			prog.show();

			locationManager = (LocationManager)getSystemService("location");

			//			LocationListener locationListener = new MyLocationListener();
						locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1L, 1F, this);
			//locationManager.requestLocationUpdates("gps", 1L, 1F, this);


			if (locationManager != null)
			{
				oldLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}


		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
			DatePath= mediaFile;

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	private void previewCapturedImage() {
		try {

			ExifInterface exif = null ;
			try {
				exif = new ExifInterface(DatePath.getPath());
				date=exif.getAttribute(ExifInterface.TAG_DATETIME);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			getPhotoToByteArray();
			getBoringCoordinates();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public byte [] getPhotoToByteArray() {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(mediaFile.getPath(),
				options);
		bitmap=	fixOrientation(bitmap);
		try {

			//Encode to Stringche
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
			Drawable drawable = new BitmapDrawable(getResources(), bitmap);


			int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				mBuildingPhotoButton.setBackgroundDrawable(drawable);
				mBuildingPhotoButton.setText(" ");
				//				boringPointPhoto.setText(" ");
			} else {
				mBuildingPhotoButton.setBackgroundDrawable(drawable);
				mBuildingPhotoButton.setText(" ");
				//				boringPointPhoto.setText(" ");
			}
			/*	int sdk = android.os.Build.VERSION.SDK_INT;
			   mBuildingPhotoButton.setBackground(drawable);
			   mBuildingPhotoButton.setImageDrawable(null);*/
			//mBuildingPhotoButton.setBackground(drawable);
			//			mBuildingPhotoButton.setText(" ");
			byteArray = byteArrayOutputStream .toByteArray();
			photo_Encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
			//				dateTv.setText(encoded);
			handlePhoto=byteArray;

		} catch (Exception e) {
			System.out.println("exception in byte data"+ e);
		}
		return byteArray;

	}
	public Bitmap fixOrientation(Bitmap mBitmap) {
		if (mBitmap.getWidth() > mBitmap.getHeight()) {
			Matrix matrix = new Matrix();
			matrix.postRotate(90);
			mBitmap = Bitmap.createBitmap(mBitmap , 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		}
		return mBitmap;
	}

	@Override
	public void onBeginningOfSpeech() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndOfSpeech() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(int error) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResults(Bundle results) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRmsChanged(float rmsdB) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {

		Toast.makeText(getApplicationContext(), "accuracy"+location.getAccuracy(), 0).show();


		if (location != null/*&&location.getAccuracy()<15*/) {

			loc = location;

			double latitude = loc.getLatitude(); 
			double longitude = loc.getLongitude();

			String lat=  String.valueOf(latitude);
			String longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);


			try {

				String[] dmsBoringLong = strLongitude.split(":"); 
				System.out.println("dmsBoringLong"+dmsBoringLong[0]+dmsBoringLong[1]+ dmsBoringLong[2]);
				mLongBoringDegree = dmsBoringLong[0];
				mLongBoringMinute = dmsBoringLong[1];
				longBoringsecond = dmsBoringLong[2];


				String[] longBoringMilliSec = longBoringsecond.split("\\."); 

				mLongBoringSeconds = longBoringMilliSec[0];
				longBoringMilliSeconds = longBoringMilliSec[1];
				mLongBoringMilliSeconds=longBoringMilliSeconds.substring(0, 2);


				//latitude split
				String[] dmsBoringLat = strLatitude.split(":");

				mLatBoringDegree = dmsBoringLat[0];
				mLatBoringMinute = dmsBoringLat[1];
				latBoringSecond = dmsBoringLat[2];

				String[] latBoringMilliSec = latBoringSecond.split("\\."); 

				mLatBoringSeconds = latBoringMilliSec[0];
				latBoringMilliSeconds = latBoringMilliSec[1];
				mLatBoringMilliSeconds=latBoringMilliSeconds.substring(0, 2);

			} catch (Exception e) {
				System.out.println("Exception Occurs strLongitude spliting" + e);
			}
			try
			{


				File logFile = new File("/sdcard/gps.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();                    
					} 
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("Exception Occurs /sdcard/gps.t " + e);
					}
				}         

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
				String dateNow = lat + longi;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();      
				latlong.setText(strLatitude+ ","  +strLongitude);
				Toast.makeText(this, "GPS Coordinates Received....", 0).show(); 
				locationManager.removeUpdates(this);
				prog.dismiss();

			}
			catch (IOException e)
			{

				e.printStackTrace();
			}


			/*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();	
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();	*/



		}


	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}


}


