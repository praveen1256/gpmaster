package com.hmwssb;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.model.GbContractorDetails;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DisconnectionDetailsActivity extends SuperActivity implements OnClickListener, AppConstants,LocationListener{


	Button mSaveBtn,mDisconnBoringPhoto;
	EditText mDisconnDateET;
	TextView mDiscFileNumber,mDiscCanNo,mDiscAddress;
	public ImageView signout, home ;

	//
	private GPSTracker gps;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

	StringBuilder ConnInfo = new StringBuilder();
	private String ConnXml;
	Spinner mDiscMeterSize;


	private Uri fileUri; // file url to store image/video

	protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
	protected static final int CAMERA_REQUEST = 0;
	static File   mediaStorageDir;
	Button b1;
	static File mediaFile;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	static ImageView iv,decodeIv;
	private static File DatePath;
	private String date;
	private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";
	public Boolean Flag=true;
	private String longBoringdegree;
	private String longBoringminute;
	private String latBoringDegree;
	public byte[] byteArray;
	private byte[] handlePhoto;
	private String photo_Encoded=null;
	private String latgBoringMinute;
	private String latgBoringSecond;

	private String mLongDiscBoringDegree;
	private String mLongDiscBoringMinute;
	private String longBoringsecond;
	private String mLatDiscBoringDegree;
	private String mLatDiscBoringMinute;
	private String latDiscBoringSecond;
	private String longBoringMilliSecond;
	private String latBoringMilliSecond;

	ProgressDialog prog;
	Location loc; 
	LocationManager locationManager;

	//
	private String lat;
	private String longi;
	private String mLongDiscBoringSeconds;
	private String longDiscBoringMilliSeconds;
	private String mLongDiscBoringMilliSeconds;
	private String mLatDiscBoringSeconds;
	private String latDiscBoringMilliSeconds;
	private String mLatDiscBoringMilliSeconds;
	HashMap<String, String> map ;
	private String mCustomerFileno;
	private GbContractorDetails gbContractorDetails;
	Transactions mTransobj;
	ArrayList<String> disconectionlist;

	String metersize;
	private EditText mDisconnLatAndLong;
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.disconn_details);

		mDisconnBoringPhoto=(Button)findViewById(R.id.Diss_Boaring_photo);
		mSaveBtn=(Button)findViewById(R.id.saveBtn);
		mDisconnDateET=(EditText)findViewById(R.id.Disscon_Date);
		mDisconnLatAndLong=(EditText)findViewById(R.id.Dislatlong);
		mDiscFileNumber=(TextView)findViewById(R.id.file_No);
		//mDiscCanNo=(TextView)findViewById(R.id.can_no);
		mDiscAddress=(TextView)findViewById(R.id.Address);
		mDiscMeterSize=(Spinner) findViewById(R.id.Disscon_MeterSize);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);
		mDisconnBoringPhoto.setOnClickListener(this);

		mTransobj = new Transactions();
		try {
			mTransobj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				finish();
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//			    logoutFromApp();
				finish();
				System.exit(0);
			}
		});
		gbContractorDetails = new GbContractorDetails();

		Bundle bundle = getIntent().getExtras();
		mCustomerFileno = bundle.getString(KEY_FILENO);  

		String Name = bundle.getString(KEY_NAME);  
		String Address = bundle.getString(KEY_Address);  
		mDiscFileNumber.setText(getString(R.string.customer_file_no, mCustomerFileno));
		mDiscAddress.setText("Name:"+Name+"\n"+"Address:"+Address);

		System.out.println(" the file numbermCustomerFileno "+mCustomerFileno);
		try
		{
			disconectionlist=	mTransobj.getDisconnection(mCustomerFileno);

			for(int i=0;i<disconectionlist.size();i++)
			{

				metersize=disconectionlist.get(0);
				date=disconectionlist.get(1);
				mDisconnDateET.setText(date);

				mLatDiscBoringDegree=disconectionlist.get(2);
				mLatDiscBoringMinute=disconectionlist.get(3);
				mLatDiscBoringSeconds=disconectionlist.get(4);
				mLatDiscBoringMilliSeconds=disconectionlist.get(5);
				mLongDiscBoringDegree=disconectionlist.get(6);
				mLongDiscBoringMinute=disconectionlist.get(7);
				mLongDiscBoringSeconds=disconectionlist.get(8);
				mLongDiscBoringMilliSeconds=disconectionlist.get(9);

				photo_Encoded=disconectionlist.get(10);
				int sdk = android.os.Build.VERSION.SDK_INT;

				byte[] decodedString = Base64.decode(photo_Encoded, Base64.DEFAULT);
				Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
				Drawable drawable = new BitmapDrawable(getResources(), decodedByte);
				mDisconnBoringPhoto.setBackground(drawable);
				if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
					mDisconnBoringPhoto.setBackground(drawable);
					mDisconnBoringPhoto.setText("");
				} else {

					mDisconnBoringPhoto.setBackground(drawable);
					mDisconnBoringPhoto.setText("");
				}


			}

		}
		catch(Exception e)
		{
			System.out.println("exception in reterving  data"+ e);
		}
		/*cFilenumber.setText("Fileno:"+map.get(AppConstants.KEY_FILENO));
		cAddress.setText(map.get(AppConstants.KEY_Address));
		cName.setText(map.get(AppConstants.KEY_NAME));*/

		mSaveBtn.setOnClickListener(this);
		RelativeLayout layout = (RelativeLayout)findViewById(R.id.disconnection_header);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("Enter Disconnection Details");


		//mDiscMeterSize=(Spinner) findViewById(R.id.Disscon_MeterSize);
		ArrayAdapter<String> aa=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item){
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				if (position == getCount()) {
					((TextView)v.findViewById(android.R.id.text1)).setText("");
					((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
				}

				return v;
			}       

			@Override
			public int getCount() {
				return super.getCount()-1; // you dont display last item. It is used as hint.
			}
		};
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		aa.add("Enter Disconnected Meter Size");
		aa.add("15");
		aa.add("20");
		aa.add("25");
		aa.add("40");
		aa.add("50");
		aa.add("100");
		aa.add("200");
		aa.add("300");

		mDiscMeterSize.setAdapter(aa);
		System.out.println("the size of array"+aa.getCount());
		int position=0;
		while(position<aa.getCount()){

			if(aa.getItem(position).equals(metersize)){
				mDiscMeterSize.setSelection(position);
				break;
			}
			position++;
		}
	}
	public String getCustomerFileno() {
		return mCustomerFileno;
	}
	String currentDateandTime;
	private Location oldLocation;
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.Diss_Boaring_photo:
		{
			captureImage();
		}

		break;
		case R.id.saveBtn:
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			currentDateandTime = sdf.format(new Date());

			//if (!checkEnteredInput()) {

			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo", Toast.LENGTH_LONG).show();
				return;
			}

			if((mDiscMeterSize.getSelectedItem().toString()).contains("Enter Disconnected Meter Size"))
			{
				Toast.makeText(getApplicationContext(), "Please Select  Meter Size", Toast.LENGTH_LONG).show();
				return;
			}
//			getBoringCoordinates();

			System.out.println("mLongDiscBoringDegree:"+mLongDiscBoringDegree+"mLongDiscBoringMinute:"+mLongDiscBoringMinute+"longBoringsecond:"+longBoringsecond);




			System.out.println("mLatDiscBoringDegree:"+mLatDiscBoringDegree+"mLatDiscBoringMinute:"+mLatDiscBoringMinute+"latDiscBoringSecond:"+latDiscBoringSecond);

			if(mLatDiscBoringDegree==null||mLatDiscBoringDegree.length()==0 )
			{
				Toast.makeText(getApplicationContext(), " Disconnection latitude and longitudes is manditaory", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLatDiscBoringMinute==null||mLatDiscBoringMinute.length()==0)
			{
				Toast.makeText(getApplicationContext(), "Invalid Disconnection latitude mintues", Toast.LENGTH_LONG).show();
				return;
			}

			if(latDiscBoringSecond==null||latDiscBoringSecond.length()==0)
			{
				Toast.makeText(getApplicationContext(), "Invalid Disconnection latitude Seconds", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongDiscBoringDegree==null||mLongDiscBoringDegree.length()==0 )
			{
				Toast.makeText(getApplicationContext(), " Disconnection longitudes and longitudes is manditaory  ", Toast.LENGTH_LONG).show();
				return;
			}



			try{


				if(disconectionlist.isEmpty()){
					PostConnectionInfo();
					System.out.println("********disconectionlist**********"+ConnXml);
					ContentValues PostDisconnectionDetailsvalues = new ContentValues();
					PostDisconnectionDetailsvalues.put("FileNo", mCustomerFileno);
					PostDisconnectionDetailsvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					PostDisconnectionDetailsvalues.put("DisConnectionPhoto", photo_Encoded);
					PostDisconnectionDetailsvalues.put("DisconnMeterSize", mDiscMeterSize.getSelectedItem().toString());
					PostDisconnectionDetailsvalues.put("DisconnectionDate", date);
					PostDisconnectionDetailsvalues.put("DisconnLatitudeDegrees", mLatDiscBoringDegree);
					PostDisconnectionDetailsvalues.put("DisconnLatitudeMilliSeconds", mLatDiscBoringMilliSeconds);
					PostDisconnectionDetailsvalues.put("DisconnLatitudeMinutes", mLatDiscBoringMinute);
					PostDisconnectionDetailsvalues.put("DisconnLatitudeSeconds", mLatDiscBoringSeconds);
					PostDisconnectionDetailsvalues.put("DisconnLongitudeDegrees", mLongDiscBoringDegree);
					PostDisconnectionDetailsvalues.put("DisconnLongitudeMilliSeconds", mLongDiscBoringMilliSeconds);
					PostDisconnectionDetailsvalues.put("DisconnLongitudeMinutes", mLongDiscBoringMinute);
					PostDisconnectionDetailsvalues.put("DisconnLongitudeSeconds", mLongDiscBoringSeconds);
					PostDisconnectionDetailsvalues.put("DisconnectionDetailsInfoXMLString",ConnXml);
					PostDisconnectionDetailsvalues.put("DisconnectionServerStatus", "finished");
					mTransobj.insert_PostDisconnectionDetailsInfo(PostDisconnectionDetailsvalues);

					Intent i = new Intent(DisconnectionDetailsActivity.this, NewConnection.class);
					//i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
					startActivity(i);
					setResult(DISCONNECTION_DETAILS);
					finish();
				}
				else
				{

					PostConnectionInfo();
					ContentValues updatedisconnection = new ContentValues();
					// PostDisconnectionDetailsvalues.put("FileNo", mCustomerFileno);
					updatedisconnection.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					updatedisconnection.put("DisConnectionPhoto", photo_Encoded);
					updatedisconnection.put("DisconnMeterSize", mDiscMeterSize.getSelectedItem().toString());
					updatedisconnection.put("DisconnectionDate", date);
					updatedisconnection.put("DisconnLatitudeDegrees", mLatDiscBoringDegree);
					updatedisconnection.put("DisconnLatitudeMilliSeconds", mLatDiscBoringMilliSeconds);
					updatedisconnection.put("DisconnLatitudeMinutes", mLatDiscBoringMinute);
					updatedisconnection.put("DisconnLatitudeSeconds", mLatDiscBoringSeconds);
					updatedisconnection.put("DisconnLongitudeDegrees", mLongDiscBoringDegree);
					updatedisconnection.put("DisconnLongitudeMilliSeconds", mLongDiscBoringMilliSeconds);
					updatedisconnection.put("DisconnLongitudeMinutes", mLongDiscBoringMinute);
					updatedisconnection.put("DisconnLongitudeSeconds", mLongDiscBoringSeconds);
					updatedisconnection.put("DisconnectionDetailsInfoXMLString",ConnXml);
					updatedisconnection.put("DisconnectionServerStatus", "finished");
					mTransobj.update_PostDisconnectionDetails(updatedisconnection, mCustomerFileno);

					Intent i = new Intent(DisconnectionDetailsActivity.this, NewConnection.class);
					// i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
					startActivity(i);
					setResult(DISCONNECTION_DETAILS);
					finish();
				}
				// PostConnectionInfo();

				setResult(DISCONNECTION_DETAILS);

				finish();
			}
			catch(Exception e)
			{
				System.out.println("exception in post connection  data"+ e);
			}
			//}


		}
		break;
		default:
			break;
		}
	}

	private boolean checkEnteredInput() {
		boolean flag=false;
		try
		{
			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo", Toast.LENGTH_LONG).show();
				flag=true;
			}

			if((mDiscMeterSize.getSelectedItem().toString()).contains("Enter Disconnected Meter Size"))
			{
				Toast.makeText(getApplicationContext(), "Please Select  Meter Size", Toast.LENGTH_LONG).show();
				flag=true;
			}

			if(mDisconnDateET.getText().toString().length()==0)
			{
				mDisconnDateET.requestFocus();
				mDisconnDateET.setError("FIELD CANNOT BE EMPTY");
				flag=true;
			}
			if(mDisconnLatAndLong.getText().toString().length()==0)
			{
				mDisconnDateET.requestFocus();
				mDisconnDateET.setError("FIELD CANNOT BE EMPTY");
				flag=true;
			}else {
				//mDisconnDateET.setError(null);
			}
		}
		catch(Exception e)
		{
			System.out.println("exception in onPostExecute  data"+ e);
		}

		return flag;
	}


	private String PostConnectionInfo() {
		String result=null;
		ConnInfo.append("<ConnInfo>");

		ConnInfo.append("<FileNo>");
		ConnInfo.append(mCustomerFileno);
		ConnInfo.append("</FileNo>");

		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<DisconnLatitudeDegrees>");
		ConnInfo.append(mLatDiscBoringDegree);
		ConnInfo.append("</DisconnLatitudeDegrees>");

		ConnInfo.append("<DisconnLatitudeMinutes>");
		ConnInfo.append(mLatDiscBoringMinute);
		ConnInfo.append("</DisconnLatitudeMinutes>");

		ConnInfo.append("<DisconnLatitudeSeconds>");
		ConnInfo.append(mLatDiscBoringSeconds);
		ConnInfo.append("</DisconnLatitudeSeconds>");

		ConnInfo.append("<DisconnLatitudeMilliSeconds>");
		ConnInfo.append(mLatDiscBoringMilliSeconds);
		ConnInfo.append("</DisconnLatitudeMilliSeconds>");

		ConnInfo.append("<DisconnLongitudeDegrees>");
		ConnInfo.append(mLongDiscBoringDegree);
		ConnInfo.append("</DisconnLongitudeDegrees>");

		ConnInfo.append("<DisconnLongitudeMinutes>");
		ConnInfo.append(mLongDiscBoringMinute);
		ConnInfo.append("</DisconnLongitudeMinutes>");

		ConnInfo.append("<DisconnLongitudeSeconds>");
		ConnInfo.append(mLongDiscBoringSeconds);
		ConnInfo.append("</DisconnLongitudeSeconds>");

		ConnInfo.append("<DisconnLongitudeMilliSeconds>");
		ConnInfo.append(mLongDiscBoringMilliSeconds);
		ConnInfo.append("</DisconnLongitudeMilliSeconds>");

		ConnInfo.append("<DisConnectionPhoto>");
		ConnInfo.append(photo_Encoded);
		ConnInfo.append("</DisConnectionPhoto>");

		ConnInfo.append("<DisconnMeterSize>");
		ConnInfo.append(mDiscMeterSize.getSelectedItem().toString());
		ConnInfo.append("</DisconnMeterSize>");

		ConnInfo.append("<DisconnLedgerNo>");
		ConnInfo.append("asd123");
		ConnInfo.append("</DisconnLedgerNo>");


		ConnInfo.append("<DisconnectionDate>");
		ConnInfo.append(currentDateandTime);
		ConnInfo.append("</DisconnectionDate>");

		ConnInfo.append("</ConnInfo>");

		ConnXml=ConnInfo.toString();

		Log.v(ConnInfo.toString(), "XML");

		//CheckNetworkStatus();

		AsyncCallWS task = new AsyncCallWS();
		//Call execute
		task.execute();

		return result;
	}

	private class AsyncCallWS extends AsyncTask<Void, Void, String> {
		String result;
		//	AE7654
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(Void... params) {
			try {
				result=	GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostDisconnectionDetailsInfo");
				System.out.println("****************************result values of PostDisconnectionDetailsInfo"+result);


			} catch (Exception e) {
				System.out.println("exception in AsyncCallWS  data"+ e);
			}


			//GbPostPendingDataToServer.invokePendingFiles();
			//		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			String spliteresult=null;
			try {
				if(result.contains("failed"))
				{

					Toast.makeText(getApplicationContext(), "Please Check Internet", Toast.LENGTH_LONG).show();
					ContentValues updatedisconnection = new ContentValues();
					updatedisconnection.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					updatedisconnection.put("DisconnectionDetailsInfoWebServiceStatus", result);
					mTransobj.update_PostDisconnectionDetails(updatedisconnection, mCustomerFileno);
				}
				else
				{
					String arr[]=result.split("|");
					spliteresult=arr[1];
					System.out.println("the split result valuesa arr[0]:"+result+"******************the split values of arr[1]:"+arr[1]);


				}
				if(spliteresult.contains("0"))
				{
					Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();

				}
				if(spliteresult.contains("1"))
				{
					Toast.makeText(getApplicationContext(), "Data SuccessFully Sent", 500).show();

					ContentValues updatedisconnection = new ContentValues();
					// PostDisconnectionDetailsvalues.put("FileNo", mCustomerFileno);
					updatedisconnection.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					updatedisconnection.put("DisconnectionDetailsInfoWebServiceStatus", spliteresult);
					mTransobj.update_PostDisconnectionDetails(updatedisconnection, mCustomerFileno);
				}

			} catch (Exception e) {
				System.out.println("exception in onPostExecute  data"+ e);
			}


		}

	}

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

//		getBoringCoordinates();
	}
	private void getBoringCoordinates() {/*
		gps = new GPSTracker(this);


		// check if GPS enabled
		if(gps.canGetLocation()){
			gps = new GPSTracker(this);


			// check if GPS enabled
			if(gps.canGetLocation()){



				double latitude = gps.getLatitude();
				double longitude = gps.getLongitude();

				lat=  String.valueOf(latitude);
				longi=  String.valueOf(longitude);

				String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
				String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);

				try {

					String[] dmsDiscBoringLong = strLongitude.split(":");

					mLongDiscBoringDegree = dmsDiscBoringLong[0];
					mLongDiscBoringMinute = dmsDiscBoringLong[1];
					longBoringsecond = dmsDiscBoringLong[2];

					String[] longDiscBoringMilliSec = longBoringsecond.split("\\.");

					mLongDiscBoringSeconds = longDiscBoringMilliSec[0];
					longDiscBoringMilliSeconds = longDiscBoringMilliSec[1];
					mLongDiscBoringMilliSeconds=longDiscBoringMilliSeconds.substring(0, 2);
					//			longBoringMilliSecond=dmsBoringLong[3];

					//latitude split
					String[] dmsDiscBoringLat = strLatitude.split(":");
					mLatDiscBoringDegree = dmsDiscBoringLat[0];
					mLatDiscBoringMinute = dmsDiscBoringLat[1];
					latDiscBoringSecond = dmsDiscBoringLat[2];

					String[] latDiscBoringMilliSec = latDiscBoringSecond.split("\\.");

					mLatDiscBoringSeconds = latDiscBoringMilliSec[0];
					latDiscBoringMilliSeconds = latDiscBoringMilliSec[1];
					mLatDiscBoringMilliSeconds=latDiscBoringMilliSeconds.substring(0, 2);
				} catch (Exception e) {
					System.out.println("exception in disconnection spliting  data"+ e);
				}

				//			latBoringMilliSecond=dmsBoringLat[3];
				try
				{
					//	if (filDate.length<4) {

					File logFile = new File("/sdcard/gpstest.txt");
					if (!logFile.exists())
					{
						try
						{
							logFile.createNewFile();
						}
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("exception in disconnection /sdcard/gpstest"+ e);
						}
					}

					BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
					String dateNow = lat + longi;
					buf.append(dateNow+";");
					buf.newLine();
					buf.close();

				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
				Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();


			}else{
				gps.showSettingsAlert();
			}

		}else{
			gps.showSettingsAlert();
		}

	 */gpsFinding();
	}

	public void gpsFinding() {
		try {
			loc = null;
			prog = new ProgressDialog(this);
			prog.setTitle("Please Wait...!");
			prog.setMessage("Searching for GPS Coordinates...");
			prog.setCancelable(false);
			prog.show();

			locationManager = (LocationManager)getSystemService("location");

			//			LocationListener locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1L, 1F, this);
			//			locationManager.requestLocationUpdates("gps", 1L, 1F, this);

			if (locationManager != null)
			{
				oldLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}


		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
			DatePath= mediaFile;

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	private void previewCapturedImage() {
		try {

			ExifInterface exif = null ;
			try {
				exif = new ExifInterface(DatePath.getPath());
				date=exif.getAttribute(ExifInterface.TAG_DATETIME);
				mDisconnDateET.setText(date);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			getPhotoToByteArray();
			getBoringCoordinates();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public byte [] getPhotoToByteArray() {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(mediaFile.getPath(),
				options);
		bitmap=fixOrientation(bitmap);
		try {

			//Encode to Stringche
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
			Drawable drawable = new BitmapDrawable(getResources(), bitmap);
			int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				mDisconnBoringPhoto.setBackgroundDrawable(drawable);
				mDisconnBoringPhoto.setText("");
			} else {
				mDisconnBoringPhoto.setBackgroundDrawable(drawable);
				mDisconnBoringPhoto.setText("");
			}


			//mDisconnBoringPhoto.setBackground(drawable);
			byteArray = byteArrayOutputStream .toByteArray();
			photo_Encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
			//				dateTv.setText(encoded);
			handlePhoto=byteArray;

		} catch (Exception e) {
			System.out.println("exception in disconnection byte array"+ e);
		}
		return byteArray;

	}

	public Bitmap fixOrientation(Bitmap mBitmap) {
		if (mBitmap.getWidth() > mBitmap.getHeight()) {
			Matrix matrix = new Matrix();
			matrix.postRotate(90);
			mBitmap = Bitmap.createBitmap(mBitmap , 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		}
		return mBitmap;
	}
	@Override
	public void onLocationChanged(Location location) {

		Toast.makeText(getApplicationContext(), "accuracy"+location.getAccuracy(), 0).show();


//		if (location != null&&location.getAccuracy()<14) {
		if (location != null) {

			loc = location;

			double latitude = loc.getLatitude();
			double longitude = loc.getLongitude();


			lat=  String.valueOf(latitude);
			longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);

			try {

				String[] dmsDiscBoringLong = strLongitude.split(":");

				mLongDiscBoringDegree = dmsDiscBoringLong[0];
				mLongDiscBoringMinute = dmsDiscBoringLong[1];
				longBoringsecond = dmsDiscBoringLong[2];

				String[] longDiscBoringMilliSec = longBoringsecond.split("\\.");

				mLongDiscBoringSeconds = longDiscBoringMilliSec[0];
				longDiscBoringMilliSeconds = longDiscBoringMilliSec[1];
				mLongDiscBoringMilliSeconds=longDiscBoringMilliSeconds.substring(0, 2);
				//			longBoringMilliSecond=dmsBoringLong[3];

				//latitude split
				String[] dmsDiscBoringLat = strLatitude.split(":");
				mLatDiscBoringDegree = dmsDiscBoringLat[0];
				mLatDiscBoringMinute = dmsDiscBoringLat[1];
				latDiscBoringSecond = dmsDiscBoringLat[2];

				String[] latDiscBoringMilliSec = latDiscBoringSecond.split("\\.");

				mLatDiscBoringSeconds = latDiscBoringMilliSec[0];
				latDiscBoringMilliSeconds = latDiscBoringMilliSec[1];
				mLatDiscBoringMilliSeconds=latDiscBoringMilliSeconds.substring(0, 2);
			} catch (Exception e) {
				System.out.println("exception in disconnection spliting  data"+ e);
			}

			//			latBoringMilliSecond=dmsBoringLat[3];
			try
			{
				//	if (filDate.length<4) {

				File logFile = new File("/sdcard/gps.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("exception in disconnection /sdcard/gps"+ e);
					}
				}

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				String dateNow = lat + longi;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();

				mDisconnLatAndLong.setText(strLatitude+";"+strLongitude);
				Toast.makeText(this, "GPS Coordinates Received....", 0).show();
				locationManager.removeUpdates(this);
				prog.dismiss();

			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		/*	Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
*/

		}
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}


}
