package com.hmwssb;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.Sms_service;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomerFeedback extends SuperActivity implements OnClickListener , AppConstants {
	EditText File_NOET,Cus_NameET,AddresET,RemarksET,OTPET;
	Spinner Feedback;
	Button mSaveBtn;
	private ProgressDialog mProgressDialog;
	StringBuilder ConnInfo = new StringBuilder();
	private String mFileno;
	private String ConnXml;
	private String mCustomerName;
	private String mCustomerAddress;
	private Transactions mTransobj;
	ArrayList<String>feedbecklist;
	String strfeedback;
	private Button otpBtn;
	private String mobilenum,strusereneterotp;
	private String uniqueCode=null;
	public ImageView signout, home;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.customer_feedback);

		mTransobj = new Transactions();
		try {
			mTransobj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		File_NOET = (EditText)findViewById(R.id.File_No);
		Cus_NameET = (EditText)findViewById(R.id.Cus_Name);
		AddresET = (EditText)findViewById(R.id.Address);
		RemarksET = (EditText)findViewById(R.id.Remarks);
		OTPET = (EditText)findViewById(R.id.OTP);
		mSaveBtn = (Button)findViewById(R.id.saveBtn);
		mSaveBtn.setOnClickListener(this);
		Feedback = (Spinner)findViewById(R.id.spinner1);
		otpBtn = (Button)findViewById(R.id.otpBtn);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);

		otpBtn.setOnClickListener(this);

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.new_connection);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("Customer FeedBack");

		Bundle bundle = getIntent().getExtras();
		mFileno = bundle.getString(KEY_FILENO); 
		mCustomerName = bundle.getString(KEY_NAME); 
		mCustomerAddress = bundle.getString(KEY_Address); 

		File_NOET.setText(mFileno);
		Cus_NameET.setText(mCustomerName);
		AddresET.setText(mCustomerAddress);

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				finish();
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				logoutFromApp();
			}
		});

		try{
			ArrayList<String> filelist=mTransobj.getfiledetails(mFileno);
			System.out.println("******************************filelist"+filelist.size());
			for(int i=0;i<filelist.size();i++)
			{
				mobilenum=filelist.get(3);
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occurs in getfiledetails--- " + e);

		}


		feedbecklist=mTransobj.getPostCustomerFeedBackDetails(mFileno);
		System.out.println("******************************feedbecklist"+feedbecklist.size());
		if(!feedbecklist.isEmpty())

		{
			for(int i=0;i<feedbecklist.size();i++)
			{
				System.out.println("******************************feedbecklist"+feedbecklist.get(0)+feedbecklist.get(1)+feedbecklist.get(2)+feedbecklist.get(3));
				RemarksET.setText(feedbecklist.get(0));	
				strfeedback=feedbecklist.get(1);

			}
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {

			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				if (position == getCount()) {
					((TextView)v.findViewById(android.R.id.text1)).setText("");
					((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
				}

				return v;
			}       

			@Override
			public int getCount() {
				return super.getCount()-1; // you dont display last item. It is used as hint.
			}

		};

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter.add("AVERAGE");
		adapter.add("BAD");
		adapter.add("EXCELLENT");
		adapter.add("GOOD");

		adapter.add("Select FeedBack");

		Feedback.setAdapter(adapter);
		Feedback.setSelection(adapter.getCount()); //display hint

		int position=0;
		while(position<adapter.getCount()){

			if(adapter.getItem(position).equals(strfeedback)){
				Feedback.setSelection(position);
				break;
			}
			position++;
		}

	}

	private Boolean checkField() {
		boolean flag=false;
		try
		{

			if((Feedback.getSelectedItem().toString()).contains("Select FeedBack"))
			{
				Toast.makeText(getApplicationContext(), "Please Select FeedBack", Toast.LENGTH_LONG).show();
				flag=true;
			}



		}
		catch(Exception e)
		{
			System.out.println("Exception Occurs in checkField--- " + e);	
		}
		return flag;
	}

	private void PostConnectionInfo() {
		String sppinerid = null;
		strfeedback = Feedback.getSelectedItem().toString();
		if((Feedback.getSelectedItem().toString()).contains("AVERAGE"));
		{
			sppinerid="3";
		}
		if((Feedback.getSelectedItem().toString()).contains("BAD"));
		{
			sppinerid="1";
		}
		if((Feedback.getSelectedItem().toString()).contains("EXCELLENT"));
		{
			sppinerid="5";
		}
		if((Feedback.getSelectedItem().toString()).contains("GOOD"));
		{
			sppinerid="4";
		}

		ConnInfo.append("<ConnInfo>");
		ConnInfo.append("<FileNo>");
		ConnInfo.append(mFileno);
		ConnInfo.append("</FileNo>");
		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<CustomerRemarks>");
		ConnInfo.append(RemarksET.getText().toString());
		ConnInfo.append("</CustomerRemarks>");

		ConnInfo.append("<CustomerStarRating>");
		ConnInfo.append(sppinerid);
		ConnInfo.append("</CustomerStarRating>");

		ConnInfo.append("</ConnInfo>");


		ConnXml=ConnInfo.toString();

		Log.v(ConnInfo.toString(), "XML");
		/*if (isServiceAvailable) {
			CheckNetworkStatus(mFileno);
		}*/
		//		else {
		//		generateGBBill(mFileno);
		try {
			BoringConnection mCangenerateStatus = mTransobj.getBoringConnection(mFileno);
			//			BoringConnection boringConnection = new BoringConnection();

			if (!mCangenerateStatus.isGbAcceptedForCanGeneration()) {
				CheckNetworkStatus(mFileno);
			}
			AsyncCallWS task = new AsyncCallWS();
			task.execute();
		} catch (Exception e) {
		}

		//		}

	}


	private class AsyncCallWS extends AsyncTask<Void, Void, String> {
		String result;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*mProgressDialog = ProgressDialog.show(CustomerFeedback.this, "",
					"Loading. Please wait...", true);*/
		}

		//	AE7654
		@Override
		protected String doInBackground(Void... params) {
			try {
				System.out.println("ConnXml customer+"+ConnXml);
				result= GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostCustomerFeedBackDetailsInfo");
			} catch (Exception e) {
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {

			try {
				String splitresult=null;
				if(result.contains("Failed"))
				{
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
					ContentValues customerfeedbackvalues =new ContentValues();
					customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					customerfeedbackvalues.put("CustomerFeedBackWebServiceStatus",result);
					mTransobj.update_PostCustomerFeedBackDetails(customerfeedbackvalues,mFileno);
				}
				else
				{
					String arr[]=result.split("|");
					splitresult=arr[1];
					System.out.println("the split result valuesa arr[0]:"+result+"******************the split values of arr[1]:"+arr[1]);


				}
				if(splitresult.contains("0"))
				{
					//					Toast.makeText(getApplicationContext(),result , Toast.LENGTH_SHORT).show();
				}

				if(splitresult.contains("1"))
				{
					Toast.makeText(getApplicationContext(), "Customer Feedback send Successfully", Toast.LENGTH_SHORT).show();
					ContentValues customerfeedbackvalues =new ContentValues();
					customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
					customerfeedbackvalues.put("CustomerFeedBackWebServiceStatus",splitresult);
					mTransobj.update_PostCustomerFeedBackDetails(customerfeedbackvalues,mFileno);
				}

			} catch (Exception e) {
				System.out.println("Exception Occurs in AsyncCallWS--- " + e);
			}


		}

	}

	private class AsyncCallGetOTPWS extends AsyncTask<Void, Void, String> {
		//	AE7654
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = ProgressDialog.show(CustomerFeedback.this, "",
					"Sending OTP. Please wait...", true);
		}
		@Override
		protected String doInBackground(Void... params) {
			try {
				Sms_service.pullSmsFromServer(mobilenum, "Please handover the secret Pin "+String.valueOf(uniqueCode)+" for your Water Connection (File No: "+mFileno+") to GB Contractor", "SENDTESTSMS");
				System.out.println("**********uniqueCode**************"+uniqueCode);
				mProgressDialog.dismiss();

			} catch (Exception e) {

			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			try {
				//	mProgressDialog.dismiss();
				/*
				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
				 */} catch (Exception e) {
					 System.out.println("Exception Occurs in AsyncCallGetOTPWS--- " + e);
				 }


		}

	}
	@Override
	protected void onPause() {
		super.onPause();
		//		mProgressDialog.dismiss();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.otpBtn:
		{

			int uniqueotp = AppUtils.generateRandomDigits();
			uniqueCode = String.valueOf(uniqueotp);
			//send generated  OTP to consumer
			AsyncCallGetOTPWS task = new AsyncCallGetOTPWS();
			task.execute();
			if(feedbecklist.isEmpty())
			{
				ContentValues customerfeedbackvalues =new ContentValues();
				customerfeedbackvalues.put("FileNo",mFileno);
				customerfeedbackvalues.put("CustomerFeedBackOTP",uniqueCode);
				mTransobj.insert_PostCustomerFeedBackDetailsInfo(customerfeedbackvalues);
			}
			else
			{
				ContentValues customerfeedbackvalues =new ContentValues();
				customerfeedbackvalues.put("CustomerFeedBackOTP",uniqueCode);
				mTransobj.update_PostCustomerFeedBackDetails(customerfeedbackvalues,mFileno);

			}

		}
		break;
		case R.id.saveBtn:
		{
			strusereneterotp =	OTPET.getText().toString();
			try
			{

				if (!checkField()) {
					/************************customer feedback details********************
					 * sudhir
					 */

					/*if(strusereneterotp.length()==0)
				{
					Toast.makeText(getApplicationContext(), "Please enter valid OTP", Toast.LENGTH_LONG).show();
					return;
				}*/
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
					String currentDateandTime = sdf.format(new Date());
					if(feedbecklist.isEmpty())
					{
						PostConnectionInfo();
						ContentValues customerfeedbackvalues =new ContentValues();
						customerfeedbackvalues.put("FileNo",mFileno);
						customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
						customerfeedbackvalues.put("CustomerRemarks",RemarksET.getText().toString());
						customerfeedbackvalues.put("CustomerStarRating",Feedback.getSelectedItem().toString());
						customerfeedbackvalues.put("FeedBackServerStatus","finished");
						customerfeedbackvalues.put("CustomerFeedBackDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
						customerfeedbackvalues.put("CustomerFeedBackDetailsInfoCreatedDtm",currentDateandTime);
						customerfeedbackvalues.put("CustomerFeedBackOTP",uniqueCode);
						customerfeedbackvalues.put("CustomerFeedBackXMLString",ConnXml);
						mTransobj.insert_PostCustomerFeedBackDetailsInfo(customerfeedbackvalues);
						feedbecklist=mTransobj.getPostCustomerFeedBackDetails(mFileno);
						/*if(feedbecklist.get(3)==null)
						{
							Toast.makeText(getApplicationContext(), "Please click on Send OTP button ", Toast.LENGTH_LONG).show();
							return;

						}*/
						//strusereneterotp.contains(feedbecklist.get(3))   ||
						/*	if(strusereneterotp==null)
						{
							PostConnectionInfo();
							ContentValues customerfeedbackvalues1 =new ContentValues();

							customerfeedbackvalues1.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
							customerfeedbackvalues1.put("CustomerRemarks",RemarksET.getText().toString());
							customerfeedbackvalues1.put("CustomerStarRating",Feedback.getSelectedItem().toString());
							customerfeedbackvalues1.put("FeedBackServerStatus","finished");
							customerfeedbackvalues1.put("CustomerFeedBackDetailsInfoCreateByUid",SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
							customerfeedbackvalues1.put("CustomerFeedBackDetailsInfoCreatedDtm",currentDateandTime);
							customerfeedbackvalues1.put("CustomerFeedBackXMLString",ConnXml);
							customerfeedbackvalues1.put("CustomerFeedBackOTP",uniqueCode);
							mTransobj.update_PostCustomerFeedBackDetails(customerfeedbackvalues1,mFileno);
						}*/
						/*else
				{
					Toast.makeText(getApplicationContext(), "Please enter valid OTP ", Toast.LENGTH_LONG).show();
					return;
				}*/

						Intent i = new Intent(CustomerFeedback.this, NewConnection.class);
						//						i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
						startActivity(i);
						finish();
					}
					else
					{
						//check customer otp
						feedbecklist=mTransobj.getPostCustomerFeedBackDetails(mFileno);
						System.out.println("otp in db"+feedbecklist.get(3));
						System.out.println("otp in textfield"+OTPET.getText().toString());

						/*if(feedbecklist.get(3)==null)
						{
							Toast.makeText(getApplicationContext(), "Please click on Send OTP button ", Toast.LENGTH_LONG).show();
							return;

						}*/
						//					  ||
						if ( OTPET.getText().toString().trim().equals("")) {
							PostConnectionInfo();
							ContentValues customerfeedbackvalues =new ContentValues();

							customerfeedbackvalues.put("GBNo", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
							customerfeedbackvalues.put("CustomerRemarks",RemarksET.getText().toString());
							customerfeedbackvalues.put("CustomerStarRating",Feedback.getSelectedItem().toString());
							customerfeedbackvalues.put("FeedBackServerStatus","finished");
							customerfeedbackvalues.put("CustomerFeedBackDetailsInfoCreateByUid", SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, ""));
							customerfeedbackvalues.put("CustomerFeedBackDetailsInfoCreatedDtm",currentDateandTime);
							customerfeedbackvalues.put("CustomerFeedBackXMLString",ConnXml);
							customerfeedbackvalues.put("CustomerFeedBackOTP",uniqueCode);
							mTransobj.update_PostCustomerFeedBackDetails(customerfeedbackvalues,mFileno);

							Intent i = new Intent(CustomerFeedback.this, NewConnection.class);
							i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
							startActivity(i);
							finish();
						} 
						else {
							OTPET.setError("Invalid OTP");
							return;
						}


					}
					//PostConnectionInfo();
					finish();
				}

			}
			catch(Exception e)
			{
				System.out.println("the exception feed checking"+e);
			}

		}

		break;

		default:
			break;
		}}
}
