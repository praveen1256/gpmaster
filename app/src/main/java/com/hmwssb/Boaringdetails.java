package com.hmwssb;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Boaringdetails extends SuperActivity implements OnClickListener, AppConstants, LocationListener{


	private TextView mFilenumber, mAddress;
	private Button mSaveBoringDetailsButton,boringPointPhoto;
	//	private ImageView boringPointPhoto;
	private EditText latandlong,photodate,lengthofconnection,boringdate;
	//
	//	private GPSTracker gps;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

	private Uri fileUri; // file url to store image/video

	protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
	protected static final int CAMERA_REQUEST = 0;
	static File   mediaStorageDir;
	Button b1;
	static File mediaFile;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	static ImageView iv,decodeIv;
	private static File DatePath;
	//	public TextView dateTv;
	private String date;
	private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";
	public Boolean Flag=true;
	private String mLongBoringDegree;
	private String mLongBoringMinute;
	private String longBoringsecond;
	private String mLatBoringDegree;
	public byte[] byteArray;
	private byte[] handlePhoto;
	private String photo_Encoded;
	private String mLatBoringMinute;
	Bitmap bitmap ;
	private String latBoringSecond;
	private String longBoringMilliSecond;
	private String latBoringMilliSecond;
	private String lat;
	private String longi;
	StringBuilder ConnInfo = new StringBuilder();
	private String lenghtOfConnection;
	private String ConnXml;
	private String mCustomerFileno;
	private SimpleDateFormat dateFormatter;
	private ProgressDialog mProgressDialog;
	private DatePickerDialog datePicker;
	private String mBoringDatetoServer;
	private BoringConnection bConn;
	private Transactions transobj;
	ArrayList<String> boaringdetailslist;
	String allotmentdate;
	public ImageView signout, home;
	private String mSelfConnStatus;

	ProgressDialog prog;
	Location loc; 
	LocationManager locationManager;
	private LocationListener mlocListener;

	//
	@SuppressWarnings("deprecation")
	@SuppressLint({ "NewApi", "SimpleDateFormat" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.boaringdetails);
		dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		findViewsById();
		transobj = new Transactions();
		try {
			transobj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}


		/*GetSystemDate disasyn=new GetSystemDate();
		disasyn.execute();
		 */
		photodate.setEnabled(false);

		//
		//

		/*Intent in = getIntent();
		map = (HashMap<String, String>) in.getSerializableExtra(AppConstants.GB_DATA);*/

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.new_connection);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
				//				finish();
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//			    logoutFromApp();
				finish();
				System.exit(0);
			}
		});

		lenghtOfConnection=lengthofconnection.getText().toString();
		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);

		textView.setText("Enter Boring Details");


		Bundle bundle = getIntent().getExtras();
		mCustomerFileno = bundle.getString(KEY_FILENO);  
		String Name = bundle.getString(KEY_NAME);  
		String Address = bundle.getString(KEY_Address);  
		mFilenumber.setText(getString(R.string.customer_file_no, mCustomerFileno +System.getProperty("line.separator")+"Name:"+Name));
		mAddress.setText("Address:"+Address);


		/**************************************
		 * allotmentdate date
		 */

		//		mSelfConnStatus=transobj.getPendingdetails()(mCustomerFileno);


		mSelfConnStatus=	transobj.GetSelfConnStatus(mCustomerFileno);



		boaringdetailslist = transobj.getBoringDetailsInfo(mCustomerFileno);

		try{
			for (int i = 0; i < boaringdetailslist.size(); i++) {


				lenghtOfConnection=boaringdetailslist.get(0);
				mLatBoringDegree=boaringdetailslist.get(1);
				mLatBoringMinute=boaringdetailslist.get(2);
				mLatBoringSeconds=boaringdetailslist.get(3);
				mLatBoringMilliSeconds=boaringdetailslist.get(4);
				mLongBoringDegree=boaringdetailslist.get(5);
				mLongBoringMinute=boaringdetailslist.get(6);
				mLongBoringSeconds=boaringdetailslist.get(7);
				mLongBoringMilliSeconds=boaringdetailslist.get(8);
				photo_Encoded=boaringdetailslist.get(9);
				mBoringDatetoServer=boaringdetailslist.get(11);

				boringdate.setText(mBoringDatetoServer);
				lengthofconnection.setText(lenghtOfConnection);
				latandlong.setText(mLatBoringDegree+mLatBoringMinute+mLatBoringSeconds+mLatBoringMilliSeconds+","+mLongBoringDegree+mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);
				int sdk = android.os.Build.VERSION.SDK_INT;
				byte[] decodedString = Base64.decode(photo_Encoded, Base64.DEFAULT);
				Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
				Drawable drawable = new BitmapDrawable(getResources(), decodedByte);
				/*	boringPointPhoto.setImageBitmap(bitmap);
				boringPointPhoto.setImageDrawable(null);*/

				boringPointPhoto.setBackgroundDrawable(drawable);
				if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
					boringPointPhoto.setBackgroundDrawable(drawable);
					boringPointPhoto.setText(" ");

				} else {
					boringPointPhoto.setBackground(drawable);
					boringPointPhoto.setText(" ");
				}
				photodate.setText(boaringdetailslist.get(10));

			}
		}
		catch(Exception e)
		{

		}


	}


	private void findViewsById() {	
		mFilenumber=(TextView)findViewById(R.id.cus_fileno);
		mAddress=(TextView)findViewById(R.id.cus_address);
		boringPointPhoto=(Button)findViewById(R.id.boring_point_photo);
		latandlong=(EditText)findViewById(R.id.latandlongofboringpoint);
		photodate=(EditText)findViewById(R.id.boringphotodate);

		boringdate=(EditText)findViewById(R.id.dateofboring);
		lengthofconnection=(EditText)findViewById(R.id.lengthofconnection);
		mSaveBoringDetailsButton=(Button)findViewById(R.id.savebutton);
		mSaveBoringDetailsButton.setOnClickListener(this);
		boringPointPhoto.setOnClickListener(this);


		boringdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				/*		GetSystemDate disasyn=new GetSystemDate();
				disasyn.execute();*/
				/*				new DatePickerDialog(Boaringdetails.this, date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
				/*DatePickerDialog mDatePickerDialog =  */
				new DatePickerDialog(Boaringdetails.this, date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();

				//				 mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

			}
		});


	}

	public String getCustomerFileno() {
		return mCustomerFileno;
	}


	private void updateLabel() {
		String myFormat = "dd/MM/yyyy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
		Calendar cal = Calendar.getInstance();
		// change in DB
		String currenttime = new String(sdf.format(cal.getTime()));
		try {
			Date currentDate = sdf.parse(currenttime);
			//			Toast.makeText(getApplicationContext(), "date is "+currenttime, 500).show();
			String userBoringDate = new String(sdf.format(myCalendar.getTime()));
			Date boringDateset=sdf.parse(userBoringDate);

			/* long diff = currentDate.getTime() - boringDateset.getTime();
		        long seconds = diff / 1000;
		        long minutes = seconds / 60;
		        long hours = minutes / 60;
		        long days = hours / 24;

		        if (boringDateset.before(currentDate)) {

		            Log.e("oldDate", "is previous date");
		            Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
		                    + " hours: " + hours + " days: " + days);

		        }*/
			//for setting curent date
			if (System.currentTimeMillis() < boringDateset.getTime()) {
				mBoringDatetoServer=sdf.format(cal.getTime());
			}else {
				mBoringDatetoServer=sdf.format(myCalendar.getTime());
			}



		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		//		mBoringDatetoServer=sdf.format(myCalendar.getTime());
		int i= OTPSet.getStatus();
		if(i==1)
		{


			boringdate.setText(mBoringDatetoServer);

		}
		else
		{

			boringdate.setText(mBoringDatetoServer);

		}
	}
	Calendar myCalendar = Calendar.getInstance();

	DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {


			try {
				myCalendar.set(Calendar.YEAR, year);
				myCalendar.set(Calendar.MONTH, monthOfYear);
				myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				if (mSelfConnStatus.equals("1")) {
					updateSelfConnStatus();
				}else {
					updateLabel();
				}
			} catch (Exception e) {
			}


		}

		private void updateSelfConnStatus() {
			String myFormat = "dd/MM/yyyy"; //In which you need put here
			SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
			Calendar cal = Calendar.getInstance();
			String currenttime = new String(sdf.format(cal.getTime()));
			try {
				Date currentDate = sdf.parse(currenttime);
				String userBoringDate = new String(sdf.format(myCalendar.getTime()));
				Date boringDateset=sdf.parse(userBoringDate);
				if (System.currentTimeMillis() < boringDateset.getTime()) {
					mBoringDatetoServer=sdf.format(cal.getTime());
				}else {
					mBoringDatetoServer=sdf.format(boringDateset.getTime());
				}
			} catch (ParseException e) {
				e.printStackTrace();
			} 
			int i= OTPSet.getStatus();
			if(i==1)
			{
				boringdate.setText(mBoringDatetoServer);
			}
			else
			{
				boringdate.setText(mBoringDatetoServer);
			}
		}			

	};
	private String mLongBoringSeconds;
	private String longBoringMilliSeconds;
	private String mLongBoringMilliSeconds;
	private String mLatBoringSeconds;
	private String latBoringMilliSeconds;
	private String mLatBoringMilliSeconds;
	private Location oldLocation;




	private boolean checkEnteredInput() {
		boolean flag=false;
		boolean result=false;
		try
		{

			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo", Toast.LENGTH_LONG).show();
				flag=true;
			}
			/*if(mLongBoringDegree.contains("0.0")||mLongBoringDegree==null||mLongBoringDegree.length()==0)
		{
			Toast.makeText(getApplicationContext(), "Lat And Long Values  are Null", Toast.LENGTH_LONG).show();
			flag=true;	
		}
		if(mLongBoringDegree==null)
		{
			Toast.makeText(getApplicationContext(), "Lat And Long Values  are Null", Toast.LENGTH_LONG).show();
			flag=true;		
		}*/
			if(mBoringDatetoServer==null)
			{
				Toast.makeText(getApplicationContext(), "Please Select Boaring Date", Toast.LENGTH_LONG).show();
				flag=true;	
			}   
			else
			{
				allotmentdate=	transobj.GBAllotmentDate(mCustomerFileno);
				System.out.println("******************allotmentdate"+allotmentdate);
				try {
					Date date = null;

					date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(allotmentdate);
					String format = new SimpleDateFormat("dd/MM/yyyy").format(date);



					result=	differentDateFormat(format,mBoringDatetoServer);
					System.out.println("the return is  are===>" + result);
					if(result==false)
					{
						System.out.println("the result is  are===>" + result);
						Toast.makeText(getApplicationContext(), "Selceted Date Must Be Greater than allotment date"+allotmentdate, Toast.LENGTH_LONG).show();
						flag=true;	
					}
					System.out.println(format);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}





			lenghtOfConnection=lengthofconnection.getText().toString();

			if(photodate.getText().toString().length()==0)
			{
				photodate.requestFocus();
				photodate.setError("FIELD CANNOT BE EMPTY");
				flag=true;
			}
			else {
				photodate.setError(null);
			}

			if(lenghtOfConnection.length()==0)
			{
				lengthofconnection.requestFocus();
				lengthofconnection.setError("INVALID DETAILS");
				flag=true;
			}

			else {
				lengthofconnection.setError(null);
			}
		}
		catch(Exception e)
		{
			System.out.println("the error "+e);
		}

		return flag;
	}

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

		//		getBoringCoordinates();
	}
	@SuppressLint("SdCardPath")
	private void getBoringCoordinates() {
		gpsFinding();
		/*
		gps = new GPSTracker(this);


		// check if GPS enabled
		if(gps.canGetLocation()){
			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();

			lat=  String.valueOf(latitude);
			longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);

			try {

				String[] dmsBoringLong = strLongitude.split(":");

				mLongBoringDegree = dmsBoringLong[0];
				mLongBoringMinute = dmsBoringLong[1];
				longBoringsecond = dmsBoringLong[2];

				String[] longBoringMilliSec = longBoringsecond.sp
				mLongBoringSeconds = longBoringMilliSec[0];lit("\\.");

				longBoringMilliSeconds = longBoringMilliSec[1];
				mLongBoringMilliSeconds=longBoringMilliSeconds.substring(0, 2);
				//			longBoringMilliSecond=dmsBoringLong[3];

				//latitude split
				String[] dmsBoringLat = strLatitude.split(":");
				mLatBoringDegree = dmsBoringLat[0];
				mLatBoringMinute = dmsBoringLat[1];
				latBoringSecond = dmsBoringLat[2];

				String[] latBoringMilliSec = latBoringSecond.split("\\.");

				mLatBoringSeconds = latBoringMilliSec[0];
				latBoringMilliSeconds = latBoringMilliSec[1];
				mLatBoringMilliSeconds=latBoringMilliSeconds.substring(0, 2);


				latandlong.setText(mLatBoringDegree+mLatBoringMinute+mLatBoringSeconds+mLatBoringMilliSeconds+","+mLongBoringDegree+mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);



			} catch (Exception e) {
				// TODO: handle exception
			}

			//			latBoringMilliSecond=dmsBoringLat[3];
			try
			{
				//	if (filDate.length<4) {

				File logFile = new File("/sdcard/gpstest.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				String dateNow = lat + longi + ":"+gps.getAccuracy() ;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();

			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();



		}else{
			gps.showSettingsAlert();
		}

		 */}

	public void gpsFinding() {
		try {
			loc = null;
			prog = new ProgressDialog(this);
			prog.setTitle("Please Wait...!");
			prog.setMessage("Searching for GPS Coordinates...");
			prog.setCancelable(false);
			prog.show();

			locationManager = (LocationManager)getSystemService("location");

			//			LocationListener locationListener = new MyLocationListener();
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1L, 1F, this);
			//			locationManager.requestLocationUpdates("gps", 1L, 1F, this);

			if (locationManager != null)
			{
				oldLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}


	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		try {
			if (!mediaStorageDir.exists()) {
				if (!mediaStorageDir.mkdirs()) {
					Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
							+ IMAGE_DIRECTORY_NAME + " directory");
					return null;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		// Create the storage directory if it does not exist


		if (mediaStorageDir==null) {
			System.out.println("directory not created");
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
			DatePath= mediaFile;

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	private void previewCapturedImage() {
		try {

			ExifInterface exif = null ;
			try {
				exif = new ExifInterface(mediaFile.getPath());

				date=exif.getAttribute(ExifInterface.TAG_DATETIME);
				photodate.setText(date);
				photodate.setEnabled(false);
				//latandlong.setText(mLatBoringDegree+mLatBoringMinute+mLatBoringSeconds+mLatBoringSeconds+ "," +mLongBoringDegree+ mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			getPhotoToByteArray();
			getBoringCoordinates();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public byte [] getPhotoToByteArray() {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		bitmap = BitmapFactory.decodeFile(mediaFile.getPath(),
				options);
		bitmap= fixOrientation( bitmap);

		try {

			//Encode to Stringche
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
			Drawable drawable = new BitmapDrawable(getResources(), bitmap);


			int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				boringPointPhoto.setBackgroundDrawable(drawable);
				boringPointPhoto.setText(" ");
				//				boringPointPhoto.setText(" ");
			} else {
				boringPointPhoto.setBackgroundDrawable(drawable);
				boringPointPhoto.setText(" ");
				//				boringPointPhoto.setText(" ");
			}


			byteArray = byteArrayOutputStream .toByteArray();
			photo_Encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
			//				dateTv.setText(encoded);
			handlePhoto=byteArray;

		} catch (Exception e) {
		}
		return byteArray;

	}
	private class AsyncCallWS extends AsyncTask<Void, Void, String> {


		String result;
		//	AE7654
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		//	AE7654
		@Override
		protected String doInBackground(Void... params) {
			try {
				System.out.println("in  PostBoringDetailsInfo ConnXml"+ConnXml);
				result=	GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostBoringDetailsInfo");
			} catch (Exception e) {


			}


			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			//			mProgressDialog.dismiss();
			String spliteresult=null;
			try {
				if(result.contains("failed"))
				{
					ContentValues PostBoringDetailsvalues= new ContentValues();
					PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", result);
					transobj.update_PostBoringDetails(PostBoringDetailsvalues, mCustomerFileno);
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();

				}
				else
				{


					String arr[]=result.split("|");
					spliteresult=arr[1];

					System.out.println("the split result valuesa arr[0]:"+spliteresult+"******************the split values of arr[1]"+spliteresult);


				}


				if(spliteresult.contains("0"))
				{

					Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();

				}

				if(spliteresult.contains("1"))
				{
					ContentValues PostBoringDetailsvalues= new ContentValues();
					PostBoringDetailsvalues.put("BoringDetailsInfoWebServiceStatus", spliteresult);
					transobj.update_PostBoringDetails(PostBoringDetailsvalues, mCustomerFileno);
					Toast.makeText(getApplicationContext(), "Boring Details Successfuly Send", Toast.LENGTH_SHORT).show();
				}
				/*if(result.contains("f"))
				{
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
				}*/


			} catch (Exception e) {
			}


		}

	}
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.boring_point_photo:
		{
			captureImage();
		}

		break;

		case R.id.savebutton:
		{
			//if (!checkEnteredInput()) {
			lenghtOfConnection=lengthofconnection.getText().toString();
			lenghtOfConnection=trimLeadingZeros(lenghtOfConnection);
			if(photo_Encoded==null)
			{
				Toast.makeText(getApplicationContext(), "Please Capture Photo", Toast.LENGTH_LONG).show();
				return;
			}

			//			getBoringCoordinates();
			System.out.println("mLatBoringDegree"+mLatBoringDegree+"mLatBoringMinute:"+mLatBoringMinute+"mLatBoringSeconds"+mLatBoringSeconds+mLatBoringMilliSeconds+","+mLongBoringDegree+mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);
			if(mLatBoringDegree==null||mLatBoringDegree.length()==0 || mLatBoringDegree.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Boaringdetails longitudes and latitiudes is manditaory  ", Toast.LENGTH_LONG).show();
				return;
			}



			if(mLatBoringMinute==null||mLatBoringMinute.length()==0 || mLatBoringMinute.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Invalid Boaringdetails latitude mintues", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLatBoringSeconds==null||mLatBoringSeconds.length()==0 || mLatBoringSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "Invalid Boaringdetails latitude Seconds", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLatBoringMilliSeconds==null||mLatBoringMilliSeconds.length()==0 || mLatBoringMilliSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "Invalid Boaringdetails latitude MilliSeconds", Toast.LENGTH_LONG).show();
				return;
			}
			System.out.println("mLatBoringMilliSeconds:"+mLatBoringMilliSeconds+"mLongBoringDegree:"+mLongBoringDegree+"mLongBoringMinute:"+mLongBoringMinute);
			if(mLongBoringDegree==null||mLongBoringDegree.length()==0||mLongBoringDegree.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "Invalid Boaringdetails longitudes and", Toast.LENGTH_LONG).show();
				return;
			}

			if(mLongBoringMinute==null||mLongBoringMinute.length()==0||mLongBoringMinute.contains("null"))
			{
				Toast.makeText(getApplicationContext(), " Invalid Boaringdetails longitudes mintues", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongBoringSeconds==null||mLongBoringSeconds.length()==0|| mLongBoringSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "Invalid Boaringdetails longitudes Seconds", Toast.LENGTH_LONG).show();
				return;
			}
			if(mLongBoringMilliSeconds==null||mLongBoringMilliSeconds.length()==0||mLongBoringMilliSeconds.contains("null"))
			{
				Toast.makeText(getApplicationContext(), "Invalid Boaringdetails longitude MilliSeconds", Toast.LENGTH_LONG).show();
				return;
			}

			boolean result=false;
			if(mBoringDatetoServer==null)
			{
				Toast.makeText(getApplicationContext(), "Please Select Boaring Date", Toast.LENGTH_LONG).show();
				return;
			}
			else
			{
				if (mSelfConnStatus.equals("1")) {

				}else {
					allotmentdate=	transobj.GBAllotmentDate(mCustomerFileno);
					System.out.println("******************allotmentdate"+allotmentdate);
					try {
						Date date = null;

						date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(allotmentdate);
						String format = new SimpleDateFormat("dd/MM/yyyy").format(date);



						result=	differentDateFormat(format,mBoringDatetoServer);
						System.out.println("the return is  are===>" + result);
						if(result==false)
						{
							System.out.println("the result is  are===>" + result);
							Toast.makeText(getApplicationContext(), "Selected Date Must Be Greater", Toast.LENGTH_LONG).show();
							return;
						}
						System.out.println(format);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

			}

			if(lenghtOfConnection==null||lenghtOfConnection.equals("0") || lenghtOfConnection.trim().length()==0)
			{
				Toast.makeText(getApplicationContext(), " lenght Of Connection must be greater than zero", Toast.LENGTH_LONG).show();
				return;

			}


			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
			String currentDateandTime = sdf.format(new Date());
			if(boaringdetailslist.isEmpty())
			{
				PostConnectionInfo();
				System.out.println("********SharedPreferenceUtils**********"+(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "")));
				ContentValues PostBoringDetailsvalues= new ContentValues();

				PostBoringDetailsvalues.put("FileNo", mCustomerFileno);
				PostBoringDetailsvalues.put("GBNo", (SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") ));
				PostBoringDetailsvalues.put("BoringLatitudeDegrees", mLatBoringDegree);
				PostBoringDetailsvalues.put("BoringLatitudeMilliSeconds", mLatBoringMilliSeconds);
				PostBoringDetailsvalues.put("BoringLatitudeMinutes", mLatBoringMinute);
				PostBoringDetailsvalues.put("BoringLatitudeSeconds", mLatBoringSeconds);
				PostBoringDetailsvalues.put("BoringLongitudeDegrees", mLongBoringDegree);
				PostBoringDetailsvalues.put("BoringLongitudeMilliSeconds",mLongBoringMilliSeconds);
				PostBoringDetailsvalues.put("BoringLongitudeMinutes", mLongBoringMinute);
				PostBoringDetailsvalues.put("BoringLongitudeSeconds", mLongBoringSeconds);
				PostBoringDetailsvalues.put("BoringPointPhoto", photo_Encoded);
				//PostBoringDetailsvalues.put("BoringLongitudeSeconds", mLongBoringSeconds);BoringDetailsInfoXMLString
				PostBoringDetailsvalues.put("LengthOfConnection", lenghtOfConnection);
				PostBoringDetailsvalues.put("DateOfConnection", mBoringDatetoServer);
				PostBoringDetailsvalues.put("BoringDetailsInfoXMLString", ConnXml);
				PostBoringDetailsvalues.put("BoringServerStatus", "finished");
				PostBoringDetailsvalues.put("BoringDetailsInfoCreateByUid", (SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") ));
				PostBoringDetailsvalues.put("BoringDetailsInfoCreatedDtm", currentDateandTime);
				transobj.insert_PostBoringDetailsInfo(PostBoringDetailsvalues);

				Intent i = new Intent(Boaringdetails.this, NewConnection.class);
				startActivity(i);
				//	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
				setResult(BORING_CONNECTION);
				finish();
			}
			else
			{
				/**********************update boaring details ***************************
				 * sudhir
				 */
				PostConnectionInfo();
				ContentValues PostBoringDetailsvalues= new ContentValues();

				PostBoringDetailsvalues.put("GBNo", (SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") ));
				PostBoringDetailsvalues.put("BoringLatitudeDegrees", mLatBoringDegree);
				PostBoringDetailsvalues.put("BoringLatitudeMilliSeconds", mLatBoringMilliSeconds);
				PostBoringDetailsvalues.put("BoringLatitudeMinutes", mLatBoringMinute);
				PostBoringDetailsvalues.put("BoringLatitudeSeconds", mLatBoringSeconds);
				PostBoringDetailsvalues.put("BoringLongitudeDegrees", mLongBoringDegree);
				PostBoringDetailsvalues.put("BoringLongitudeMilliSeconds",mLongBoringMilliSeconds);
				PostBoringDetailsvalues.put("BoringLongitudeMinutes", mLongBoringMinute);
				PostBoringDetailsvalues.put("BoringLongitudeSeconds", mLongBoringSeconds);
				PostBoringDetailsvalues.put("BoringPointPhoto", photo_Encoded);
				//PostBoringDetailsvalues.put("BoringLongitudeSeconds", mLongBoringSeconds);
				PostBoringDetailsvalues.put("LengthOfConnection", lenghtOfConnection);
				PostBoringDetailsvalues.put("DateOfConnection", mBoringDatetoServer);
				PostBoringDetailsvalues.put("BoringDetailsInfoXMLString", ConnXml);
				PostBoringDetailsvalues.put("BoringServerStatus", "finished");
				PostBoringDetailsvalues.put("BoringDetailsInfoCreateByUid", (SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") ));
				PostBoringDetailsvalues.put("BoringDetailsInfoCreatedDtm", currentDateandTime);
				transobj.update_PostBoringDetails(PostBoringDetailsvalues, mCustomerFileno);

				Intent i = new Intent(Boaringdetails.this, NewConnection.class);


				startActivity(i);
				setResult(BORING_CONNECTION);
				finish();

			}



			//
			//PostConnectionInfo();
			setResult(BORING_CONNECTION);
			finish();
		}



		//}
		break;

		default:
			break;
		}

	}
	public String trimLeadingZeros(String source)
	{   String s=source;
	String removeString="";

	for(int i =0;i<s.length();i++){
		if(s.charAt(i)=='0')
			removeString=removeString+"0";
		else
			break;
	}

	System.out.println("original string - "+s);

	System.out.println("after removing 0's -"+s.replaceFirst(removeString,""));
	return s.replaceFirst(removeString,"");

	}

	private void PostConnectionInfo() {
		ConnInfo.append("<ConnInfo>");

		ConnInfo.append("<FileNo>");
		ConnInfo.append(mCustomerFileno);
		ConnInfo.append("</FileNo>");

		ConnInfo.append("<GBNO>");
		ConnInfo.append(SharedPreferenceUtils.getPreference(getApplicationContext(), AppConstants.IPreferencesConstants.GB_CODE, "") );
		ConnInfo.append("</GBNO>");

		ConnInfo.append("<BoringLatitudeDegrees>");
		ConnInfo.append(mLatBoringDegree);
		ConnInfo.append("</BoringLatitudeDegrees>");

		ConnInfo.append("<BoringLatitudeMinutes>");
		ConnInfo.append(mLatBoringMinute);
		ConnInfo.append("</BoringLatitudeMinutes>");

		ConnInfo.append("<BoringLatitudeSeconds>");
		ConnInfo.append(mLatBoringSeconds);
		ConnInfo.append("</BoringLatitudeSeconds>");

		ConnInfo.append("<BoringLatitudeMilliSeconds>");
		ConnInfo.append(mLatBoringMilliSeconds);
		ConnInfo.append("</BoringLatitudeMilliSeconds>");

		ConnInfo.append("<BoringLongitudeDegrees>");
		ConnInfo.append(mLongBoringDegree);
		ConnInfo.append("</BoringLongitudeDegrees>");

		ConnInfo.append("<BoringLongitudeMinutes>");
		ConnInfo.append(mLongBoringMinute);
		ConnInfo.append("</BoringLongitudeMinutes>");

		ConnInfo.append("<BoringLongitudeSeconds>");
		ConnInfo.append(mLongBoringSeconds);
		ConnInfo.append("</BoringLongitudeSeconds>");

		ConnInfo.append("<BoringLongitudeMilliSeconds>");
		ConnInfo.append(mLongBoringMilliSeconds);
		ConnInfo.append("</BoringLongitudeMilliSeconds>");

		ConnInfo.append("<BoringPointPhoto>");
		ConnInfo.append(photo_Encoded);
		ConnInfo.append("</BoringPointPhoto>");

		ConnInfo.append("<DateOfConnection>");
		ConnInfo.append(mBoringDatetoServer);
		ConnInfo.append("</DateOfConnection>");

		ConnInfo.append("<LengthOfConnection>");
		ConnInfo.append(lenghtOfConnection);
		ConnInfo.append("</LengthOfConnection>");

		ConnInfo.append("</ConnInfo>");

		ConnXml=ConnInfo.toString();

		Log.v(ConnInfo.toString(), "XML");

		CheckNetworkStatus(mCustomerFileno);

		AsyncCallWS task = new AsyncCallWS();
		//Call execute
		task.execute();
	}
	public static int daysBetween(Date dateFrom, Date dateTo) {
		return (int) ((dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24));
	}

	private static boolean differentDateFormat(String strSmalldate,String strBigDate) {
		boolean status=false;
		try {

			System.out.println("the strSmalldate are===>" + strSmalldate);
			System.out.println("the strBigDate are===>" + strBigDate);
			DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
			Calendar c1 = Calendar.getInstance();
			c1.add(Calendar.DATE, 0);
			String strTodayDate = df1.format(c1.getTime());
			System.out.println("Today Date is======" + strTodayDate);
			Date BigDate = null;
			Date Smalldate = null;
			BigDate = df1.parse(strBigDate);
			Smalldate = df1.parse(strSmalldate);
			int Betweendays = daysBetween(Smalldate, BigDate);

			if(Betweendays>=0)
			{
				status=true;
			}
			System.out.println("the Betweendays are===>" + Betweendays);
			System.out.println("the STATUS is  are===>" + status);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception Occurs differentDateFormat" + e);
		}
		return status;
	}

	public Bitmap fixOrientation(Bitmap mBitmap) {
		if (mBitmap.getWidth() > mBitmap.getHeight()) {
			Matrix matrix = new Matrix();
			matrix.postRotate(90);
			mBitmap = Bitmap.createBitmap(mBitmap , 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		}
		return mBitmap;
	}

	//date Asynck


	private class GetSystemDate extends AsyncTask<Void, String, String> {
		String result;
		@Override
		protected String doInBackground(Void... params) {
			try {
				result=	GbPostPendingDataToServer.GetSystemDate("GetSystemDate");
				System.out.println("****************************GetSystemDate "+result);
			} catch (Exception e) {
				System.out.println("exception in GetSystemDate error"+ e);
			}
			return result;
		}
		@SuppressWarnings("unused")
		@Override
		protected void onPostExecute(String result) {

			try {
				//				Toast.makeText(getApplicationContext(), "sytemDate:"+result, Toast.LENGTH_LONG).show();
				System.out.println("****************************result  GetSystemDate"+result);
				if(result.contains("failed"))
				{
					Toast.makeText(getApplicationContext(), "Please Check Internet Connection", Toast.LENGTH_LONG).show();
					finish();
					return;
				}
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date date = new Date();
				System.out.println(dateFormat.format(date)); 
				System.out.println("formattedDate*************************"+dateFormat.format(date));
				if(dateFormat.format(date).equals(result))
				{
					System.out.println("inside ifloopin getsystem date "+dateFormat.format(date));
					return;
				}
				else
				{
					startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
					finish();
				}


			} catch (Exception e) {
				System.out.println("Exception Occurs Getversioncontrol " + e);
			}


		}

	}
	@Override
	public void onLocationChanged(Location location) {
		Toast.makeText(getApplicationContext(), "accuracy"+location.getAccuracy(), 0).show();

			if (location != null /*&& location.getAccuracy()<14*/) {
//		if (location != null) {
			loc = location;

			double latitude = loc.getLatitude(); 
			double longitude = loc.getLongitude();


			/*double latitude = gps.getLatitude();
				double longitude = gps.getLongitude();*/

			lat=  String.valueOf(latitude);
			longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);

			try {

				String[] dmsBoringLong = strLongitude.split(":"); 

				mLongBoringDegree = dmsBoringLong[0];
				mLongBoringMinute = dmsBoringLong[1];
				longBoringsecond = dmsBoringLong[2];

				String[] longBoringMilliSec = longBoringsecond.split("\\."); 

				mLongBoringSeconds = longBoringMilliSec[0];
				longBoringMilliSeconds = longBoringMilliSec[1];
				mLongBoringMilliSeconds=longBoringMilliSeconds.substring(0, 2);
				//			longBoringMilliSecond=dmsBoringLong[3];

				//latitude split
				String[] dmsBoringLat = strLatitude.split(":"); 
				mLatBoringDegree = dmsBoringLat[0];
				mLatBoringMinute = dmsBoringLat[1];
				latBoringSecond = dmsBoringLat[2];

				String[] latBoringMilliSec = latBoringSecond.split("\\."); 

				mLatBoringSeconds = latBoringMilliSec[0];
				latBoringMilliSeconds = latBoringMilliSec[1];
				mLatBoringMilliSeconds=latBoringMilliSeconds.substring(0, 2);


				latandlong.setText(mLatBoringDegree+mLatBoringMinute+mLatBoringSeconds+mLatBoringMilliSeconds+","+mLongBoringDegree+mLongBoringMinute+mLongBoringSeconds+mLongBoringMilliSeconds);

			} catch (Exception e) {
				// TODO: handle exception
			}

			//			latBoringMilliSecond=dmsBoringLat[3];
			try
			{
				//	if (filDate.length<4) {

				File logFile = new File("/sdcard/gps.txt");
				if (!logFile.exists())
				{
					try
					{
						logFile.createNewFile();                    
					} 
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}         

				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
				String dateNow = mCustomerFileno+";"+lat +";"+ longi ;
				buf.append(dateNow+";");
				buf.newLine();
				buf.close();    
				Toast.makeText(this, "GPS Coordinates Received....", 0).show(); 
				locationManager.removeUpdates(this);
				prog.dismiss();


			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block                    
				e.printStackTrace();
			}


			/*Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();	
				Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();	*/







		}

	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

}
