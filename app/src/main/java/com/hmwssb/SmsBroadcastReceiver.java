package com.hmwssb;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.vajra.utils.AppConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.SharedPreferenceUtils;

public class SmsBroadcastReceiver extends BroadcastReceiver {

	public static final String SMS_BUNDLE = "pdus";
	public  String smsBody;

	public void onReceive(Context context, Intent intent) {
		Bundle intentExtras = intent.getExtras();
		if (intentExtras != null) {
			Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
			String smsMessageStr = "";
			for (int i = 0; i < sms.length; ++i) {
				SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
				smsBody = smsMessage.getMessageBody().toString();
				String address = smsMessage.getOriginatingAddress();
				smsMessageStr += "SMS From: " + address + "\n";
				smsMessageStr += smsBody + "\n";
			}
			int generatedOtp = SharedPreferenceUtils.getPreference(context, AppConstants.IPreferencesConstants.UNIQUE_OTP, 0);
			if (smsMessageStr.contains(generatedOtp+"")) {
				AppUtils.showToast(context, smsMessageStr);
				 Intent i = new Intent();
                 i.setClassName("com.vajra.hmwssb", "com.vajra.hmwssb.HomeScreenTabActivity");
                 i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                 context.startActivity(i);
			/*	Intent i = new Intent(context, HomeScreenTabActivity.class);
				context.startActivity(i);*/
			}else{
				Toast.makeText(context, "receiver not unique :" + smsMessageStr, Toast.LENGTH_SHORT).show();
			}
			//this will update the UI with message
			/* SmsActivity inst = SmsActivity.instance();
            inst.updateList(smsMessageStr);*/
		}
	}

}
