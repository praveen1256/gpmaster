package com.hmwssb;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.hmwssb.utility.Utility;
import com.vajra.service.GPSTracker;
import com.vajra.service.GbPostPendingDataToServer;
import com.vajra.service.UrlConstants;
import com.vajra.utils.AppConstants;

public class Screen3 extends Activity implements OnClickListener,UrlConstants {
	EditText gb_DOConnET,gb_LengthOfConnectionET,gb_LedgerNoET,gb_MeterNoET,gb_MeterMakeET,gb_IntialMtrReadET,gb_MetrSizeET,gb_MeterWarrentyET,gb_CustmerFbET,gb_CustmerOtpET;
	Button gb_CaptMetrPhtoBt,gb_DiscDetailsBt,gb_Photo_GpsCordnts,gb_sendDataToServerBt;
	RatingBar gb_ratingToGb;


	//	JSONObject webJS = new JSONObject();

	//
	// Activity request codes
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

	private Uri fileUri; // file url to store image/video

	protected static final int CAPTURE_IMAGE_CAPTURE_CODE = 0;
	protected static final int CAMERA_REQUEST = 0;
	static File   mediaStorageDir;
	Button b1;
	static File mediaFile;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	static ImageView iv,decodeIv;
	private static File DatePath;
	//	public TextView dateTv;
	private String date;
	private static final String IMAGE_DIRECTORY_NAME = "HMWSSBDEMO";
	public Boolean Flag=true;
	//
	private DatePickerDialog datePicker;
	private SimpleDateFormat dateFormatter;
	public byte[] byteArray;
	private byte[] handlePhoto;
	private String photo_Encoded;
	private String DateOfConn;
	private String LengthOfConnec;
	private String LedgerNo;
	private String MeterNum;
	private String MeterMakes;
	private String InitialMtrRead;
	private String MeterSizes;
	private String MeterWarrenty;
	private String CustmerFB;
	


	GPSTracker gps;


	StringBuilder ConnInfo = new StringBuilder();
	private String Custmrating;
	private String ConnXml;
	private String longBoringdegree;
	private String longBoringminute;
	private String longBoringsecond;
	private String latBoringDegree;
	private String latgBoringMinute;
	private String latgBoringSecond;
	private String longBoringMilliSecond;
	private String latBoringMilliSecond;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DisplayMetrics displaymetrics = new DisplayMetrics();   
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics); 

		setContentView(R.layout.screen3);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		Intent in = getIntent();
		int position = in.getIntExtra(AppConstants.POSITION, 0);
//		HashMap<String, String> map = WebService.newItems.get(position);
		
		dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

		findViewsById();
		setDateTimeField();



		/*if (Flag) {

			checkEnteredInput();

		}*/
		gb_sendDataToServerBt.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {

				//				webServiceCalling();
				PostConnectionInfo(1);

			}
		});


		gb_CaptMetrPhtoBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				captureImage();
			}
		});
		gb_Photo_GpsCordnts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!checkEnteredInput()) {
					/*Intent gpsPoints = new  Intent(getApplicationContext(), Screen2.class);
					startActivity(gpsPoints);*/
				}

			}
		});
	}

	private void findViewsById() {
		// TODO Auto-generated method stub
		gb_DOConnET=(EditText)findViewById(R.id.editText1);
		gb_LengthOfConnectionET=(EditText)findViewById(R.id.editText2);
		gb_LedgerNoET=(EditText)findViewById(R.id.dateofboring);
		gb_MeterNoET=(EditText)findViewById(R.id.lengthofconnection);
		gb_MeterMakeET=(EditText)findViewById(R.id.editText5);
		gb_IntialMtrReadET=(EditText)findViewById(R.id.editText6);
		gb_MetrSizeET=(EditText)findViewById(R.id.editText7);
		gb_MeterWarrentyET=(EditText)findViewById(R.id.editText8);
		gb_MeterWarrentyET.setInputType(InputType.TYPE_NULL);
		gb_CustmerFbET=(EditText)findViewById(R.id.editText9);
		gb_CustmerOtpET=(EditText)findViewById(R.id.editText10);
		gb_CaptMetrPhtoBt=(Button)findViewById(R.id.camera_Mp);
		gb_DiscDetailsBt=(Button)findViewById(R.id.disconnection_Details);
		gb_Photo_GpsCordnts=(Button)findViewById(R.id.photo_GpsCordn);
		gb_sendDataToServerBt=(Button)findViewById(R.id.sendToServerBt);
		gb_ratingToGb=(RatingBar)findViewById(R.id.ratingBar1);

	}


	/*private void webServiceCalling() {


		try {
			SharedPreferences pref = getSharedPreferences(OTPActivity.MyPREFERENCES, MODE_PRIVATE); 

			AsyncCallWS task = new AsyncCallWS();
			//Call execute 
			task.execute();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}*/
	private void setDateTimeField() {
		gb_MeterWarrentyET.setOnClickListener(this);
		//		toDateEtxt.setOnClickListener(this);

		Calendar newCalendar = Calendar.getInstance();
		datePicker = new DatePickerDialog(this, new OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				gb_MeterWarrentyET.setText(dateFormatter.format(newDate.getTime()));
			}

		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

	}

	private boolean checkEnteredInput() {
		boolean flag=false;

		DateOfConn=gb_DOConnET.getText().toString();
		LengthOfConnec = gb_LengthOfConnectionET.getText().toString();
		LedgerNo=gb_LedgerNoET.getText().toString();
		MeterNum = gb_MeterNoET.getText().toString();
		MeterMakes=gb_MeterMakeET.getText().toString();
		InitialMtrRead = gb_IntialMtrReadET.getText().toString();
		MeterSizes=gb_MetrSizeET.getText().toString();

		MeterWarrenty=gb_MeterWarrentyET.getText().toString();
		CustmerFB = gb_CustmerFbET.getText().toString();
		Custmrating=String.valueOf(gb_ratingToGb.getRating());  

		if(DateOfConn.length()==0)
		{
			gb_DOConnET.requestFocus();
			gb_DOConnET.setError("FIELD CANNOT BE EMPTY");
			flag=true;
		}else {
			gb_DOConnET.setError(null);
		}
		if(LengthOfConnec.length()==0|LengthOfConnec.length()>21)
		{
			gb_LengthOfConnectionET.requestFocus();
			gb_LengthOfConnectionET.setError("FIELD CANNOT BE EMPTY");
			flag=true;
		}
		else {
			gb_LengthOfConnectionET.setError(null);
		}
		if(LedgerNo.length()==0 | LedgerNo.length()>15 )
		{
			gb_LedgerNoET.requestFocus();
			gb_LedgerNoET.setError("ENTER ONLY ALPHABETICAL CHARACTER");
			flag=true;
		}
		else {
			gb_LedgerNoET.setError(null);

		}
		if (MeterNo.length()==0) {
			gb_MeterNoET.requestFocus();
			gb_MeterNoET.setError("ENTER ONLY ALPHABETICAL CHARACTER");
			flag=true;
		}else {
			gb_MeterNoET.setError(null);

		}

		if (MeterMake.length()==0) {
			gb_MeterMakeET.requestFocus();
			gb_MeterMakeET.setError("ENTER ONLY ALPHABETICAL CHARACTER");
			flag=true;
		}else {
			gb_MeterMakeET.setError(null);

		}

		if (MeterSize.length()==0) {
			gb_MetrSizeET.requestFocus();
			gb_MetrSizeET.setError("ENTER MeterSize");
			flag=true;
		}else {
			gb_MetrSizeET.setError(null);

		}

		if (InitialMtrRead.length()==0) {
			gb_IntialMtrReadET.requestFocus();
			gb_IntialMtrReadET.setError("ENTER MeterRead");
			flag=true;
		}else {
			gb_IntialMtrReadET.setError(null);

		}
		if (MeterWarrenty.length()==0) {
			gb_MeterWarrentyET.requestFocus();
			gb_MeterWarrentyET.setError("ENTER Date");
			flag=true;
		}
		else {
			gb_IntialMtrReadET.setError(null);

		}
		return flag;
	}

	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

//		getBoringCoordinates();
	}

	private void getBoringCoordinates() {
		gps = new GPSTracker(this);


		// check if GPS enabled		
		if(gps.canGetLocation()){
			
			

			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();
			
			String lat=  String.valueOf(latitude);
			String longi=  String.valueOf(longitude);

			String strLongitude = Location.convert(longitude, Location.FORMAT_SECONDS);
			String strLatitude = Location.convert(latitude, Location.FORMAT_SECONDS);
			

			String[] dmsBoringLong = strLongitude.split(":"); 
			
			longBoringdegree = dmsBoringLong[0];
			longBoringminute = dmsBoringLong[1];
			longBoringsecond = dmsBoringLong[2];
//			longBoringMilliSecond=dmsBoringLong[3];
			
			
			String[] dmsBoringLat = strLongitude.split(":"); 
			 latBoringDegree = dmsBoringLat[0];
			latgBoringMinute = dmsBoringLat[1];
			latgBoringSecond = dmsBoringLat[2];
//			latBoringMilliSecond=dmsBoringLat[3];
			try
			{
			//	if (filDate.length<4) {
				
					File logFile = new File("/sdcard/gps.txt");
					if (!logFile.exists())
					{
						try
						{
							logFile.createNewFile();                    
						} 
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}         

					//BufferedWriter for performance, true to set append to file flag
					BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
					String dateNow = lat + longi;
					buf.append(dateNow+";");
					buf.newLine();
					buf.close();                  

			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + strLongitude + "\nLong: " + strLatitude, Toast.LENGTH_LONG).show();	
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();	

				

		}else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {

			ExifInterface exif = null ;
			try {
				exif = new ExifInterface(DatePath.getPath());
				date=exif.getAttribute(ExifInterface.TAG_DATETIME);
				/*String[] date1=date.split(" ");
				if (date1.length>0) {
					String date=date1[0];
					gb_DOConnET.setText(date);
				}
				 */
				gb_DOConnET.setText(date);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			// hide video preview

			//			iv.setVisibility(View.VISIBLE);

			/*			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;

			Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
					options);*/

			//			iv.setImageBitmap(bitmap);


			getPhotoToByteArray();
			/*
			try {

				//Encode to String
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
				bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
				byte[] byteArray = byteArrayOutputStream .toByteArray();
				//		to encode base64 from byte array use following method
				String photo_Encoded = android.util.Base64.encodeToString(byteArray, android.util.Base64.NO_WRAP);
				//				dateTv.setText(encoded);



				//Decode to BItmap		
				byte[] decodedString = Base64.decode(photo_Encoded, Base64.DEFAULT);
				Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
				decodeIv.setImageBitmap(decodedByte);
			} catch (Exception e) {
				// TODO: handle exception
			}
			 */
			//Decode And Encode Ended//
			//paint over image  

			/*			
			 try {
			        FileOutputStream out = new FileOutputStream(DatePath);

			        // NEWLY ADDED CODE STARTS HERE [

			            Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
			            Canvas canvas = new Canvas(mutableBitmap);
			            Paint paint = new Paint();
			            paint.setColor(Color.RED); // Text Color
			            paint.setStrokeWidth(80); // Text Size
//			       paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
			            // some more settings...

			            canvas.drawBitmap(mutableBitmap, 80, 80, paint);
			            canvas.drawText(date, 200, 200, paint);
			        // NEWLY ADDED CODE ENDS HERE ]

			        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			        out.flush();
			        out.close();
			    } catch (Exception e) {
			       e.printStackTrace();
			    }

			 */		
			//end of paint
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}


	public byte [] getPhotoToByteArray() {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
				options);
		try {

			//Encode to Stringche
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
			byteArray = byteArrayOutputStream .toByteArray();
			photo_Encoded = android.util.Base64.encodeToString(byteArray, android.util.Base64.NO_WRAP);
			//				dateTv.setText(encoded);
			handlePhoto=byteArray;
		} catch (Exception e) {
		}
		return byteArray;

	}

	public Uri getOutputMediaFileUri(int type) {
//		return Uri.fromFile(getOutputMediaFile(type));
		if (Utility.isTargetSdkVersionGreaterThanNaugat()) {
			return FileProvider.getUriForFile(
					this,
					getApplicationContext()
							.getPackageName() + ".provider", getOutputMediaFile(type));
		} else {
			return Uri.fromFile(getOutputMediaFile(type));
		}
	}
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}


		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
			DatePath= mediaFile;

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
	public  void submitSurveyFeedBackOfApp(int sessionId) {
		StringBuilder request = new StringBuilder();
		//	        soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem="http://tempuri.org/" xmlns:ctl="http://schemas.datacontract.org/2004/07/CTL.ERP.EIF.CommonService.Model.Domain"
		request.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ctl=\"http://schemas.datacontract.org/2004/07/CTL.ERP.EIF.CommonService.Model.Domain/ctl\">");

		request.append("<soapenv:Header/>");
		request.append("<soapenv:Body>");
		request.append(PostConnectionInfo(1));
		request.append("</soapenv:Body>");
		request.append("</soapenv:Envelope>");
		System.out.print(request.toString());
		Log.v(request.toString(), "output");
	}
	public static String PostGrievance(int sessionId) {
		StringBuilder griev = new StringBuilder("<PostGrievance>");
		//	        griev.append("<griev>");
		/** appending Result id **/
		griev.append("<Result>");
		griev.append(sessionId);
		griev.append("</Result>");
		griev.append("</PostGrievance>");

		// griev.append("PostGrievance>");

		Log.v(griev.toString(), "this is out put");
		System.out.println(griev.toString());
		return griev.toString();

	}

	public  String PostConnectionInfo(int sessionId ) {

		try {
			int latitiude=17;
			String DateNo ="28/05/2015";
			String FileNo="2015-3-6485";
			String gbnum ="58";
			int metersize =15;
			checkEnteredInput();
			ConnInfo.append("<ConnInfo>");
			ConnInfo.append("<BoringLatitudeDegrees>");
			ConnInfo.append(latBoringDegree);
			ConnInfo.append("</BoringLatitudeDegrees>");
			ConnInfo.append("<BoringLatitudeSeconds>");
			ConnInfo.append(gbnum);
			ConnInfo.append("</BoringLatitudeSeconds>");
			ConnInfo.append("<BoringLatitudeMilliSeconds>");
			ConnInfo.append(gbnum);
			ConnInfo.append("</BoringLatitudeMilliSeconds>");
			ConnInfo.append("<BoringLatitudeMinutes>");
			ConnInfo.append(latgBoringMinute);
			ConnInfo.append("</BoringLatitudeMinutes>");
	
			ConnInfo.append("<BoringLongitudeDegrees>");
			ConnInfo.append(longBoringdegree);
			ConnInfo.append("</BoringLongitudeDegrees>");
			ConnInfo.append("<BoringLongitudeMilliSeconds>");
			ConnInfo.append(gbnum);
			ConnInfo.append("</BoringLongitudeMilliSeconds>");
			ConnInfo.append("<BoringLongitudeMinutes>");
			ConnInfo.append(longBoringminute);
			ConnInfo.append("</BoringLongitudeMinutes>");

			ConnInfo.append("<BoringLongitudeSeconds>");
			ConnInfo.append(gbnum);
			ConnInfo.append("</BoringLongitudeSeconds>");

			//////////////////////////////////

			ConnInfo.append("<BoringPointPhoto>");
			ConnInfo.append(photo_Encoded);
			ConnInfo.append("</BoringPointPhoto>");
			ConnInfo.append("<BuildingPhoto>");
			ConnInfo.append(photo_Encoded);
			ConnInfo.append("</BuildingPhoto>");

			ConnInfo.append("<CustomerRemarks>");
			ConnInfo.append(CustmerFB);
			ConnInfo.append("</CustomerRemarks>");
			ConnInfo.append("<CustomerStarRating>");
			ConnInfo.append(Custmrating);
			ConnInfo.append("</CustomerStarRating>");
			ConnInfo.append("<DateOfConnection>");
			ConnInfo.append(DateNo);
			ConnInfo.append("</DateOfConnection>");

			ConnInfo.append("<DisConnectionPhoto>");
			ConnInfo.append("");
			ConnInfo.append("</DisConnectionPhoto>");

			ConnInfo.append("<DisconnLedgerNo>");
			ConnInfo.append("");
			ConnInfo.append("</DisconnLedgerNo>");

			ConnInfo.append("<DisconnMeterSize>");
			ConnInfo.append("");
			ConnInfo.append("</DisconnMeterSize>");

			ConnInfo.append("<DisconnectionDate>");
			ConnInfo.append("");
			ConnInfo.append("</DisconnectionDate>");

			ConnInfo.append("<FileNo>");
			//			ConnInfo.append(getIntent().getStringExtra(FileNo));
			ConnInfo.append(FileNo);
			ConnInfo.append("</FileNo>");

			ConnInfo.append("<GBNo>");
			ConnInfo.append(gbnum);
			ConnInfo.append("</GBNo>");

			ConnInfo.append("<LedgerNo>");
			ConnInfo.append(LedgerNo);
			ConnInfo.append("</LedgerNo>");

			ConnInfo.append("<LengthOfConnection>");
			ConnInfo.append(LengthOfConnec);
			ConnInfo.append("</LengthOfConnection>");

			ConnInfo.append("<MeterInitialReading>");
			ConnInfo.append(InitialMtrRead);
			ConnInfo.append("</MeterInitialReading>");

			ConnInfo.append("<MeterLatitudeDegrees>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLatitudeDegrees>");

			ConnInfo.append("<MeterLatitudeMilliSeconds>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLatitudeMilliSeconds>");

			ConnInfo.append("<MeterLatitudeMinutes>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLatitudeMinutes>");

			ConnInfo.append("<MeterLatitudeSeconds>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLatitudeSeconds>");

			ConnInfo.append("<MeterLongitudeDegrees>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLongitudeDegrees>");

			ConnInfo.append("<MeterLongitudeMilliSeconds>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLongitudeMilliSeconds>");

			ConnInfo.append("<MeterLongitudeMinutes>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLongitudeMinutes>");

			ConnInfo.append("<MeterLongitudeSeconds>");
			ConnInfo.append(latitiude);
			ConnInfo.append("</MeterLongitudeSeconds>");

			ConnInfo.append("<MeterMake>");
			ConnInfo.append(MeterMakes);
			ConnInfo.append("</MeterMake>");

			ConnInfo.append("<MeterNo>");
			ConnInfo.append(MeterNum);
			ConnInfo.append("</MeterNo>");

			ConnInfo.append("<MeterPhoto>");
			ConnInfo.append(photo_Encoded);
			ConnInfo.append("</MeterPhoto>");

			ConnInfo.append("<MeterSize>");
			ConnInfo.append(metersize);
			ConnInfo.append("</MeterSize>");

			ConnInfo.append("<MeterWarrantyValidUpto>");
			ConnInfo.append(DateNo);
			ConnInfo.append("</MeterWarrantyValidUpto>");
			ConnInfo.append("</ConnInfo>");

			ConnXml=ConnInfo.toString();

			Log.v(ConnInfo.toString(), "XML");

			AsyncCallWS task = new AsyncCallWS();
			//Call execute 
			task.execute();






		} catch (Exception e) {
			// TODO: handle exception
		}
		return ConnInfo.toString();
		//	        StringBuilder ConnInfo = new StringBuilder("<ConnInfo>");
		/** appending Result id **/

	}


	@Override
	public void onClick(View v) {
		if(v == gb_MeterWarrentyET) {
			datePicker.show();
		}
	}
	private class AsyncCallWS extends AsyncTask<Void, Void, Void> {
		private String mobileNumber;

		//	AE7654
		@Override
		protected Void doInBackground(Void... params) {
			try {
				GbPostPendingDataToServer.invokePendingFiles(ConnXml, "PostConnectionInfo");
			} catch (Exception e) {
				// TODO: handle exception
			}


			//GbPostPendingDataToServer.invokePendingFiles();
			//		GbPostPendingDataToServer.submitSurveyFeedBackOfApp(1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Toast.makeText(getApplicationContext(), "Data SuccessFully Send", 500).show();	
			try {
				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
			} catch (Exception e) {
				// TODO: handle exception
			}


		}

	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
