package com.hmwssb;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vajra.utils.AppConstants;

public class CanGenerationDetails extends SuperActivity implements OnClickListener, AppConstants {
	Button mCustomerFeedbackDetails,mCustomerBuildingDetails;
	private String mFileno;
	private String mCustomerName;
	private String mCustomerAddress;
	public ImageView signout, home;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.cangeneration_activity);

		Bundle bundle = getIntent().getExtras();
		mFileno = bundle.getString(KEY_FILENO); 
		mCustomerName = bundle.getString(KEY_NAME);
		mCustomerAddress = bundle.getString(KEY_Address);

		mCustomerFeedbackDetails=(Button)findViewById(R.id.customer_feedback_details);
		mCustomerBuildingDetails=(Button)findViewById(R.id.customer_building_details);
		home = (ImageView) findViewById(R.id.home);
		signout = (ImageView) findViewById(R.id.signout);

		mCustomerFeedbackDetails.setOnClickListener(this);
		mCustomerBuildingDetails.setOnClickListener(this);

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				finish();
				startActivity(i);
			}
		});

		signout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//			    logoutFromApp();
				finish();
				System.exit(0);
			}
		});

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.disconnection_header);
		LayoutInflater inflater = getLayoutInflater();

		// inflate the header description layout and add it to the linear layout:
		View descriptionLayout = inflater.inflate(R.layout.newconnection_header, null, false);
		layout.addView(descriptionLayout);

		// you can set text here too:
		TextView textView = (TextView) descriptionLayout.findViewById(R.id.headerTv);
		textView.setText("New Can Generation");

	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.customer_feedback_details:
		{
			Intent i = new Intent(getApplicationContext(), CustomerFeedback.class);
			Bundle bundle = new Bundle();
			bundle.putString("FileNo",mFileno); 
			bundle.putString("Name",mCustomerName); 
			bundle.putString("Address",mCustomerAddress); 
			i.putExtras(bundle);
			startActivity(i);

		}
		break;

		case R.id.customer_building_details:
		{
			Intent i = new Intent(getApplicationContext(), BuildingDetails.class);
			Bundle bundle = new Bundle();
			bundle.putString("FileNo",mFileno); 
			i.putExtras(bundle);
			startActivity(i);
		}
		break;

		default:
			break;
		}
	}
}
