package com.vajra.hmwssb.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hmwssb.R;
import com.vajra.hmwssb.tabhost.SlidingTabLayout;



/**
 * Created by nareshpoola on 20/04/15.
 */
public class HomeFragment extends Fragment {

	private SlidingTabLayout mSlidingTabLayout;
	private ViewPager mViewPager;
	private View rootView;
	EnhancementFragment frag1;

	ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

		private int currentPosition = 0;
		
		

		@Override
		public void onPageSelected(int newPosition) {
			//initViews();
			currentPosition = newPosition;
			/*if(currentPosition==0)
			{
			frag1= (EnhancementFragment)mViewPager.getAdapter().instantiateItem(mViewPager, mViewPager.getCurrentItem());
			frag1.getDBdata();
			}
			else
			{
				NewConnFragment frag2 = (NewConnFragment)mViewPager.getAdapter().instantiateItem(mViewPager, mViewPager.getCurrentItem());
				frag2.getDBdata();
			}*/
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

		}

		@Override
		public void onPageScrollStateChanged(int state) {

		}
	};
	private Handler handler;
	private ViewPagerAdapter adapter;
	private boolean isViewAttached = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.tab_fragment, container, false);
			init();
		} else {
			ViewGroup parent = (ViewGroup) rootView.getParent();
			if (parent != null) {
				parent.removeView(rootView);
			}
		}
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		return rootView;
	}

	public void init() {
		handler = new Handler();
		initViews();
	}


	@Override
	public void onResume() {
		super.onResume();

	}

	public void searchForTheText(String searchText) {
		// TODO Auto-generated method stub
		if(mViewPager.getCurrentItem() == 0)
		{
			
			NewConnFragment frag2 = (NewConnFragment)mViewPager.getAdapter().instantiateItem(mViewPager, mViewPager.getCurrentItem());
			frag2.updateList(searchText);
			/**/

		}else if(mViewPager.getCurrentItem() == 1)
		{
			EnhancementFragment frag1= (EnhancementFragment)mViewPager.getAdapter().instantiateItem(mViewPager, mViewPager.getCurrentItem());
			frag1.updateList(searchText);

		}
	}

	public void getConnectionDetaials() {


		try {
			
			
			NewConnFragment frag2 = (NewConnFragment)mViewPager.getAdapter().instantiateItem(mViewPager, 0);
			frag2.getDBdata();
			
			EnhancementFragment frag1= (EnhancementFragment)mViewPager.getAdapter().instantiateItem(mViewPager, 1);
			frag1.getDBdata();



			


//			PendingFailedFragment frag3 = (PendingFailedFragment)mViewPager.getAdapter().instantiateItem(mViewPager, 2);
			// frag3.getDBdata();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public String  getpostion()
	{
		String type=null;
		try
		{
		
		if(mViewPager.getCurrentItem() == 0)
		{
				
			type="N";
		}
		
		if(mViewPager.getCurrentItem()==1 )
		{
		
			
			type="E";
		}
		}
		catch(Exception e)
		{
			System.out.println("the exception getting position:"+e);
		}
		return type;
	}
	

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	public void initViews() {
		mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
		mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
		mSlidingTabLayout.setDistributeEvenly(true);
		mSlidingTabLayout.setCustomTabView(R.layout.tab_textview, R.id.tab_textview);
		mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(android.R.color.white));
		adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
		mViewPager.setAdapter(adapter);
		mSlidingTabLayout.setOnPageChangeListener(pageChangeListener);
		mSlidingTabLayout.setViewPager(mViewPager);
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				pageChangeListener.onPageSelected(1);
			}
		}, 2000);
	}


	private class ViewPagerAdapter extends FragmentStatePagerAdapter {

		String tabTitles[] = { "NEW CONNECTION","ENHANCEMENT","FAILED FILES"};
		private Fragment[] fragments;


		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
			fragments = new Fragment[]{
					
					new NewConnFragment(),
					new EnhancementFragment(),
					new Pendingfiles()
			};
		}
		@Override
		public Fragment getItem(int position) {
			return fragments[position];
		}


		@Override
		public CharSequence getPageTitle(int position) {
			return tabTitles[position];
		}

		@Override
		public int getCount() {
			return tabTitles.length;
		}
	}


	



}