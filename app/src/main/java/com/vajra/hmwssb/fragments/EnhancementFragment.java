package com.vajra.hmwssb.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hmwssb.NewConnection;
import com.hmwssb.R;
import com.vajra.hmwsdatabase.Transactions;
import com.vajra.pendinglist.ArrayAdapterItem;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;
import com.vajra.utils.SharedPreferenceUtils;


public class EnhancementFragment extends BaseFragment implements AppConstants{

	private ArrayAdapterItem adapter;
	private ListView listViewItems;
	private ViewGroup header;
	private Transactions mTransObj;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		

		
		View rootView = inflater.inflate(R.layout.fragment_games, container, false);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		mTransObj = new Transactions();
		try {
			mTransObj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		listViewItems = (ListView) rootView.findViewById(R.id.faculty_history_lit);
		inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		header = (ViewGroup) inflater.inflate(R.layout.gb_header_layout, listViewItems,
				false);
		
		   listViewItems.addHeaderView(header);
		   getDBdata();

		listViewItems.setOnItemClickListener(itemClickListener);
//		getData();
		return rootView;
	}
	OnItemClickListener itemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View container, int position, long id) {

			//			BoringConnection bConn = (BoringConnection) adapter.getItem(position-1);
			try {

				Intent intent = new Intent(getActivity().getBaseContext(), NewConnection.class);
				BoringConnection bConn = (BoringConnection)adapter.getItem(position-1);
				intent.putExtra(AppConstants.GB_DATA, bConn);
				String FILENO=AppConstants.GB_DATA;
				System.out.println("*******FILENO**************FILENO"+FILENO+","+bConn.getFileNo());
				
				 SharedPreferenceUtils.savePreference(getActivity(), IPreferencesConstants.FILENO, bConn.getFileNo());
				startActivity(intent);
				
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	};
	
	@Override
	public void onResume() {
		super.onResume();
		getDBdata();
		//		getData();
	}
	public void updateList(String searchText) {
		
		  Log.i("new conn mesg", "" + searchText); //TODO need to check with fragment loaded

		  ArrayList<BoringConnection> newSearchedItems = new ArrayList<BoringConnection>();
		  ArrayList<BoringConnection> newItems = mTransObj.getpendingfilenewconnection(AppConstants.ENHANCED_CONNECTION);
		  for (int i = 0; i < newItems.size(); i++) {
		   if (newItems.get(i).getFileNo().toLowerCase().contains(searchText.toLowerCase())) {
		    newSearchedItems.add(newItems.get(i));
		   }
		  }
		  adapter.updateData(newSearchedItems);
	}
	public void getDBdata() {
		
		try {
			ArrayList<BoringConnection> connectionData = mTransObj.getpendingfilenewconnection(AppConstants.ENHANCED_CONNECTION);
			System.out.println("**************************connectionData"+connectionData.toString());
	
			adapter =  new ArrayAdapterItem(getActivity(),connectionData);
			listViewItems.setAdapter(adapter);
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}
}
