package com.vajra.hmwssb.fragments;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hmwssb.Failedfiles;
import com.hmwssb.R;
import com.hmwssb.SuperActivity;
import com.vajra.hmwsdatabase.Transactions;
import com.vajra.pendinglist.ArrayAdapterItem;
import com.vajra.service.model.BoringConnection;
import com.vajra.utils.AppConstants;

public class Pendingfiles extends BaseFragment implements AppConstants{

	private ArrayAdapterItem adapter;
	private ListView listViewItems;
	private ViewGroup header ;
	private Transactions mTransObj;
	private ProgressDialog mProgressDialog;
	SuperActivity superActivity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_games, container, false);
		//mTransObj = new Transactions(getActivity());
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
		mTransObj = new Transactions();
		try {
			mTransObj.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		listViewItems = (ListView) rootView.findViewById(R.id.faculty_history_lit);
		inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		header = (ViewGroup) inflater.inflate(R.layout.gb_header_layout, listViewItems,
				false);
		listViewItems.addHeaderView(header);
		getDBdata();
		//		listViewItems.addHeaderView(header, null, false);
		/*ArrayList<BoringConnection> connectionData = mTransObj.getpendingfilenewconnection(AppConstants.NEW_CONNECTION);

        myAsyncTask failedfiletask= new myAsyncTask();
				failedfiletask.execute(itempostion);
				getDBdata();


		//code to add header and footer to listview

		listViewItems.addHeaderView(header);
		adapter =  new ArrayAdapterItem(getActivity(),connectionData);
		listViewItems.setAdapter(adapter);*/
		
		listViewItems.setOnItemClickListener(itemClickListener);
		//		getData();
		return rootView;
	}



	// Item Click Listener for the listview
	OnItemClickListener itemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View container, int position, long id) {
			try {
			
				int itempostion=position-1;
				myAsyncTask failedfiletask= new myAsyncTask();
				failedfiletask.execute(itempostion);
				getDBdata();
				
				
			
				
				
			} catch (Exception e) {
				System.out.println("the exception  pending files"+e);
			}

		}
	};
	@Override
	public void onResume() {
		super.onResume();
		getDBdata();
		//		getData();
	}
	public void updateList(String searchText) {
		// TODO Auto-generated method stub
		Log.i("new conn mesg", "" + searchText);	//TODO need to check with fragment loaded

		ArrayList<BoringConnection> newSearchedItems = new ArrayList<BoringConnection>();
		  ArrayList<BoringConnection> newItems = mTransObj.getpendingfilenewconnection(AppConstants.NEW_CONNECTION);
		for (int i = 0; i < newItems.size(); i++) {
			if (newItems.get(i).getFileNo().toLowerCase().contains(searchText.toLowerCase())) {
				newSearchedItems.add(newItems.get(i));
			}
		}
		adapter.updateData(newSearchedItems);
		

	}
	public void callintent(int postoion)
	{
		Intent intent = new Intent(getActivity().getBaseContext(), Failedfiles.class);
		BoringConnection bConn = (BoringConnection)adapter.getItem(postoion);
		intent.putExtra(AppConstants.GB_DATA, bConn);
		String FILENO=AppConstants.GB_DATA;
	    startActivity(intent);	
	}

	public void getDBdata() {
	
		ArrayList<BoringConnection> connectionData = mTransObj.getPendingdetails();
//		listViewItems.addHeaderView(header);
		adapter =  new ArrayAdapterItem(getActivity(),connectionData);
		listViewItems.setAdapter(adapter);
	}
	private class myAsyncTask extends AsyncTask<Integer, Void, String>
	{

	    ProgressDialog dialog;
	    @Override
	    protected void onPreExecute() {
	        dialog = ProgressDialog.show(getActivity(), "Failed Files", "Processing Failed Files Please Wait !",true,false);
	    }
	    @Override
		protected String doInBackground(Integer... params) {
	    	
	    int fileid=	params[0];
	   callintent(fileid);
			return null;
		}
	   
	    @Override
	    protected void onPostExecute(String result) {
	        if (dialog.isShowing())
	            dialog.dismiss();
	    }
		
	}

}
