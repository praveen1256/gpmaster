package com.vajra.hmwssb.fragments;


import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;

import com.hmwssb.R;
import com.vajra.service.HmwssbApplication;

/**
 * Created by sriram on 14/4/15.
 */
public abstract class BaseFragment extends Fragment {

    public BaseFragment() {
        super();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public HmwssbApplication getApp() {
        return (HmwssbApplication) getActivity().getApplication();
    }
    
    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.main, menu);
	    super.onCreateOptionsMenu(menu,inflater);
	}

    // public abstract void updateFilter(int scrollState);

}
