package com.vajra.hmwsdatabase;



import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hmwssb.OTPSet;
import com.vajra.service.model.BoringConnection;

public class Transactions {
	DatabaseHelper  dbhelper;
	SQLiteDatabase database;
	Context cnxt;
	OTPSet otpset;

	public Transactions open() throws Exception {
		dbhelper = new DatabaseHelper();
		database = dbhelper.getWritableDatabase();
		return this;
	}

//	public void open() throws SQLException {
//		database = dbhelper.getWritableDatabase();
//	}

	public void close() {
		dbhelper.close();
	}





	/************************************************get service status of meter details webservices*****************************
	 * 
	 */
	public String get_Count_Files(String Applicationtype)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "Select count(*) from GetGBPendingFilesInfo where ApplicationType='"+Applicationtype+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_Count_Files() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	
	public String getnewsimno(String simno)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = simno;
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i( "@@ ERROR at simno() ", simno.toString());
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at cdfvxzcv() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	
public ArrayList<String> getfailedfiles()
{
	ArrayList<String> array_list = new ArrayList<String>();
	Cursor cursor = null;
	database = dbhelper.getReadableDatabase();
	try {
		open();
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	cursor = database.rawQuery("SELECT * from getGBPendingFilesInfo where GBStatus='Pending'  OR  GBStatus='Remarks Captured'", null);
	array_list.add("Select One");
	try {
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			// array_list.add(cursor.getString(0)); SELECT distinct
			// va_vehicleid FROM ersvehicle_assign_trans where
			// va_eventid=$eventid
		
			array_list.add(cursor.getString(3));

			cursor.moveToNext();
		}
	} catch (Exception e) {
		if (cursor != null) {
			cursor.close();
		}
	}
	finally {

		if (cursor != null) {
			cursor.close();
		}
		close();
	}

	return array_list;

}

public String delete_newfiles() {
	String result = null;
	Cursor cursor = null;
	database = dbhelper.getReadableDatabase();

	try {
		open();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String selectQuery = " Delete from getGBPendingFilesInfo where  GBStatus='New' ";
	
	cursor = database.rawQuery(selectQuery, null);
	try {
		if (cursor.moveToFirst()) {
			do {// cursor.getString(0)
				if (cursor.getString(0) != null) {

					result = (cursor.getString(0));
				}
			} while (cursor.moveToNext());
		}

	} catch (Exception ex) {
		Log.e("ERROR", "@@ ERROR at deletefile() ", ex);
		if (cursor != null) {
			cursor.close();
		}
		ex.printStackTrace();
	} finally {

		if (cursor != null) {
			cursor.close();
		}
		close();
	}

	return result;

}
	
	

	
	
	public ArrayList<BoringConnection> getPendingdetails() {

		ArrayList<BoringConnection> connectionData = new ArrayList<BoringConnection>();
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String retrieveQuiery = "SELECT distinct FileNo from  PostDisconnectionDetailsInfo  where DisconnectionDetailsInfoWebServiceStatus='failed'  Union select distinct FileNo from  PostBoringDetailsInfo  where BoringDetailsInfoWebServiceStatus='failed'   Union select distinct FileNo from  PostMeterDetailsInfo  where MeterDetailsInfoWebServiceStatus='failed' Union select distinct FileNo from  PostGBRemarksInfo  where GBRemarksInfoWebServiceStatus='failed' Union select distinct FileNo from  PostCustomerFeedBackDetailsInfo  where CustomerFeedBackWebServiceStatus='failed' Union select distinct FileNo from  CanGenerateCAN  where CanGenerateCANWebServiceStatus='failed'  Union select distinct FileNo from  PostBoringDetailsInfo  where BoringDetailsInfoWebServiceStatus='failed'  Union select distinct FileNo from  CanGenerateGBBill  where CanGenerateGBBillWebServiceStatus='failed'";
		cursor = database.rawQuery(retrieveQuiery, null);

		try {

			if (cursor.moveToFirst())  {
				do {
					String fileNumber = cursor.getString(0);
					BoringConnection connData = getBoringConnection(fileNumber);
					if (connData != null) {
						connectionData.add(connData);
					}
				}while (cursor.moveToNext());
			}
		} catch (Exception e) {
			if (cursor != null) {
				cursor.close();
			}
		}

		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}

		return connectionData;
	}
	//get gb date of particular 

	public String GBAllotmentDate(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select GBAllotmentDate from GetGBPendingFilesInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at cangbbillxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}


	public BoringConnection getBoringConnection(String fileNumber) {
		Cursor cursor = null;
		BoringConnection boringConnection = new BoringConnection();
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String selectQuery = "SELECT * FROM GetGBPendingFilesInfo WHERE FileNo = '"+fileNumber+"'"; 

		System.out.printf("selectQuery", selectQuery);
		cursor = database.rawQuery(selectQuery, null);
		System.out.printf("detailslist", cursor.toString());
		try {
			if (cursor.moveToFirst()) {

				boringConnection.setAddress(cursor.getString(cursor.getColumnIndex("Address")));
				boringConnection.setFileNo(cursor.getString(cursor.getColumnIndex("FileNo")));
				boringConnection.setMobileNumber(cursor.getString(cursor.getColumnIndex("MobileNo")));
				boringConnection.setApplicationType(cursor.getString(cursor.getColumnIndex("ApplicationType")));
				boringConnection.setName(cursor.getString(cursor.getColumnIndex("Name")));
				boringConnection.setSanctionedPipe(cursor.getString(cursor.getColumnIndex("SanctionedPipeSize")));
				boringConnection.setMeterMandatory(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsMeterMandatory"))));
				boringConnection.setBoringDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsBoringDetailsReceived"))));
				boringConnection.setBuildingDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsBuildingDetailsReceived"))));
				boringConnection.setCustomerFeedBackDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsCustomerFeedBackDetailsReceived"))));
				boringConnection.setDisConnectionDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsDisconnectionDetailsReceived"))));
				boringConnection.setGbAcceptedForCanGeneration(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsGBAcceptedForCANGeneration"))));
				boringConnection.setGbAcceptedForGbBillGeneration(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsGBAcceptedForGBBillGeneration"))));
				boringConnection.setMeterDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsMeterDetailsReceived"))));
				boringConnection.setConnectionType(cursor.getString(cursor.getColumnIndex("ApplicationType")));

				return boringConnection;
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at  newconnectionlist;() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return  null;
	}

	//***************************************getPendingFileNewConnection******************************************
	//sunil
	//***************************************getPendingFileNewConnection******************************************
	//sunil
	public ArrayList<BoringConnection> getpendingfilenewconnection(String ApplicationType)
	{
		Cursor cursor = null;
		Log.i("ApplicationType",ApplicationType);

		ArrayList<BoringConnection> newconnectionlist = new ArrayList<BoringConnection>();
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		//String selectQuery = "SELECT * FROM GetGBPendingFilesInfo WHERE ApplicationType = '"+ApplicationType+"' and GBStatus='Pending' UNION ALL SELECT * FROM GetGBPendingFilesInfo WHERE ApplicationType = '"+ApplicationType+"' and GBStatus='New' Order by GBAllotmentDate DESC" ;

		String selectQuery ="select * , 1 as ord from getGBPendingFilesInfo where GBStatus='Pending'  and ApplicationType = '"+ApplicationType+"' UNION select * , 2 as ord from getGBPendingFilesInfo where GBStatus='New' and ApplicationType = '"+ApplicationType+"' UNION select * , 3 as ord from getGBPendingFilesInfo where GBStatus='Remarks Captured' and ApplicationType = '"+ApplicationType+"' Order by ord, GBAllotmentDate DESC" ;

		System.out.printf("selectQuery", selectQuery);
		cursor = database.rawQuery(selectQuery, null);
		System.out.printf("detailslist", cursor.toString());
		try {
			if (cursor.moveToFirst()) {
				do {

					Log.i("cursor.getString(1)", cursor.getString(1));
					BoringConnection boringConnection = new BoringConnection();

					boringConnection.setAddress(cursor.getString(cursor.getColumnIndex("Address")));
					boringConnection.setFileNo(cursor.getString(cursor.getColumnIndex("FileNo")));
					boringConnection.setMobileNumber(cursor.getString(cursor.getColumnIndex("MobileNo")));
					boringConnection.setApplicationType(cursor.getString(cursor.getColumnIndex("ApplicationType")));
					boringConnection.setName(cursor.getString(cursor.getColumnIndex("Name")));
					boringConnection.setSanctionedPipe(cursor.getString(cursor.getColumnIndex("SanctionedPipeSize")));
					boringConnection.setMeterMandatory(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsMeterMandatory"))));
					boringConnection.setBoringDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsBoringDetailsReceived"))));
					boringConnection.setBuildingDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsBuildingDetailsReceived"))));
					boringConnection.setCustomerFeedBackDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsCustomerFeedBackDetailsReceived"))));
					boringConnection.setDisConnectionDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsDisconnectionDetailsReceived"))));
					boringConnection.setGbAcceptedForCanGeneration(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsGBAcceptedForCANGeneration"))));
					boringConnection.setGbAcceptedForGbBillGeneration(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsGBAcceptedForGBBillGeneration"))));
					boringConnection.setMeterDetailsReceived(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("IsMeterDetailsReceived"))));
					boringConnection.setConnectionType(cursor.getString(cursor.getColumnIndex("ApplicationType")));
					System.out.println("********GET STATUS OF PENDING FILES*****"+cursor.getString(cursor.getColumnIndex("GBStatus")));
					boringConnection.setStatus(cursor.getString(cursor.getColumnIndex("GBStatus")));

					newconnectionlist.add(boringConnection);

					// }
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at  newconnectionlist;() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return  newconnectionlist;
	}

	/**************************************************************update pending file-********************
	 * 
	 */

	/******************************update post bulirlding details bill ***************************/
	public int update_pendingfileconnection(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items GetGBPendingFilesInfo status " + values.toString()+","+fileno);
			result = database.update("GetGBPendingFilesInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ PostBuildingDetailsInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at update GetGBPendingFilesInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	//****SIMSERIAL Delete Recodrd*******************************************


	public String deleterecode(String gbcode) {
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = " Delete from swc_licence where swc_gb_code='"+gbcode+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at erseventinfo_trans() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}

		return result;

	}
	//

	public ArrayList<String> getgbcode()
	{
		ArrayList<String> array_list = new ArrayList<String>();
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		cursor = database.rawQuery("SELECT * from GetGBInfo", null);
		array_list.add("Select One");
	
		try {
			cursor.moveToFirst();
			while (cursor.isAfterLast() == false) {
				// array_list.add(cursor.getString(0)); SELECT distinct
				// va_vehicleid FROM ersvehicle_assign_trans where
				// va_eventid=$eventid
				array_list.add(cursor.getString(1));
				array_list.add(cursor.getString(2));
				array_list.add(cursor.getString(3));

				cursor.moveToNext();
			}
		} catch (Exception e) {
			if (cursor != null) {
				cursor.close();
			}
		}
		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}

		return array_list;

	}



	public String getmeteravailable(String FileNo)
	{
		String result=null;
		Cursor cursor = null;

		try {
			open();
			String selectQuery= "SELECT IsMeterMandatory from GetGBPendingFilesInfo  WHERE FileNo LIKE '"+FileNo+"'";
			cursor = database.rawQuery(selectQuery, null);
			Log.i(" Get child userid",selectQuery);
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null
							) {
						result=cursor.getString(0);


					}
				} while (cursor.moveToNext());
			}


		}
		catch (Exception ex) {
			Log.e("error", "@@ ERROR atget_userdetails()()", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;
	}

	//**************************************************get file numbers

	public ArrayList<String> getfilenumber() {

		ArrayList<String> array_list = new ArrayList<String>();
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		cursor = database.rawQuery(
				"SELECT FileNo from GetGBPendingFilesInfo", null);
		array_list.add("Select One");
		try {
			cursor.moveToFirst();
			while (cursor.isAfterLast() == false) {
				// array_list.add(cursor.getString(0)); SELECT distinct
				// va_vehicleid FROM ersvehicle_assign_trans where
				// va_eventid=$eventid
				array_list.add(cursor.getString(0));
				cursor.moveToNext();
			}
		} catch (Exception e) {
			if (cursor != null) {
				cursor.close();
			}
		}
		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}

		return array_list;
	}

	//********************************get file details********************
	public ArrayList<String> getfiledetails( String fileno) {

		ArrayList<String> array_list = new ArrayList<String>();
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		cursor = database.rawQuery(
				"SELECT Address,GBNo,MobileNo,Name,SanctionedPipeSize,IsMeterMandatory from GetGBPendingFilesInfo where FileNo= '"+fileno+"' ", null);
		array_list.add("Select One");
		try {
			cursor.moveToFirst();
			while (cursor.isAfterLast() == false) {
				// array_list.add(cursor.getString(0)); SELECT distinct
				// va_vehicleid FROM ersvehicle_assign_trans where
				// va_eventid=$eventid
				System.out.println("******************************address"+cursor.getString(0));
				array_list.add(cursor.getString(0));
				array_list.add(cursor.getString(1));
				array_list.add(cursor.getString(2));
				array_list.add(cursor.getString(3));
				array_list.add(cursor.getString(4));
				array_list.add(cursor.getString(5));
				cursor.moveToNext();
			}
		} catch (Exception e) {
			if (cursor != null) {
				cursor.close();
			}
		}
		finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}

		return array_list;
	}
	/*public ArrayList<String> getfilenumber()
	{
		Cursor cursor = null;
		   String result=null;
		   try {
		   open();
		   ArrayList<String> getfilenum= new ArrayList<String>();
		   String selectQuery= "SELECT FileNo from GetGBPendingFilesInfo";
		   cursor = database.rawQuery(selectQuery, null);
		   if (cursor.moveToFirst()) {
			   do {// cursor.getString(0)
				   if (cursor.getString(0) != null) {
					   getfilenum.add(cursor.getString(0));

						}
						} while (cursor.moveToNext());
						}


						}
						catch (Exception ex) {
								Log.e("error", "@@ ERROR atget_userdetails()()", ex);
								ex.printStackTrace();
							}
						finally {
							 if (cursor != null) {
								cursor.close();
							}
							 close();
						}
						return getfilenum;
			}*/

	//******************************PostBoringDetailsInfo*******************************
	public long insert_GetGBInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_GetGBInfo", values.toString());


			result = database.insert("GetGBInfo", null, values);

		} catch (Exception e) {
			close();
		}

		close();
		return result;
	}

	//******************************PostBoringDetailsInfo*******************************
	public long insert_swc_licence(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("swc_licence", values.toString());
			result = database.insert("swc_licence", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}


	//******************************GetGBPendingFilesInfo*******************************
	public long insert_GetGBPendingFilesInfo(ContentValues values) {

		long result = 0;
		try {
			try {
				open();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.i("insert_GetGBPendingFilesInfo", values.toString());
			result = database.insert("GetGBPendingFilesInfo", null, values);
		} catch (SQLiteConstraintException  e) {
			close();
		}
		close();
		return result;
	}

	/********************************get pending files*****************************
	 * 
	 */
	/*public ArrayList<String>getpendingfilenewconnection( String ApplicationType)
	{
		Cursor cursor = null;

		  ArrayList<String> newconnectionlist = new ArrayList<String>();


		  database = dbhelper.getReadableDatabase();
			open();


		  String selectQuery = "select FileNo,IsBoringDetailsReceived,IsBuildingDetailsReceived,IsCustomerFeedBackDetailsReceived,IsCustomerFeedBackDetailsReceived,IsGBAcceptedForCANGeneration,IsGBAcceptedForGBBillGeneration" +
		  		"IsGBAcceptedForGBBillGeneration,IsMeterDetailsReceived,IsMeterMandatory,MobileNo from GetGBPendingFilesInfo where ApplicationType='"+ApplicationType+"'" ;

		  System.out.printf("selectQuery", selectQuery);

		  cursor = database.rawQuery(selectQuery, null);

		  System.out.printf("detailslist", cursor.toString());

		  try {
		   if (cursor.moveToFirst()) {
		    do {

		     Log.i("cursor.getString(0)", cursor.getString(0));

		     newconnectionlist.add((cursor.getString(0)));
		     newconnectionlist.add(cursor.getString(1));
		     newconnectionlist.add(cursor.getString(2));

		     newconnectionlist.add(cursor.getString(3));
		     newconnectionlist.add(cursor.getString(4));
		     newconnectionlist.add(cursor.getString(5));
		     newconnectionlist.add(cursor.getString(6));

		     newconnectionlist.add(cursor.getString(7));
		     newconnectionlist.add(cursor.getString(8));
		     newconnectionlist.add(cursor.getString(9));




		     // }
		    } while (cursor.moveToNext());
		   }

		  } catch (Exception ex) {
		   Log.e("ERROR", "@@ ERROR at  newconnectionlist;() ", ex);
		   ex.printStackTrace();
		  }
		  return  newconnectionlist;
		 }	*/

	//******************************PostBoringDetailsInfo*******************************
	public long insert_PostBoringDetailsInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostBoringDetailsInfo", values.toString());
			result = database.insert("PostBoringDetailsInfo", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}


	/******************************update post Boaring details details bill ***************************/
	public int update_PostBoringDetails(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items PostBoringDetailsInfo status " + values.toString()+","+fileno);
			result = database.update("PostBoringDetailsInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ PostBuildingDetailsInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at PostBoringDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	//******************************PostBuildingDetailsInfo*******************************
	public long insert_PostBuildingDetailsInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostBuildingDetailsInfo", values.toString());
			result = database.insert("PostBuildingDetailsInfo", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}

	/******************************update post bulirlding details bill ***************************/
	public int update_PostBuildingDetails(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items PostBuildingDetailsInfo status " + values.toString()+","+fileno);
			result = database.update("PostBuildingDetailsInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ PostBuildingDetailsInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at PostBuildingDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	//******************************insert can generattion *******************************
	public long insert_cangenerateInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_CanGenerateCAN", values.toString());
			result = database.insert("CanGenerateCAN", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}

	/******************************update cangeneration bill ***************************/
	public int update_CanGenerateCAN(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items details status " + values.toString()+","+fileno);
			result = database.update("CanGenerateCAN", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ CanGenerateCAN " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at CanGenerateCAN :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}


	//******************************PostDisconnectionDetailsInfo*******************************
	public long insert_PostDisconnectionDetailsInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostBoringDetailsInfo", values.toString());
			result = database.insert("PostDisconnectionDetailsInfo", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}
	/********************************** update postconnection details***********************/
	public int update_PostDisconnectionDetails(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items details status " + values.toString()+","+fileno);
			result = database.update("PostDisconnectionDetailsInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ PostDisconnectionDetailsInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at PostDisconnectionDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	//******************************PostGBRemarksInfo*******************************
	public long insert_PostCustomerFeedBackDetailsInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostCustomerFeedBackDetailsInfo", values.toString());
			result = database.insert("PostCustomerFeedBackDetailsInfo", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}
	/***************************update remarks **********************************
	 * 
	 *//*CREATE TABLE `PostCustomerFeedBackDetailsInfo` (
				`FileNo`	TEXT,
				`GBNo`	TEXT,
				`CustomerRemarks`	TEXT,
				`CustomerStarRating`	TEXT,
				`FeedBackServerStatus`	TEXT,
				`CustomerFeedBackDetailsInfoCreateByUid`	TEXT,
				`CustomerFeedBackDetailsInfoCreatedDtm`	TEXT,
				PRIMARY KEY(FileNo)
			);*/
	public int update_PostCustomerFeedBackDetails(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " PostCustomerFeedBackDetailsInfo details status " + values.toString()+","+fileno);
			result = database.update("PostCustomerFeedBackDetailsInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ update_PostCustomerFeedBackDetails " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at update_PostCustomerFeedBackDetails :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	//******************************PostGBRemarksInfo*******************************
	public long insert_PostGBRemarksInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostGBRemarksInfo", values.toString());
			result = database.insert("PostGBRemarksInfo", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}

	/***************************update post gb remarks**********************/

	public int update_PostGBRemarksInfo(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " PostGBRemarksInfo details status " + values.toString()+","+fileno);
			result = database.update("PostGBRemarksInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ PostGBRemarksInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at PostGBRemarksInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}



	//******************************PostGBRemarksInfo*******************************
	public long insert_PostMeterDetailsInfo(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_PostMeterDetailsInfo", values.toString());
			result = database.insert("PostMeterDetailsInfo", null, values);

		} catch (Exception e) {

		}
		close();
		return result;
	}

	public int update_PostMeterDetails(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items details status " + values.toString()+","+fileno);
			result = database.update("PostMeterDetailsInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ update_PostMeterDetails " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at PostMeterDetails :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}
	
//sunil updatation
	public int update_GbPendingFileInfo(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items details status " + values.toString()+","+fileno);
			result = database.update("GetGBPendingFilesInfo", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ GetGBPendingFilesInfo " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at GetGBPendingFilesInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}
	
	public String GetSelfConnStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select GetGBPendingFilesInfoCreateByUid from GetGBPendingFilesInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at cangbbillxml() ", ex);

			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/******************************insert can generattion bill*******************************/
	public long insert_CanGenerateGBBill(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("insert_CanGenerateGBBill", values.toString());
			result = database.insert("CanGenerateGBBill", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}
	public int update_CanGenerateGBBill(ContentValues values,String fileno)
	{

		int result=0;
		try
		{

			open();
			Log.v("tagActivity", " update_items CanGenerateGBBill status " + values.toString()+","+fileno);
			result = database.update("CanGenerateGBBill", values,
					"FileNo LIKE '" + fileno + "' ", null);
			Log.v("tagActivity", "@ CanGenerateGBBill " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at CanGenerateGBBill :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}
	
	public ArrayList<String> getBoringDetailsInfo(String fileno)
	{

		Cursor cursor = null;

		ArrayList<String> boaringdetailslist = new ArrayList<String>();


		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}


		String selectQuery = "select LengthOfConnection,BoringLatitudeDegrees,BoringLatitudeMinutes, BoringLatitudeSeconds,BoringLatitudeMilliSeconds,BoringLongitudeDegrees,BoringLongitudeMinutes,BoringLongitudeSeconds,BoringLongitudeMilliSeconds,BoringPointPhoto,BoringDetailsInfoCreatedDtm,DateOfConnection from PostBoringDetailsInfo where FileNo='"+fileno+"' ";
		System.out.printf("selectQuery", selectQuery);
		Log.i("selectQuery", selectQuery);
		cursor = database.rawQuery(selectQuery, null);
		Log.i("selectQuery", cursor.toString());
		System.out.printf("detailslist", cursor.toString());

		try {
			if (cursor.moveToFirst()) {
				do {

					Log.i("cursor.getString(0)", cursor.getString(0));

					boaringdetailslist.add((cursor.getString(0)));
					boaringdetailslist.add(cursor.getString(1));
					boaringdetailslist.add(cursor.getString(2));

					boaringdetailslist.add(cursor.getString(3));
					boaringdetailslist.add(cursor.getString(4));
					boaringdetailslist.add(cursor.getString(5));
					boaringdetailslist.add(cursor.getString(6));
					boaringdetailslist.add(cursor.getString(7));
					boaringdetailslist.add(cursor.getString(8));
					boaringdetailslist.add(cursor.getString(9));
					boaringdetailslist.add(cursor.getString(10));
					boaringdetailslist.add(cursor.getString(11));


					// }
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at boaringdetailslist() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return boaringdetailslist;
	}


	public ArrayList<String> getPostCustomerFeedBackDetails(String FileNo)
	{
		Cursor cursor = null;
		ArrayList<String> feedbacklist = new ArrayList<String>();
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		String selectquery="Select CustomerRemarks,CustomerStarRating,FeedBackServerStatus,CustomerFeedBackOTP from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
		System.out.printf("selectQuery",selectquery );

		cursor = database.rawQuery(selectquery, null);
		try {
			if (cursor.moveToFirst()) {
				do { 
					feedbacklist.add(cursor.getString(0));
					feedbacklist.add(cursor.getString(1));
					feedbacklist.add(cursor.getString(2));
					feedbacklist.add(cursor.getString(3));
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at boaringdetailslist() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return feedbacklist;
	}

	public ArrayList<String> getmeter_details(String fileno)
	{

		Cursor cursor = null;

		ArrayList<String> meterdetailslist = new ArrayList<String>();
		Log.i("fileno",fileno);


		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (cursor != null) {
				cursor.close();
			}
			e.printStackTrace();
		}
		String selectQuery = "select MeterSize,MeterInitialReading,MeterMake,MeterNo,MeterLatitudeDegrees,MeterLatitudeMinutes,MeterLatitudeSeconds, MeterLatitudeMilliSeconds,MeterLongitudeDegrees,MeterLongitudeMinutes,MeterLongitudeSeconds,MeterLongitudeMilliSeconds,MeterWarrantyValidUpto,MeterPhoto from PostMeterDetailsInfo where FileNo='"+fileno+"'";
		System.out.printf("selectQuery", selectQuery);

		Log.i("selectQuery", selectQuery);
		cursor = database.rawQuery(selectQuery, null);

		System.out.printf("detailslist", cursor.toString());

		try {
			if (cursor.moveToFirst()) {
				do {



					meterdetailslist.add((cursor.getString(0)));
					meterdetailslist.add(cursor.getString(1));
					meterdetailslist.add(cursor.getString(2));

					meterdetailslist.add(cursor.getString(3));
					meterdetailslist.add(cursor.getString(4));
					meterdetailslist.add(cursor.getString(5));
					meterdetailslist.add(cursor.getString(6));

					meterdetailslist.add(cursor.getString(7));
					meterdetailslist.add(cursor.getString(8));
					meterdetailslist.add(cursor.getString(9));
					meterdetailslist.add(cursor.getString(10));
					meterdetailslist.add(cursor.getString(11));
					meterdetailslist.add(cursor.getString(12));
					meterdetailslist.add(cursor.getString(13));
					meterdetailslist.add(cursor.getString(14));


					// }
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at boaringdetailslist() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return meterdetailslist;
	}

	public ArrayList<String> getgbbilllist(String fileno)
	{
		Cursor cursor = null;
		ArrayList<String>getgbbilllist=new ArrayList<String>();
		String result=null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		String selectQuery = "select * from CanGenerateGBBill where FileNo='"+fileno+"'  ";
		cursor = database.rawQuery(selectQuery, null);
		Log.i("selectQuery", cursor.toString());
		try {
			if (cursor.moveToFirst()) {
				do {


					getgbbilllist.add(cursor.getString(0));
					getgbbilllist.add(cursor.getString(1));

				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getPostGBRemarks() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return getgbbilllist;
	}


	public ArrayList<String> getPostGBRemarks(String fileno)
	{
		Cursor cursor = null;
		ArrayList<String>remarksdetails=new ArrayList<String>();
		String result=null;
		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		String selectQuery = "select GBREMARKS ,RemarksServerStatus from PostGBRemarksInfo where FileNo='"+fileno+"'  ";
		cursor = database.rawQuery(selectQuery, null);
		Log.i("selectQuery", cursor.toString());
		try {
			if (cursor.moveToFirst()) {
				do {


					remarksdetails.add(cursor.getString(0));
					remarksdetails.add(cursor.getString(1));

				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getPostGBRemarks() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return remarksdetails;
	}
	public ArrayList<String> getDisconnection(String fileno)
	{

		Cursor cursor = null;

		ArrayList<String> disconnectiondetailslist = new ArrayList<String>();


		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}


		String selectQuery = "select DisconnMeterSize,DisconnectionDate,DisconnLatitudeDegrees,DisconnLatitudeMinutes,DisconnLatitudeSeconds,DisconnLatitudeMilliSeconds," +
				"DisconnLongitudeDegrees,DisconnLongitudeMinutes,DisconnLatitudeSeconds,DisconnLongitudeMilliSeconds,DisConnectionPhoto from PostDisconnectionDetailsInfo where FileNo= '"+fileno+"' ";
		System.out.printf("selectQuery", selectQuery);
		Log.i("selectQuery", selectQuery);
		cursor = database.rawQuery(selectQuery, null);
		Log.i("selectQuery", cursor.toString());
		System.out.printf("detailslist", cursor.toString());

		try {
			if (cursor.moveToFirst()) {
				do {

					Log.i("cursor.getString(0)", cursor.getString(0));

					disconnectiondetailslist.add((cursor.getString(0)));
					disconnectiondetailslist.add(cursor.getString(1));
					disconnectiondetailslist.add(cursor.getString(2));
					disconnectiondetailslist.add(cursor.getString(3));
					disconnectiondetailslist.add(cursor.getString(4));
					disconnectiondetailslist.add(cursor.getString(5));
					disconnectiondetailslist.add(cursor.getString(6));
					disconnectiondetailslist.add(cursor.getString(7));
					disconnectiondetailslist.add(cursor.getString(8));
					disconnectiondetailslist.add(cursor.getString(9));
					disconnectiondetailslist.add(cursor.getString(10));


					// }
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at disconnectiondetailslist() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return disconnectiondetailslist;
	} 

	public ArrayList<String> getBuildingdetails(String fileno)
	{

		Cursor cursor = null;

		ArrayList<String> getBuildingdetailslist = new ArrayList<String>();


		database = dbhelper.getReadableDatabase();
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}


		String selectQuery = "select BuildingPhoto,BuildingLatitudeDegrees,BuildingLatitudeMinutes,BuildingLatitudeSeconds,BuildingLatitudeMilliSeconds," +
				"BuildingLongitudeDegrees,BuildingLongitudeMinutes,BuildingLongitudeSeconds,BuildingLongitudeMilliSeconds, BuildingDetailsInfoCreatedDtm " +
				" from PostBuildingDetailsInfo where FileNo = '"+fileno+"'";

		System.out.printf("selectQuery", selectQuery);

		cursor = database.rawQuery(selectQuery, null);

		System.out.printf("detailslist", cursor.toString());

		try {
			if (cursor.moveToFirst()) {
				do {

					Log.i("cursor.getString(0)", cursor.getString(0));

					getBuildingdetailslist.add((cursor.getString(0)));
					getBuildingdetailslist.add(cursor.getString(1));
					getBuildingdetailslist.add(cursor.getString(2));

					getBuildingdetailslist.add(cursor.getString(3));
					getBuildingdetailslist.add(cursor.getString(4));
					getBuildingdetailslist.add(cursor.getString(5));
					getBuildingdetailslist.add(cursor.getString(6));

					getBuildingdetailslist.add(cursor.getString(7));
					getBuildingdetailslist.add(cursor.getString(8));
					getBuildingdetailslist.add(cursor.getString(9));



					// }
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at boaringdetailslist() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		}
		return getBuildingdetailslist;
	}
	//****SIMSERIAL Mobile NO*******************************************
	public String mobilenumber(String gbcode) {
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = " select swc_mobile_no from swc_licence where swc_gb_code='"+gbcode+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at erseventinfo_trans() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	//********Update


	public String update_otp_details(String gbcode, String otp) {
		String result = "Updated Done";
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "UPDATE swc_licence SET swc_otp = '"+otp+"',swc_reg_status='1' WHERE swc_gb_code = '"+ gbcode+"'";

		System.out.println("******************** the Updated QRY is "+selectQuery);

		Log.v("******************** the Updated QRY is ",selectQuery);


		try {
			open();
			cursor = database.rawQuery(selectQuery, null);
		}
		catch (Exception e) {

		}close();

		return selectQuery;

	}
	// update 
	/*public int update_CanGenerateGBBill(ContentValues values,String fileno)
		  {

			  int result=0;
			  try
			  {

			  open();
			  Log.v("tagActivity", " update_items CanGenerateGBBill status " + values.toString()+","+fileno);
			  result = database.update("CanGenerateGBBill", values,
						"FileNo LIKE '" + fileno + "' ", null);
			  Log.v("tagActivity", "@ CanGenerateGBBill " + result);

			} catch (Exception ex) {
				Log.e("tagActivity", "@@ ERROR at CanGenerateGBBill :  " + ex);
				ex.printStackTrace();
			} finally {
				close();
			}
			return result;
		  }*/


	/*//******************************PostBoringDetailsInfo*******************************
				public long insert_swc_licence(ContentValues values) {

					long result = 0;
					try {
						open();
						Log.i("swc_licence", values.toString());
						result = database.insert("swc_licence", null, values);

					} catch (Exception e) {

					}
					close();
					return result;
				}*/

	//******************************PostBoringDetailsInfo*******************************
	public long insert_swc_licence_otp(ContentValues values) {

		long result = 0;
		try {
			open();
			Log.i("swc_licence", values.toString());
			result = database.insert("swc_licence_otp", null, values);

		} catch (Exception e) {
close();
		}
		close();
		return result;
	}

	//****SIMSERIAL NUMBER 1*******************************************


	public String simnumberfromSWC_Licence() {
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = " select swc_sim_serial_number from swc_licence";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at erseventinfo_trans() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}


	//****SIMSERIAL NUMBER 2*******************************************


	public String simnumberfromSWC_LicenceOTP() {
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = " select swcl_simno from swc_licence_otp";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at erseventinfo_trans() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	//*********************Status*************************

	public String status(String simnumber) {
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = " select swcl_otp_status from swc_licence_otp where swcl_simno='"+simnumber+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}



	/************************************************get service status of disconnection webservices*****************************
	 * 
	 */
	public String get_Status_Disconnection(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select DisconnectionServerStatus from PostDisconnectionDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_Status_Disconnection() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/************************************************get service status of boaring  status*****************************
	 * 
	 */
	public String get_Boaring_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BoringServerStatus from PostBoringDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_Boaring_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/************************************************get service status of meter details webservices*****************************
	 * 
	 */
	public String get_PostMeterDetails_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select MeterServerStatus from PostMeterDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_PostMeterDetails_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/************************************************get service status of can generation details webservices*****************************
	 * 
	 */
	public String get_CanGenerate_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CanGenerateCANServerStatus from CanGenerateCAN where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_CanGenerate_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/************************************************get customer details*****************************
	 * 
	 */
	public String get_PostCustomerFeedBackDetails_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select FeedBackServerStatus from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_PostCustomerFeedBackDetails_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/************************************************get service status of buliding webservices*****************************
	 * 
	 */
	public String get_PostBuildingDetails_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BuildingServerStatus from PostBuildingDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_PostBuildingDetails_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/************************************************get service status of buliding get_CanGenerateGBBill_Status*****************************
	 * 
	 */
	public String get_CanGenerateGBBill_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BoringDetailsInfoWebServiceStatus from PostBoringDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_CanGenerateGBBill_Status() ", ex);
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/************************************************get service status of buliding get_CanGenerateGBBill_Status*****************************
	 * 
	 */
	public String get_gbremarks_Status(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select RemarksServerStatus from PostGBRemarksInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at RemarksServerStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}


	/************************************************get webservice status of disconnection *****************************
	 * 
	 */
	public String getWebServiceStatusDisconnection(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select DisconnectionDetailsInfoWebServiceStatus from PostDisconnectionDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebServiceStatusDisconnection() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}	


	/************************************************************/


	public String getWebServiceBoaringStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BoringDetailsInfoWebServiceStatus from PostBoringDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebServiceBoaringStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/*************************************************************2/

public String getWebServicePostMeterDetailsStatus(String FileNo)
{
	String result = null;
	Cursor cursor = null;
	database = dbhelper.getReadableDatabase();
	String selectQuery = "select MeterDetailsInfoWebServiceStatus from PostMeterDetailsInfo where FileNo='"+FileNo+"'";
	open();
	cursor = database.rawQuery(selectQuery, null);
	try {
		if (cursor.moveToFirst()) {
			do {// cursor.getString(0)
				if (cursor.getString(0) != null) {

					result = (cursor.getString(0));
				}
			} while (cursor.moveToNext());
		}

	} catch (Exception ex) {
		Log.e("ERROR", "@@ ERROR at getWebServicePostMeterDetailsStatus() ", ex);
		ex.printStackTrace();
	} finally {

		if (cursor != null) {
			cursor.close();
		}
		close();
	}
	return result;

}



/************************************************************/


	public String getWebServiceCanGenerateStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CanGenerateCANWebServiceStatus from CanGenerateCAN where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebServiceCanGenerateStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/************************************************************/


	public String getWebServiceCustomerFeedBackStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CustomerFeedBackWebServiceStatus from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at CustomerFeedBackWebServiceStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/****************************************/



	public String getWebSerivePostCustomerFeedBackDetailsStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CustomerFeedBackWebServiceStatus from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebSerivePostCustomerFeedBackDetailsStatus() ", ex);
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/**************************************************/



	public String getWebServicePostBuildingDetailsStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BuildingDetailsInfoWebServiceStatus from PostBuildingDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebServicePostBuildingDetailsStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}



	/********************************************************/


	public String getWebServiceCanGenerateGBBillStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CanGenerateGBBillWebServiceStatus from CanGenerateGBBill where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_CanGenerateGBBill_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	public String getWebServicePostMeterDetailsStatus(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select MeterDetailsInfoWebServiceStatus from PostMeterDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getWebServicePostMeterDetailsStatus() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}




	/************************************************get service status of can generation details webservices*****************************
	 * 
	 */
	public String get_Applicationtype(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		System.out.println("******************************get_Applicationtype"+FileNo);
		String selectQuery = "select ApplicationType from GetGBPendingFilesInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
						System.out.println("***************ApplicationType*******result"+result);
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at get_CanGenerate_Status() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * getdiscoonectionxml
	 */
	public String getdisconnectionxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select DisconnectionDetailsInfoXMLString from PostDisconnectionDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getdiscoonectionxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * getboringxml
	 */
	public String getboringxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BoringDetailsInfoXMLString from PostBoringDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getboringxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * getmeterxml
	 */
	public String getmeterxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select MeterDetailsInfoXMLString from PostMeterDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at getmeterxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * getmeterxml
	 */
	public String cangeneratexml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CanGenerateCANString from CanGenerateCAN where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at cangeneratexml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}

	/***************************************************************************
	 * customerfeedbackxml
	 */
	public String customerfeedbackxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CustomerFeedBackXMLString from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at customerfeedbackxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * buildingxml
	 */
	public String buildingxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select BuildingDetailsInfoXMLString from PostBuildingDetailsInfo where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at buildingxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}
	/***************************************************************************
	 * buildingxml
	 */
	public String cangbbillxml(String FileNo)
	{
		String result = null;
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "select CanGenerateGBBillWebServiceStatus from CanGenerateGBBill where FileNo='"+FileNo+"'";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						result = (cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at cangbbillxml() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;

	}


	public ArrayList<String> getfilenumbers()
	{
		ArrayList<String> filenos=new ArrayList<String>();
		Cursor cursor = null;
		database = dbhelper.getReadableDatabase();
		String selectQuery = "Select FileNo from CanGenerateGBBill where CanGenerateGBBillWebServiceStatus=1";
		try {
			open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			close();
			e.printStackTrace();
		}
		cursor = database.rawQuery(selectQuery, null);
		try {
			if (cursor.moveToFirst()) {
				do {// cursor.getString(0)
					if (cursor.getString(0) != null) {

						filenos.add(cursor.getString(0)) ;
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception ex) {
			Log.e("ERROR", "@@ ERROR at filenos list() ", ex);
			if (cursor != null) {
				cursor.close();
			}
			ex.printStackTrace();
		} finally {

			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return filenos;

	}

	public int delete_GetGBPendingFilesInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from GetGBPendingFilesInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_GetGBPendingFilesInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_GetGBPendingFilesInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_GetGBInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from GetGBInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_GetGBInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_GetGBInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_PostBoringDetailsInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostBoringDetailsInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostBoringDetailsInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostBoringDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_PostBuildingDetailsInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostBuildingDetailsInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostBuildingDetailsInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostBuildingDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_PostMeterDetailsInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostMeterDetailsInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostMeterDetailsInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostMeterDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_PostDisconnectionDetailsInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostDisconnectionDetailsInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostDisconnectionDetailsInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostDisconnectionDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}


	public int delete_PostGBRemarksInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostGBRemarksInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostGBRemarksInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostGBRemarksInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_PostCustomerFeedBackDetailsInfo(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from PostCustomerFeedBackDetailsInfo where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_PostCustomerFeedBackDetailsInfo status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_PostCustomerFeedBackDetailsInfo :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}

	public int delete_CanGenerateGBBill(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from CanGenerateGBBill where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_CanGenerateGBBill status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_CanGenerateGBBill :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}
	public int delete_CanGenerateCAN(String FileNo) {
		Cursor cursor = null;

		int result = 0;
		try {

			open();
			String deletequery = "delete from CanGenerateCAN where FileNo='"+FileNo+"'";
			Log.i("the query vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);

			Log.v("tagActivity", "@ deleate delete_CanGenerateCAN status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_CanGenerateCAN :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}


	public int delete_unasssigendtransaction(String listoffile) {
		Cursor cursor = null;


		int result = 0;
		try {

			open();
			//			   String deletequery = "delete from GetGBPendingFilesInfo where   FileNo  NOT IN ('"+listoffile+"')";

			String deletequery = "delete from GetGBPendingFilesInfo where   FileNo In ("+listoffile+")"; 
			Log.i("the delete_unasssigendtransaction  vlaues", deletequery.toString());

			database.execSQL(deletequery);
			cursor = database.rawQuery(deletequery, null);
			result=cursor.getColumnIndex(deletequery);

			Log.v("tagActivity", "@ deleate delete_unasssigendtransaction status " + result);

		} catch (Exception ex) {
			Log.e("tagActivity", "@@ ERROR at delete_unasssigendtransaction :  " + ex);
			close();
			ex.printStackTrace();
		} finally {
			close();
		}
		return result;
	}
}
