package com.vajra.hmwsdatabase;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;

import com.hmwssb.HomeScreenTabActivity;
import com.hmwssb.R;
import com.hmwssb.SuperActivity;
import com.vajra.service.HmwssbApplication;
import com.vajra.service.OperationCompleteListener;
import com.vajra.utils.AppConstants;

public class Buttonwebservice extends SuperActivity implements AppConstants{
	  private HmwssbApplication app;
	  String otpET;
@Override
	
	public void onCreate(Bundle saveinstancestate)
	{
	Button b;
	super.onCreate(saveinstancestate);
	setContentView(R.layout.newservice);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
	b =(Button)findViewById(R.id.button1);
	Bundle extras = getIntent().getExtras(); 
	otpET = extras.getString("otpET");
	
	b.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
		    
			getApp().getBoringServices().getConnectionDetails(otpET, "GetGBPendingFilesInfo", new OperationCompleteListener() {
				@Override
				public void operationComplete(boolean status, int code, String message) {
					
					
					Intent i = new Intent(getApplicationContext(), HomeScreenTabActivity.class);
				startActivity(i);
				}
				
			});
		}
	});
	}

}
