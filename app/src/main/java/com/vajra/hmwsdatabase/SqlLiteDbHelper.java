package com.vajra.hmwsdatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class SqlLiteDbHelper extends SQLiteOpenHelper{

	// All Static variables
		// Database Version
		private static final int DATABASE_VERSION = 2;
		private static final String DATABASE_PATH = "/data/data/com.hmwssb/databases/";
		// Database Name
		private static final String DATABASE_NAME = "GBDatabase";

		private SQLiteDatabase db;


		Context ctx;

		public SqlLiteDbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			ctx = context;
			 boolean dbexist = checkdatabase();
		        if (dbexist) {
		            System.out.println("Database exists");
		        	openDataBase();
		        } else {
		            System.out.println("Database doesn't exist");
		            try {
						createdatabase();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		}

		String ph;

		// Getting single contact

		public void CopyDataBaseFromAsset() throws IOException {
			try {
				InputStream in = ctx.getAssets().open("GBDatabase");
				Log.e("sample", "Starting copying");
				String outputFileName = DATABASE_PATH + DATABASE_NAME;
				File databaseFile = new File(
						"/data/data/com.hmwssb/databases");
				// check if databases folder exists, if not create one and its
				// subfolders
				if (!databaseFile.exists()) {
					databaseFile.mkdir();
				}

				OutputStream out = new FileOutputStream(outputFileName);

				byte[] buffer = new byte[1024];
				int length;

				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				Log.e("sample", "Completed");
				out.flush();
				out.close();
				in.close();
			} catch (Exception e) {
				Log.i("*******Error at***", "CopyDataBaseFromAsset" + e);
			}

		}

		public void openDataBase() throws SQLException {
			try {
				String path = DATABASE_PATH + DATABASE_NAME;
				db = SQLiteDatabase.openDatabase(path, null,SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
				db.close();
			} catch (Exception e) {
				Log.i("*******Error at***", "openDataBase" + e);
			}

		}
		 public void createdatabase() throws IOException {
		        boolean dbexist = checkdatabase();
		        if(dbexist) {
		           System.out.println(" Database exists.");
		        } else {
					this.getReadableDatabase();


		            	CopyDataBaseFromAsset();


		        }
		 }

		 private boolean checkdatabase() {
		        //SQLiteDatabase checkdb = null;
		        boolean checkdb = false;
		        try {
		            String myPath = DATABASE_PATH + DATABASE_NAME;
		            File dbfile = new File(myPath);
		           // checkdb = SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READWRITE);
		           checkdb = dbfile.exists();
		        } catch(SQLiteException e) {
		            System.out.println("Database doesn't exist");
		        }
		        return checkdb;
		    }

		 private int exportDB(){
				int i=0;
				File sd = Environment.getExternalStorageDirectory();
				if(sd.exists())
			       {
			    	   //Toast.makeText(this, "DB exists!", Toast.LENGTH_LONG).show();

				File data = Environment.getDataDirectory();
			       FileChannel source=null;
			       FileChannel destination=null;
			       String currentDBPath = "/data/"+ "com.hmwssb" +"/databases/"+DATABASE_NAME;
			       String backupDBPath = DATABASE_NAME;
			       File currentDB = new File(data, currentDBPath);
			       File backupDB = new File(sd, backupDBPath);
			       if(backupDB.exists())
			       {
			    	   //Toast.makeText(context, "DB exists!", Toast.LENGTH_LONG).show();

			       }
			       else{
			       try {
			            source = new FileInputStream(currentDB).getChannel();
			            destination = new FileOutputStream(backupDB).getChannel();
			            destination.transferFrom(source, 0, source.size());
			            source.close();
			            destination.close();
			            //Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
			           System.out.println("DB Exported ");
			        } catch(IOException e) {
			        	e.printStackTrace();
			        }
			       }
			       i=1;
			       }
				else{
					//Toast.makeText(this, "Please Insert SD Card", Toast.LENGTH_LONG).show();
					i=2;
				}
				return i;
			}

		@Override
		public void onCreate(SQLiteDatabase db) {

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}
