package com.vajra.pendinglist;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hmwssb.R;
import com.vajra.service.model.BoringConnection;

// here's our beautiful adapter
public class ArrayAdapterItem extends BaseAdapter {

	private ArrayList<BoringConnection> connectionData = null;
	private ArrayList<String> connectionData1 = null;
	private LayoutInflater inflater = null;
	private Context context;
	private Context context1;

	public ArrayAdapterItem(Context context, ArrayList<BoringConnection> connectionData) {
		this.context = context;
		this.connectionData = connectionData;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	

	
	@Override
	public int getCount() {
		return connectionData.size();
	}

	@Override
	public Object getItem(int position) {
		return connectionData.get(position);
	}
	public void clearData() {
		connectionData.clear();	
	}
	public long getItemId(int position) {
		return position;
	}

	public synchronized void updateData(ArrayList<BoringConnection> featuredData) {
		this.connectionData = featuredData;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_view_row_item, parent, false);
			StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
			StrictMode.setVmPolicy(builder.build());
			holder = new ViewHolder();

			holder.mFileAddress = (TextView) convertView.findViewById(R.id.file_address);
			holder.mFileMobileno = (TextView) convertView.findViewById(R.id.file_mobileno);
			holder.mFileNumber = (TextView) convertView.findViewById(R.id.file_number);
			//holder.mStatus = (TextView) convertView.findViewById(R.id.file_status);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		BoringConnection bConn = connectionData.get(position);
		holder.mFileAddress.setText(bConn.getAddress()+"SanctionedPipe:"+bConn.getSanctionedPipe());
		holder.mFileMobileno.setText(bConn.getStatus());
		System.out.println("********************bConn.getStatus()*********:"+bConn.getStatus());
		System.out.println("the size of pipe"+bConn.getSanctionedPipe());
		if(bConn.getStatus()==null)
		{
			holder.mFileAddress.setTextColor(Color.BLUE);
			holder.mFileNumber.setTextColor(Color.BLUE);
			holder.mFileMobileno.setText("Failed");
			holder.mFileMobileno.setTextColor(Color.BLUE);
		}
		try
		{
		if(bConn.getStatus().contains("Pending") || bConn.isBuildingDetailsReceived())
		{
		     
			holder.mFileAddress.setTextColor(Color.parseColor("#008000"));
			holder.mFileNumber.setTextColor(Color.parseColor("#008000"));
		holder.mFileMobileno.setTextColor(Color.parseColor("#008000"));
		
		}
		if(bConn.getStatus().contains("New"))
		{
		
			holder.mFileAddress.setTextColor(Color.RED);
			holder.mFileNumber.setTextColor(Color.RED);
		holder.mFileMobileno.setTextColor(Color.RED);
		}
		if(bConn.getStatus().contains("Remarks Captured"))
		{
		
			holder.mFileAddress.setTextColor(Color.BLACK);
			holder.mFileNumber.setTextColor(Color.BLACK);
		    holder.mFileMobileno.setTextColor(Color.BLACK);
		}
		}
		catch(Exception e)
		{
			
		System.out.println("the expextion raised"+e);	
		}
		holder.mFileNumber.setText(bConn.getFileNo());
		//holder.mStatus.setText(bConn.getStatus());
		return convertView;
	}


	class ViewHolder {
		private TextView mFileAddress;
		private TextView mFileMobileno;
		private TextView mFileNumber;
		private TextView mStatus;
	}

}
