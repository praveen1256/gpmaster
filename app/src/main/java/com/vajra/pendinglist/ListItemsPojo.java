package com.vajra.pendinglist;

//another class to handle item's id and name
public class ListItemsPojo {
    
    public String imageName;
    
    public String file_no;
    
    public String file_date;

    public String file_address;
    
	public ListItemsPojo( String file_no,
			String file_date, String file_address) {
		super();
		this.file_no = file_no;
		this.file_date = file_date;
		this.file_address = file_address;
	}

    

}
