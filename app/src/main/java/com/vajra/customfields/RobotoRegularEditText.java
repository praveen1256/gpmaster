package com.vajra.customfields;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class RobotoRegularEditText extends EditText {


    private Typeface font = null;

    public RobotoRegularEditText(Context context) {
        super(context);
        if (!isInEditMode())
            init(context);
    }

    public RobotoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context);
    }

    public RobotoRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context);
    }

    private void init(Context context) {
        font = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-Regular_1.ttf");
        this.setTypeface(font);
    }


}
