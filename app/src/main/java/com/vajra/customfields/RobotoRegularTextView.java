package com.vajra.customfields;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoRegularTextView extends TextView {

    private Typeface mTypeface = null;

    public RobotoRegularTextView(Context context) {
        super(context);
        if (!isInEditMode())
            init(context);
    }

    public RobotoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context);
    }

    public RobotoRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context);
    }

    private void init(Context context) {
        mTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular_1.ttf");
        this.setTypeface(mTypeface);
    }

}
