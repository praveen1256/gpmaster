package com.vajra.service.model;

import java.io.Serializable;

/**
 * Created by sunil on 20/4/15.
 */
public class BoringConnection implements Serializable {

	private String fileNo;
    private String address;
    private String mobileNumber;
    private String applicationType;
    private String name;
    private String sanctionedPipe;
    private boolean isMeterMandatory;
    private boolean isBoringDetailsReceived;
    private boolean isBuildingDetailsReceived;
    private boolean isCustomerFeedBackDetailsReceived;
    private boolean isDisConnectionDetailsReceived;
    private boolean isGbAcceptedForCanGeneration;
    private boolean isGbAcceptedForGbBillGeneration;
    private boolean isMeterDetailsReceived;
    private String connectionType;
	 private String GBAllotmentDate;
	 private String status;
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGBAllotmentDate() {
		return GBAllotmentDate;
	}

	public void setGBAllotmentDate(String gBAllotmentDate) {
		GBAllotmentDate = gBAllotmentDate;
	}

	public BoringConnection() {
    }

	public String getFileNo() {
		return fileNo;
	}

	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSanctionedPipe() {
		return sanctionedPipe;
	}

	public void setSanctionedPipe(String sanctionedPipe) {
		this.sanctionedPipe = sanctionedPipe;
	}

	public boolean isMeterMandatory() {
		return isMeterMandatory;
	}

	public void setMeterMandatory(boolean isMeterMandatory) {
		this.isMeterMandatory = isMeterMandatory;
	}
	public boolean isBoringDetailsReceived() {
		return isBoringDetailsReceived;
	}

	public void setBoringDetailsReceived(boolean isBoringDetailsReceived) {
		this.isBoringDetailsReceived = isBoringDetailsReceived;
	}

	public boolean isBuildingDetailsReceived() {
		return isBuildingDetailsReceived;
	}

	public void setBuildingDetailsReceived(boolean isBuildingDetailsReceived) {
		this.isBuildingDetailsReceived = isBuildingDetailsReceived;
	}

	public boolean isCustomerFeedBackDetailsReceived() {
		return isCustomerFeedBackDetailsReceived;
	}

	public void setCustomerFeedBackDetailsReceived(
			boolean isCustomerFeedBackDetailsReceived) {
		this.isCustomerFeedBackDetailsReceived = isCustomerFeedBackDetailsReceived;
	}

	
	public boolean isDisConnectionDetailsReceived() {
		return isDisConnectionDetailsReceived;
	}

	public void setDisConnectionDetailsReceived(
			boolean isDisConnectionDetailsReceived) {
		this.isDisConnectionDetailsReceived = isDisConnectionDetailsReceived;
	}

	public boolean isGbAcceptedForCanGeneration() {
		return isGbAcceptedForCanGeneration;
	}

	public void setGbAcceptedForCanGeneration(boolean isGbAcceptedForCanGeneration) {
		this.isGbAcceptedForCanGeneration = isGbAcceptedForCanGeneration;
	}

	public boolean isGbAcceptedForGbBillGeneration() {
		return isGbAcceptedForGbBillGeneration;
	}

	public void setGbAcceptedForGbBillGeneration(
			boolean isGbAcceptedForGbBillGeneration) {
		this.isGbAcceptedForGbBillGeneration = isGbAcceptedForGbBillGeneration;
	}

	public boolean isMeterDetailsReceived() {
		return isMeterDetailsReceived;
	}

	public void setMeterDetailsReceived(boolean isMeterDetailsReceived) {
		this.isMeterDetailsReceived = isMeterDetailsReceived;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

}
