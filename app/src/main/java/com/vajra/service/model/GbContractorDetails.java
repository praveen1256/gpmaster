package com.vajra.service.model;

import java.io.Serializable;

public class GbContractorDetails implements Serializable{

	private String gbName;
	private String gbMobileNumber;
    private String gbCode;
    
    
	public GbContractorDetails() {
		super();
	}
	public GbContractorDetails(String gbName, String gbMobileNumber,
			String gbCode) {
		super();
		this.gbName = gbName;
		this.gbMobileNumber = gbMobileNumber;
		this.gbCode = gbCode;
	}
	public String getGbName() {
		return gbName;
	}
	public void setGbName(String gbName) {
		this.gbName = gbName;
	}
	public String getGbMobileNumber() {
		return gbMobileNumber;
	}
	public void setGbMobileNumber(String gbMobileNumber) {
		this.gbMobileNumber = gbMobileNumber;
	}
	public String getGbCode() {
		return gbCode;
	}
	public void setGbCode(String gbCode) {
		this.gbCode = gbCode;
	}
    
    
    
}
