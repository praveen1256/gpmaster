package com.vajra.service;

public interface UrlConstants {

    String SOAP_ACTION = "http://tempuri.org/IProcreateConnInfoTransferUC_V1/";
    String KEEP_LIVE_URL = "erp.hyderabadwater.gov.in";
//    String KEEP_LIVE_URL = "test.hyderabadwater.gov.in";
//    String KEEP_LIVE_URL = "test3.hyderabadwater.gov.in";
    String KEEP_LIVE_POST_URl = "/CTL/ERP/EIF/CommonService/IL/IProcreateConnInfoTransferUC_V1?wsdl";
    String NAMESPACE = "http://tempuri.org/";
    String GETGBPENDINGFILESINFO = "GetGBPendingFilesInfo";
    String SMS_URL = "http://bms.hyderabadwater.gov.in/val/mainservice.asmx";
    String APP_VERSION = "V5";
    String FileNo = "FileNo";
    String GBNo = "GBNo";
    String DateOfConnection = "DateOfConnection";
    String LengthOfConnection = "LengthOfConnection";
    String MeterNo = "MeterNo";
    String MeterSize = "MeterSize";
    String MeterInitialReading = "MeterInitialReading";
    String MeterMake = "MeterMake";
    String CustomerRemarks = "CustomerRemarks";
    String CustomerStarRating = "CustomerStarRating";
    String GRIEV = "griev";
    String POSTGRIEVANCE = "PostGrievance";
    String MeterLatitudeDegrees = "MeterLatitudeDegrees";
    String MeterLatitudeMinutes = "MeterLatitudeMinutes";
    String MeterLatitudeSeconds = "MeterLatitudeSeconds";
    String MeterLatitudeMilliSeconds = "MeterLatitudeMilliSeconds";

    String MeterLongitudeDegrees = "MeterLongitudeDegrees";
    String MeterLongitudeMinutes = "MeterLongitudeMinutes";
    String MeterLongitudeSeconds = "MeterLongitudeSeconds";
    String MeterLongitudeMilliSeconds = "MeterLongitudeMilliSeconds";

    String BoringLatitudeDegrees = "BoringLatitudeDegrees";
    String BoringLatitudeMinutes = "BoringLatitudeMinutes";
    String BoringLatitudeSeconds = "BoringLatitudeSeconds";
    String BoringLatitudeMilliSeconds = "BoringLatitudeMilliSeconds";

    String MeterPhoto = "MeterPhoto";
    String BuildingPhoto = "BuildingPhoto";
    String BoringPointPhoto = "BoringPointPhoto";

}
