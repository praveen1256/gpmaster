package com.vajra.service;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.KeepAliveHttpsTransportSE;

import android.util.Log;

import com.vajra.utils.AppConstants;

public class GbPostPendingDataToServer {
    public static String invokePendingFiles(String xmlData, String METHODNAME) {
        String resTxt = null;
//		allowAllSSL();
        // Create request
        SoapObject grievence = new SoapObject(UrlConstants.NAMESPACE, METHODNAME);
        // Property which holds input parameters
        PropertyInfo celsiusPI = new PropertyInfo();
        // Set Name
        celsiusPI.setName("xmlData");
        // Set Value
        celsiusPI.setValue(xmlData);
        // Set dataType
        celsiusPI.setType(String.class);
        // Add the property to request object
        grievence.addProperty(celsiusPI);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(grievence);
        //		envelope.setAddAdornments(false);
        //			envelope.addMapping(NAMESPACE, "griev", new Griev().getClass());

        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);
        try {
            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + METHODNAME, envelope);
            //	Object response = envelope.getResponse();
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            resTxt = response.toString();
            Log.v("resTxt", resTxt);

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }
        return resTxt;
    }

    public static String GetVersioncontrol(String METHODNAME) {
        String resTxt = null;
//		allowAllSSL();
        // Create request
        SoapObject grievence = new SoapObject(UrlConstants.NAMESPACE, METHODNAME);
        System.out.println("version method name:" + METHODNAME);
        // Property which holds input parameters
		/*	PropertyInfo celsiusPI = new PropertyInfo();
			celsiusPI.setName("xmlData");
			// Set dataType
			celsiusPI.setType(String.class);
			grievence.addProperty(celsiusPI);*/
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(grievence);
        //		envelope.setAddAdornments(false);
        //			envelope.addMapping(NAMESPACE, "griev", new Griev().getClass());

        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);

        try {

            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + METHODNAME, envelope);

            //	Object response = envelope.getResponse();
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

            resTxt = response.toString();

            Log.v("resTxt", resTxt);

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }

        return resTxt;
    }


    public static String GetSystemDate(String METHODNAME) {
        String resTxt = null;
//		allowAllSSL();
        // Create request
        SoapObject grievence = new SoapObject(UrlConstants.NAMESPACE, METHODNAME);
        System.out.println("version method name:" + METHODNAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(grievence);
        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);
        try {
            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + METHODNAME, envelope);
            //	Object response = envelope.getResponse();
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            resTxt = response.toString();
            Log.v("resTxt", resTxt);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }
        return resTxt;
    }


    public static String invokeGbFileNo(String fileNo, String GBcode, String METHODNAME) {
        String resTxt = null;
//		allowAllSSL();
        // Create request
        System.out.println("@@@ The values of server" + "fileNo :" + fileNo + "GBcode : " + GBcode + "method name :" + METHODNAME);
        SoapObject grievence = new SoapObject(UrlConstants.NAMESPACE, METHODNAME);
        // Property which holds input parameters
        PropertyInfo celsiusPIFileNo = new PropertyInfo();
        // Set Name
        celsiusPIFileNo.setName("fileNo");
        // Set Value
        celsiusPIFileNo.setValue(fileNo);
        celsiusPIFileNo.setType(String.class);
        PropertyInfo celsiusGBNo = new PropertyInfo();
        celsiusGBNo.setName("gbNo");
        // Set Value
        celsiusGBNo.setValue(GBcode);
        // Set dataType0
        celsiusGBNo.setType(String.class);
        // Add the properties to request object
        grievence.addProperty(celsiusPIFileNo);
        grievence.addProperty(celsiusGBNo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(grievence);
        //		envelope.setAddAdornments(false);
        //			envelope.addMapping(NAMESPACE, "griev", new Griev().getClass());
        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);
        try {
            System.out.println("@@@@---->SOAP ACTION : " + UrlConstants.SOAP_ACTION + METHODNAME + ", envelop FILE : " + fileNo + ", GB No: " + GBcode);
            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + METHODNAME, envelope);
            //	Object response = envelope.getResponse();
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            resTxt = response.toString();
            Log.v("resTxt", resTxt);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }
        return resTxt;
    }
    public static SoapObject getServersideStatus(String fileNo, String METHODNAME) {
        String resTxt = null;
        SoapObject root = null;
//		allowAllSSL();
        // Create request
        System.out.println("@@@ The values of server" + "fileNo :" + fileNo + "GBcode : "  + "method name :" + METHODNAME);
        SoapObject grievence = new SoapObject(UrlConstants.NAMESPACE, METHODNAME);
        // Property which holds input parameters
        grievence.addProperty("fileNo", fileNo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(grievence);
        //		envelope.setAddAdornments(false);
        //			envelope.addMapping(NAMESPACE, "griev", new Griev().getClass());
        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);
        try {
            System.out.println("@@@@---->SOAP ACTION : " + UrlConstants.SOAP_ACTION + METHODNAME + ", envelop FILE : " + fileNo );
            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + METHODNAME, envelope);
            //	Object response = envelope.getResponse();
//            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            SoapObject response= (SoapObject) envelope.getResponse();
            SoapObject soapObjectResultRoot = (SoapObject) envelope.bodyIn;

            Log.e(">>>>>>>>>>>>>>>>>>", "Root elemtn body: "
                    + soapObjectResultRoot.toString());

            root = (SoapObject) soapObjectResultRoot.getProperty(0);

            resTxt = response.toString();
            Log.v("serversidetxt", resTxt);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }
        return root;
    }

}
