package com.vajra.service;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

import com.hmwssb.OTPSet;



public class Sms_service  {

	private static String NAMESPACE = "http://tempuri.org/";
	//	private static String URL = "http://bms.hyderabadwater.gov.in/val/mainservice.asmx";
	private static String SOAP_ACTION = "http://tempuri.org/";


	public static String pullSmsFromServer(String mobileno, String serialid,String webMethName) {

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);

		// Property which holds input parameters
		PropertyInfo mobileProperty = new PropertyInfo();
		// Set Name
		mobileProperty.setName("mobileno");
		// Set Value
		mobileProperty.setValue(mobileno);
		// Set dataType
		mobileProperty.setType(String.class);
		// Add the property to request object
		request.addProperty(mobileProperty);

		PropertyInfo smsProperty=new PropertyInfo();
		smsProperty.setName("serialid");
		smsProperty.setValue(serialid);
		//SharedPreferenceUtils.savePreference(context, IPreferencesConstants.OTP, serialid);
		smsProperty.setType(String.class);
		request.addProperty(smsProperty);


		Log.i("root", "mobileno :"+ mobileno);
		Log.i("root", "serialid :"+ serialid); 
		String serialotp=serialid;
		Log.i("root", "serialotp :"+ serialotp); 
		OTPSet.setOtp(serialotp);

		Log.i("root", " OTPSet.getOtp() :"+OTPSet.getOtp()); 

		// SharedPreferenceUtils.savePreference(context, IPreferencesConstants.OTP, serialotp);
		// Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		// Set output SOAP object
		envelope.setOutputSoapObject(request);
		// Create HTTP call object
		HttpTransportSE androidHttpTransport = new HttpTransportSE(UrlConstants.SMS_URL,60000);

		try {
			// Invole web service
			androidHttpTransport.call(SOAP_ACTION+webMethName,envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();


			// Assign it to fahren static variable
			resTxt = response.toString();

		} catch (Exception e) {
			e.printStackTrace();
			resTxt = "Error occured";
		} 

		return resTxt;
	}
}
