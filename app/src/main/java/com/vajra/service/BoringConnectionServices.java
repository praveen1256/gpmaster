package com.vajra.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.KeepAliveHttpsTransportSE;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.vajra.hmwsdatabase.Transactions;
import com.vajra.service.model.BoringConnection;
import com.vajra.service.model.GbContractorDetails;
import com.vajra.utils.AppConstants;
import com.vajra.utils.AppConstants.IPreferencesConstants;
import com.vajra.utils.AppUtils;
import com.vajra.utils.NetworkUtils;
import com.vajra.utils.SharedPreferenceUtils;

public class BoringConnectionServices extends SoapConnectionManager implements AppConstants, AppConstants.ISoapConstants {


    private ArrayList<BoringConnection> mEnhancementConnectionDetails = new ArrayList<BoringConnection>();
    private ArrayList<BoringConnection> mNewConnectionDetails = new ArrayList<BoringConnection>();

    private GbContractorDetails gbContractorDetails = new GbContractorDetails();
    private Context mContext;

    public BoringConnectionServices(Context context) {
        super(context);
        mContext = context;
    }

    public GbContractorDetails getGbContractorDetails() {
        return gbContractorDetails;
    }

    public void setGbContractorDetails(GbContractorDetails gbContractorDetails) {
        this.gbContractorDetails = gbContractorDetails;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<BoringConnection> getmEnhancementConnectionDetails() {
        return mEnhancementConnectionDetails;
    }

    public void setmEnhancementConnectionDetails(
            ArrayList<BoringConnection> mEnhancementConnectionDetails) {
        this.mEnhancementConnectionDetails = mEnhancementConnectionDetails;
    }

    public ArrayList<BoringConnection> getmNewConnectionDetails() {
        return mNewConnectionDetails;
    }

    public void setmNewConnectionDetails(
            ArrayList<BoringConnection> mNewConnectionDetails) {
        this.mNewConnectionDetails = mNewConnectionDetails;
    }

    //gb details
    public void getGbContractorDetails(String gbNo, String webMethodName, final OperationCompleteListener callback) {

        getJsonResponseAsync = new GetGbContractor(gbContractorDetails);
        getJsonResponseAsync.execute(gbNo, webMethodName, callback);
    }

    //gb details
    public void getConnectionDetails(String gbNo, String webMethodName, final OperationCompleteListener callback) {
        Log.i("the getConnect", " in getConnectionDetails method");
        getJsonResponseAsync = new GetSoapResp(mEnhancementConnectionDetails, mNewConnectionDetails);
        getJsonResponseAsync.execute(gbNo, webMethodName, callback);
    }
}

class SoapConnectionManager implements AppConstants.ISoapConstants {
    private Context context;
    private int SUCCESS = 0;
    private int ERROR = 1;
    private Transactions transobj;
    public AsyncTask<Object, Integer, Integer> getJsonResponseAsync = null;


    public SoapConnectionManager(Context c) {
        context = c;
    }

    //sudhir
    String GBCode;

    //
    protected class GetGbContractor extends AsyncTask<Object, Integer, Integer> {
        OperationCompleteListener callback;
        GbContractorDetails gbContractorDetails;


        public GetGbContractor(GbContractorDetails gbContractorDetails) {
            this.gbContractorDetails = gbContractorDetails;


        }

        @Override
        protected void onPostExecute(Integer result) {
            //			allowAllSSL();

            if (result == SUCCESS) {
                try {

                    if (callback != null) {
                        callback.operationComplete(true, 1, "");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    callback.operationComplete(true, 2, "");
                }
            } else if (result == ERROR) {

                Toast.makeText(context, "Invalid GB Code", Toast.LENGTH_SHORT).show();

                Intent i = new Intent();
                i.setClassName("com.hmwssb", "com.hmwssb.OTPActivity");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }

        @Override
        protected Integer doInBackground(Object... params) {
            callback = (OperationCompleteListener) params[2];

            try {
                if (NetworkUtils.isNetworkAvailable(context)) {

                    //TODO for sending otp as sms
                    //					Sms_service.pullSmsFromServer(WebService.mGbContractorDetails.get(KEY_MOBILENUMBER), String.valueOf(uniqueCode), "SENDTESTSMS");

//					allowAllSSL();
                    SoapObject request = new SoapObject(NAMESPACE, (String) params[1]);
                    PropertyInfo gbProperty = new PropertyInfo();
                    gbProperty.setName("GBNo");
                    gbProperty.setValue(params[0]);
                    gbProperty.setType(String.class);
                    request.addProperty(gbProperty);
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    // Invole web service
                    Log.i("BASE_URL + GET_GB_INFO", "response");
                    Log.i(params[0].toString(), BASE_URL + GET_GB_INFO);
                    HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);

                    androidHttpTransport.call(BASE_URL + GET_GB_INFO, envelope);

                    Object response = envelope.getResponse();
                    SoapObject category_list = (SoapObject) response;
                    Log.i("in response", "response");

//					System.out.println("******************Size"+response.toString());

                    gbContractorDetails.setGbName(category_list.getProperty("Name").toString());
                    gbContractorDetails.setGbMobileNumber(category_list.getProperty("MobileNo").toString());
                    gbContractorDetails.setGbCode(category_list.getProperty("Code").toString());
                    //transobj= new Transactions(context);
                    transobj = new Transactions();
                    try {
                        transobj.open();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    GBCode = category_list.getProperty("Code").toString();
                    ContentValues getgbinfovalues = new ContentValues();
                    getgbinfovalues.put("GBNo", category_list.getProperty("Code").toString());
                    getgbinfovalues.put("Code", category_list.getProperty("Code").toString());
                    getgbinfovalues.put("MobileNo", category_list.getProperty("MobileNo").toString());
                    getgbinfovalues.put("Name", category_list.getProperty("Name").toString());
                    getgbinfovalues.put("GetGBInfoCreateByUid", category_list.getProperty("Code").toString());
                    getgbinfovalues.put("GetGBInfoCreatedDtm", "");


                    transobj.insert_GetGBInfo(getgbinfovalues);


                    System.out.println("******************Code before objact" + "getgbinfovalues");


                    SharedPreferenceUtils.getPreference(context, IPreferencesConstants.GB_CODE, category_list.getProperty("Code").toString());

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println(dateFormat.format(date));


                    int uniqueCode = AppUtils.generateRandomDigits();

                    //					Sms_service.pullSmsFromServer(gbContractorDetails.getGbMobileNumber(), String.valueOf(uniqueCode), "SENDTESTSMS");

                    //	SharedPreferenceUtils.savePreference(context, IPreferencesConstants.UNIQUE_OTP, uniqueCode);
                    SharedPreferenceUtils.savePreference(context, IPreferencesConstants.KEY_MobileNo, category_list.getProperty("MobileNo").toString());

                    SharedPreferenceUtils.savePreferenceGBCODE(context, IPreferencesConstants.GB_CODE, category_list.getProperty("Code").toString());

                    String otp = Integer.toString(uniqueCode);

                    String SIMSerialNumber = SharedPreferenceUtils.getPreference(context, AppConstants.IPreferencesConstants.KEY_SIMSerialNumber, "");
                    System.out.println("******************SIMSerialNumber" + SIMSerialNumber);
                    Log.i("root", SIMSerialNumber);

                    //SharedPreferenceUtils.getPreferenceSimnumber(context, "SIMSerialNumber", "SIMSerialNumber");
					/*CREATE TABLE `swc_licence` (
						`swc_sno`	Integer PRIMARY KEY AUTOINCREMENT,
						`swc_mobile_no`	text NOT NULL UNIQUE,
						`swc_name`	TEXT,
						`swc_gb_code`	text NOT NULL UNIQUE,
						`swc_otp`	text NOT NULL UNIQUE,
						`swc_sim_serial_number`	text NOT NULL UNIQUE,
						`swc_reg_status`	TEXT,
						`swc_CreateByUid`	TEXT,
						`swc_CreatedDtm`	TEXT
					);*/


                    ContentValues consumablesValues = new ContentValues();

                    consumablesValues.put("swc_mobile_no", category_list.getProperty("MobileNo").toString());
                    consumablesValues.put("swc_name", category_list.getProperty("Name").toString());
                    consumablesValues.put("swc_gb_code", category_list.getProperty("Code").toString());
                    consumablesValues.put("swc_otp", "00");
                    consumablesValues.put("swc_sim_serial_number", SIMSerialNumber);
                    consumablesValues.put("swc_reg_status", "0");
                    consumablesValues.put("swc_CreateByUid", category_list.getProperty("Code").toString());
                    consumablesValues.put("swc_CreatedDtm", dateFormat.format(date));
                    long l = transobj.insert_swc_licence(consumablesValues);
                    System.out.println("the consumablesValues are " + consumablesValues);
                    System.out.println("******************** long l " + l);

                    System.out.println("******************Code" + category_list.getProperty("Code").toString());

                }
            } catch (Exception e) {
                e.printStackTrace();
                if (callback != null) {
                    callback.operationComplete(true, 2, "");
                }
                return ERROR;
            }
            return SUCCESS;
        }
    }

    protected class GetSoapResp extends AsyncTask<Object, Integer, Integer> {
        OperationCompleteListener callback;
        ArrayList<BoringConnection> mEnhancementConnectionDetails;
        ArrayList<BoringConnection> mNewConnectionDetails;
        private Transactions transobj;
        ArrayList<String> newlistoffiles = new ArrayList<String>();
        GbContractorDetails gbContractorDetails;


        public void GetGbContractor(GbContractorDetails gbContractorDetails) {
            this.gbContractorDetails = gbContractorDetails;


        }

        public GetSoapResp(ArrayList<BoringConnection> mEnhancementConnectionDetails, ArrayList<BoringConnection> mNewConnectionDetails) {
            Log.i("in soap class", "in soap method");
            this.mEnhancementConnectionDetails = mEnhancementConnectionDetails;
            this.mNewConnectionDetails = mNewConnectionDetails;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result == SUCCESS) {
                try {

                    if (callback != null) {
                        callback.operationComplete(true, 1, "");
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    callback.operationComplete(true, 2, "");

                }
            } else if (result == ERROR) {

                //Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Integer doInBackground(Object... params) {
            callback = (OperationCompleteListener) params[2];
            try {
                if (NetworkUtils.isNetworkAvailable(context)) {

                    SoapObject request = new SoapObject(NAMESPACE, (String) params[1]);
                    PropertyInfo gbProperty = new PropertyInfo();
                    gbProperty.setName("GBNo");
                    gbProperty.setValue(params[0]);
                    gbProperty.setType(String.class);
                    request.addProperty(gbProperty);
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    // Invole web service
                    Log.i(params[0].toString(), "this is GET_GB_PENDING_FILES_INFO");
                    HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);
                    androidHttpTransport.call(BASE_URL + GET_GB_PENDING_FILES_INFO, envelope);

                    SoapObject root = (SoapObject) envelope.getResponse();

                    mNewConnectionDetails.clear();
                    mEnhancementConnectionDetails.clear();
                    for (int i = 0; i < root.getPropertyCount(); i++) {

                        Object property = root.getProperty(i);
                        if (property instanceof SoapObject) {

                            SoapObject pendingFileInfoForGBMobileApp = (SoapObject) property;

                            BoringConnection boringConnection = new BoringConnection();
                            boringConnection.setAddress(pendingFileInfoForGBMobileApp.getProperty("Address").toString());

                            boringConnection.setFileNo(pendingFileInfoForGBMobileApp.getProperty("FileNo").toString());
                            boringConnection.setMobileNumber(pendingFileInfoForGBMobileApp.getProperty("MobileNo").toString());
                            boringConnection.setApplicationType(pendingFileInfoForGBMobileApp.getProperty("ApplicationType").toString());
                            boringConnection.setName(pendingFileInfoForGBMobileApp.getProperty("Name").toString());
                            boringConnection.setSanctionedPipe(pendingFileInfoForGBMobileApp.getProperty("SanctionedPipeSize").toString());
                            boringConnection.setMeterMandatory(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsMeterMandatory").toString()));
                            boringConnection.setBoringDetailsReceived(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsBoringDetailsReceived").toString()));
                            boringConnection.setBuildingDetailsReceived(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsBuildingDetailsReceived").toString()));
                            boringConnection.setCustomerFeedBackDetailsReceived(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsCustomerFeedBackDetailsReceived").toString()));
                            boringConnection.setDisConnectionDetailsReceived(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsDisconnectionDetailsReceived").toString()));
                            boringConnection.setGbAcceptedForCanGeneration(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsGBAcceptedForCANGeneration").toString()));
                            boringConnection.setGbAcceptedForGbBillGeneration(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsGBAcceptedForGBBillGeneration").toString()));
                            boringConnection.setMeterDetailsReceived(Boolean.parseBoolean(pendingFileInfoForGBMobileApp.getProperty("IsMeterDetailsReceived").toString()));
                            boringConnection.setGBAllotmentDate(pendingFileInfoForGBMobileApp.getProperty("GBAllotmentDate").toString());
                            newlistoffiles.add(pendingFileInfoForGBMobileApp.getProperty("FileNo").toString());
                            String filenumber = "null";
                            try {
                                String fileNo = pendingFileInfoForGBMobileApp.getProperty("FileNo").toString();
                                //sudhir
                                //************************insert code for pending files************************************
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                String currentDateandTime = sdf.format(new Date());
                                //transobj= new Transactions(context);
                                transobj = new Transactions();
                                try {
                                    transobj.open();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ArrayList<String> filelist = transobj.getfilenumber();
                                Log.i("", "******** serching - " + fileNo + " in the DB List :" + filelist + ", Result -" + filelist.contains(fileNo));
                                //new changes

                                if (filelist.contains(fileNo)) {
                                    ContentValues GetPendingFilesInfo = new ContentValues();
                                    if (!boringConnection.isBuildingDetailsReceived() && boringConnection.isGbAcceptedForCanGeneration()) {
                                        GetPendingFilesInfo.put("GBStatus", "Pending");
                                    }
                                    transobj.update_GbPendingFileInfo(GetPendingFilesInfo, filenumber);

                                }
                                if (!filelist.contains(fileNo)) {
                                    System.out.println("LOOP 1===>");
                                    ContentValues GetPendingFilesInfo = new ContentValues();
                                    GetPendingFilesInfo.put("GBNo", GBCode);
                                    GetPendingFilesInfo.put("Address", pendingFileInfoForGBMobileApp.getProperty("Address").toString());
                                    GetPendingFilesInfo.put("ApplicationType", boringConnection.getApplicationType());
                                    GetPendingFilesInfo.put("FileNo", pendingFileInfoForGBMobileApp.getProperty("FileNo").toString());
                                    GetPendingFilesInfo.put("IsBoringDetailsReceived", pendingFileInfoForGBMobileApp.getProperty("IsBoringDetailsReceived").toString());
                                    GetPendingFilesInfo.put("IsBuildingDetailsReceived", pendingFileInfoForGBMobileApp.getProperty("IsBuildingDetailsReceived").toString());
                                    GetPendingFilesInfo.put("IsCustomerFeedBackDetailsReceived", pendingFileInfoForGBMobileApp.getProperty("IsCustomerFeedBackDetailsReceived").toString());
                                    GetPendingFilesInfo.put("IsDisconnectionDetailsReceived", pendingFileInfoForGBMobileApp.getProperty("IsDisconnectionDetailsReceived").toString());
                                    GetPendingFilesInfo.put("IsGBAcceptedForCANGeneration", pendingFileInfoForGBMobileApp.getProperty("IsGBAcceptedForCANGeneration").toString());
                                    GetPendingFilesInfo.put("IsGBAcceptedForGBBillGeneration", pendingFileInfoForGBMobileApp.getProperty("IsGBAcceptedForGBBillGeneration").toString());
                                    GetPendingFilesInfo.put("IsMeterDetailsReceived", pendingFileInfoForGBMobileApp.getProperty("IsMeterDetailsReceived").toString());
                                    GetPendingFilesInfo.put("IsMeterMandatory", pendingFileInfoForGBMobileApp.getProperty("IsMeterMandatory").toString());
                                    GetPendingFilesInfo.put("MobileNo", pendingFileInfoForGBMobileApp.getProperty("MobileNo").toString());
                                    GetPendingFilesInfo.put("Name", pendingFileInfoForGBMobileApp.getProperty("Name").toString());
                                    GetPendingFilesInfo.put("SanctionedPipeSize", pendingFileInfoForGBMobileApp.getProperty("SanctionedPipeSize").toString());
                                    GetPendingFilesInfo.put("GetGBPendingFilesInfoCreateByUid", GBCode);
                                    GetPendingFilesInfo.put("GBAllotmentDate", pendingFileInfoForGBMobileApp.getProperty("GBAllotmentDate").toString());
                                    if (!boringConnection.isBuildingDetailsReceived() && boringConnection.isGbAcceptedForCanGeneration()) {
                                        GetPendingFilesInfo.put("GBStatus", "Pending");
                                    } else {
                                        GetPendingFilesInfo.put("GBStatus", "New");
                                    }

                                    GetPendingFilesInfo.put("GetGBPendingFilesInfoCreatedDtm", currentDateandTime);
                                    transobj.insert_GetGBPendingFilesInfo(GetPendingFilesInfo);

                                }

                                System.out.println("  Database filelist SIZE====>" + filelist.size());

                                System.out.println(" new Webservice newlistoffiles  list SIZE====>" + newlistoffiles.size());


                            } catch (SQLiteConstraintException e) {
                                Log.i("SQLiteConstraintExn", e.toString());
                            }
							/*if (pendingFileInfoForGBMobileApp.getProperty("ApplicationType").toString().contains("N")) {
								boringConnection.setConnectionType(AppConstants.NEW_CONNECTION);
								mNewConnectionDetails.add(boringConnection);
								//									Log.i("root", "this is new conn :" + map);
							}else if (pendingFileInfoForGBMobileApp.getProperty("ApplicationType").toString().contains("E")) {
								boringConnection.setConnectionType(AppConstants.ENHANCED_CONNECTION);
								mEnhancementConnectionDetails.add(boringConnection);

							}*/

                        }

                    }

                    /*******************************delete functionality for deassign pending list*****************
                     *
                     */
                    try {


                        ArrayList<String> newlocalfilelist = transobj.getfilenumber();

                        String addnotinlistfile = "";

                        System.out.println("remainig newlocalfilelist BEFORE list====>" + newlocalfilelist.size());
                        for (String strnewFileNO : newlistoffiles) {
                            newlocalfilelist.remove(strnewFileNO);

                        }
                        System.out.println("remainig newlocalfilelist AFTER list====>" + addnotinlistfile.length());
                        for (String strvalue : newlocalfilelist) {
                            if (!strvalue.equals("Select One")) {
                                if (addnotinlistfile.length() <= 1) {
                                    addnotinlistfile = strvalue;
                                } else {
                                    addnotinlistfile = addnotinlistfile + "," + strvalue;
                                }
                            }
                        }
                        System.out.println("remainig newlocalfilelist AFTER list====>" + newlocalfilelist.size());
                        if (!addnotinlistfile.isEmpty()) {
                            transobj.delete_unasssigendtransaction(addnotinlistfile);
                        } else {
                            System.out.println("UN MATCHED FILE LIST LENGHT IS remainig file list====>" + addnotinlistfile + "---addnotinlistfile.isEmpty()---" + addnotinlistfile.isEmpty());
                        }
                        System.out.println("remainig file list====>" + addnotinlistfile);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
                if (callback != null) {
                    callback.operationComplete(true, 2, "");
                }
                return ERROR;
            }
            return SUCCESS;
        }

    }

}
