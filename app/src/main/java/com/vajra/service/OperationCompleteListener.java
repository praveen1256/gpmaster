package com.vajra.service;

/**
 * Created by sunil on 23/4/15.
 */
public interface OperationCompleteListener {
    public void operationComplete(boolean status, int code, String message);
}
