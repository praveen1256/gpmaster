package com.vajra.service;

import android.app.Application;

/**
 * Created by sriram on 16/4/15.
 */
public class HmwssbApplication extends Application {
    private static HmwssbApplication application = null;
    private BoringConnectionServices mBoringConnectionService;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public BoringConnectionServices getBoringServices() {
        if (mBoringConnectionService == null) {
        	mBoringConnectionService = new BoringConnectionServices(this);
        }
        return mBoringConnectionService;
    }

}
