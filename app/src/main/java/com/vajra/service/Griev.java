package com.vajra.service;

import java.util.Hashtable;
import java.util.Vector;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class Griev extends Vector<String>  implements KvmSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Code;
	private String Result;


	public Griev() {}

	public String getCode() {
		return Code;
	}

	public void setCode(String Code) {
	this.Code=Code;
	}

	public String getResult() {
		return Result;
	}

	public void setResult(String Result) {
		this.Result=Result;
	}

	public Griev(String Code, String Result) {
		this.Code = Code;
		this.Result = Result;
	}

	@Override
	public Object getProperty(int arg0) {
		switch(arg0) {
		case 0:
			return Code;
		case 1:
			return Result;
		}
		return null;
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo propertyInfo) {
		switch(index){
		case 0:
			propertyInfo.name = "Code";
			propertyInfo.type = PropertyInfo.STRING_CLASS;
			break;
		case 1:
			propertyInfo.name = "Result";
			propertyInfo.type = PropertyInfo.STRING_CLASS;
			break;
		default:
			break;
		}
	}

	@Override
	public void setProperty(int index, Object value) {
		switch(index) {
		case 0:
			this.Code = value.toString();
			break;
		case 1:
			this.Result =  value.toString();
			break;
		default:
			break;
		}
	}

}
