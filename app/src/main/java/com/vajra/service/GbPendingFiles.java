package com.vajra.service;

import java.util.ArrayList;
import java.util.HashMap;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.KeepAliveHttpsTransportSE;

import android.util.Log;

import com.hmwssb.SuperActivity;
import com.vajra.utils.AppConstants;

public class GbPendingFiles extends SuperActivity {

    private static String NAMESPACE = "http://tempuri.org/";
    private static String SOAP_ACTION = "http://tempuri.org/IVajraInfraConnectionInfoTransferUC/GetGBPendingFilesInfo";

    static final String KEY_Address = "id_address";
    static final String KEY_MobileNo = "id_MpbileNo";
    static final String KEY_NAME = "id_Name";
    static final String KEY_SanctionedPipe = "id_SanctionedPipeSize";
    static final String KEY_FILENO = "id_Fileno";


    public static ArrayList<HashMap<String, String>> invokePendingFiles(String GBNo, String webMethName) {
        ArrayList<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();
        String resTxt = null;

//		allowAllSSL();

        // Create request
        SoapObject request = new SoapObject(UrlConstants.NAMESPACE, webMethName);
        // Property which holds input parameters
        PropertyInfo celsiusPI = new PropertyInfo();
        // Set Name
        celsiusPI.setName("GBNo");
        // Set Value
        celsiusPI.setValue(GBNo);
        // Set dataType
        celsiusPI.setType(String.class);
        // Add the property to request object
        request.addProperty(celsiusPI);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        //		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.LIVE_PORT_NUMBER, "/CTL/ERP/EIF/CommonService/IL/IVajraInfraConnectionInfoTransferUC?wsdl", 60000);
//		HttpsTransportSE androidHttpTransport = new KeepAliveHttpsTransportSE(UrlConstants.KEEP_LIVE_URL, AppConstants.TEST_PORT_NUMBER, UrlConstants.KEEP_LIVE_POST_URl, 60000);


        try {
            // Invole web service
            androidHttpTransport.call(UrlConstants.SOAP_ACTION + UrlConstants.GETGBPENDINGFILESINFO, envelope);


            //			androidHttpTransport.call(SOAP_ACTION, envelope);
            // Get the response


//			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            Object response = envelope.getResponse();


            SoapObject root = (SoapObject) ((SoapObject) response).getProperty("PendingFileInfoForGBMobileApp");
//			           SoapObject s_deals = (SoapObject) root.getProperty("PendingFileInfoForGBMobileApp");


//			           System.out.println("********Count : "+ s_deals.getPropertyCount());

            for (int i = 0; i < root.getPropertyCount(); i++) {
//			               Object property = root.getProperty(i);
                if (root instanceof SoapObject) {

                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(KEY_Address, root.getProperty("Address").toString());
                    map.put(KEY_FILENO, root.getProperty("FileNo").toString());
                    map.put(KEY_MobileNo, root.getProperty("MobileNo").toString());
                    map.put(KEY_NAME, root.getProperty("Name").toString());
                    map.put(KEY_SanctionedPipe, root.getProperty("SanctionedPipeSize").toString());

//			                   SoapObject category_list = (SoapObject) property;
			                  /* String id_Address = root.getProperty("Address").toString();
			                   String id_FileNo = root.getProperty("FileNo").toString();
			                   String id_MobileNo = root.getProperty("MobileNo").toString();
			                   String id_Name = root.getProperty("Name").toString();
			                   String id_SanctionedPipeSize = root.getProperty("SanctionedPipeSize").toString();*/
			                /*   stringBuilder.append
			                   (
			                        "Row value of: " +(i+1)+"\n"+
			                        "Category: "+id_Address+"\n"+
			                        "Category URL: "+id_FileNo+"\n"+
			                        "Category_Icon: "+id_MobileNo+"\n"+
			                        "Category_Count: "+id_Name+"\n"+
			                        "SuperTag: "+id_SanctionedPipeSize+"\n"+
			                        "******************************"
			                   );          */
                    menuItems.add(map);
                }
            }
            // Assign it to fahren static variable
            resTxt = response.toString();


        } catch (Exception e) {
            e.printStackTrace();
            Log.i(e.toString(), "this is exception");
            resTxt = "failed";
        }

        return menuItems;
    }

}
