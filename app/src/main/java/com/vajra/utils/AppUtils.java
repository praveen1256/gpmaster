//***************************************************************************************************
//***************************************************************************************************
//      Project name                    		: Alfred
//      Class Name                              : AppUtils
//      Author                                  : Opatu, Inc.
//***************************************************************************************************
//      Description: 
//***************************************************************************************************
//***************************************************************************************************

package com.vajra.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.widget.Toast;

import com.hmwssb.R;

/**
 *  
 * 
 * @author Opatu, Inc.
 */
public class AppUtils
{
	public static final int DEFAULT_SCREEN_WIDTH = 800;
	public static final int DEFAULT_SCREEN_HEIGHT = 1280;
	public static int SCREEN_WIDTH;
	public static int SCREEN_HEIGHT;

	public static String BUNDLEDATA = "BUNDLEDATA";

	private static AppUtils mStatisInstance = null;

	private AppUtils()
	{
	}



	public static AppUtils getInstance()
	{
		if (mStatisInstance == null)
		{
			mStatisInstance = new AppUtils();
		}
		return mStatisInstance;
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connection = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nInfo = null;
		if (connection != null) {
			nInfo = connection.getActiveNetworkInfo();
		}
		if (nInfo == null || !nInfo.isConnectedOrConnecting()) {
			return false;
		}

		if (nInfo == null || !nInfo.isConnected()) {
			return false;
		}
		if (nInfo != null
				&& ((nInfo.getType() == ConnectivityManager.TYPE_MOBILE) || (nInfo
						.getType() == ConnectivityManager.TYPE_WIFI))) {
			if (nInfo.getState() != NetworkInfo.State.CONNECTED
					|| nInfo.getState() == NetworkInfo.State.CONNECTING) {
				return false;
			}
		}
		return true;
	}

	public static void disp(Context context,String string) {
		// TODO Auto-generated method stub
		Toast.makeText(context, string, 3000).show();
	}
	public Bitmap getSampleBitmapFromFile(Context context, Uri bitmapFilePath, int reqWidth, int reqHeight) throws FileNotFoundException, IOException{

		InputStream input = context.getContentResolver().openInputStream(bitmapFilePath);

		// calculating image size
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(input, null, options);
		input.close();
		int scale = calculateInSampleSize(options, reqWidth, reqHeight);

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		input = context.getContentResolver().openInputStream(bitmapFilePath);
		Bitmap image=BitmapFactory.decodeStream(input, null, o2);
		input.close();
		return 	   image ;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float)height / (float)reqHeight);
			final int widthRatio = Math.round((float)width / (float)reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	public static void showToast(Context mContext, String mesgToShow) {
		Toast.makeText(mContext, mesgToShow, 3000).show();
	}

	public static int generateRandomDigits() {
		Random r = new Random( System.currentTimeMillis() );
		return (1 + r.nextInt(2)) * 10000 + r.nextInt(10000);
	}
/*	public static void SecureRandomNumber() {
		try {
			// Create a secure random number generator using the SHA1PRNG algorithm
			SecureRandom secureRandomGenerator = SecureRandom.getInstance("SHA1PRNG");
			// Get 128 random bytes
			byte[] randomBytes = new byte[128];
			secureRandomGenerator.nextBytes(randomBytes);
			// Create two secure number generators with the same seed
			int seedByteCount = 5;
			byte[] seed = secureRandomGenerator.generateSeed(seedByteCount);
			SecureRandom secureRandom1 = SecureRandom.getInstance("SHA1PRNG");
			secureRandom1.setSeed(seed);
			SecureRandom secureRandom2 = SecureRandom.getInstance("SHA1PRNG");
			secureRandom2.setSeed(seed);
		} catch (NoSuchAlgorithmException e) {
		}
	}
*/
	/**
	 * Returns an int array of size two, containing width and height
	 * @return [0]-screenWidth, [1]-screenHeight
	 */
	public int[] getScreenResolution(Context context)
	{
		// Only works above API level 13
		/*int Measuredwidth = 0;
		int Measuredheight = 0;
		Point size = new Point();
		WindowManager w = ((Activity) context).getWindowManager();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			w.getDefaultDisplay().getSize(size);

			Measuredwidth = size.x;
			Measuredheight = size.y; 
		}
		else
		{
			Display d = w.getDefaultDisplay(); 
			Measuredwidth = d.getWidth(); 
			Measuredheight = d.getHeight(); 
		}*/
		// Only works above API level 13

		int widthPixels = 0;
		int heightPixels = 0;
		Display d = ((Activity) context).getWindowManager().getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		widthPixels = metrics.widthPixels;
		heightPixels = metrics.heightPixels;
		try
		{
			// used when 17 > SDK_INT >= 14; includes window decorations (statusbar bar/menu bar)
			widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
			heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		}
		catch (Exception ignored)
		{
		}
		try
		{
			// used when SDK_INT >= 17; includes window decorations (statusbar bar/menu bar)
			Point realSize = new Point();
			Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
			widthPixels = realSize.x;
			heightPixels = realSize.y;
		}
		catch (Exception ignored)
		{
		}

		return new int [] {widthPixels, heightPixels};
	}

	public int getActionBarHeigth(Context mContext)
	{
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (mContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, mContext.getResources().getDisplayMetrics());
		}
		return actionBarHeight;
	}

	public boolean isGPSenabled(Context context)
	{
		final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
	/*
	 * sunil
	 */
	public static Bitmap getBitmap(String imageName,Context ctx) {

		try {
			Bitmap scaledBitmap = BitmapFactory.decodeResource(ctx.getResources(),getDrawableId(imageName));
			return scaledBitmap;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;


	}  

	public static int getDrawableId(String paramString)
	{
		try
		{
			int i = R.drawable.class.getField(paramString).getInt(null);
			return i;
		}
		catch (Exception localException)
		{
			throw new RuntimeException(localException);
		}

	}  


	///////
	public boolean isServiceRunning(Context mContext, String serviceName)
	{
		boolean serviceRunning = false;
		ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(50);
		Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
		while (i.hasNext())
		{
			ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo)i.next();

			if(runningServiceInfo.service.getClassName().equals(serviceName))
			{
				serviceRunning = true;
			}
		}
		return serviceRunning;
	}

	public boolean is7InchTablet(Context context)
	{
		boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
		return large;
	}

	public boolean is10InchTablet(Context context)
	{
		boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
		return xlarge;
	}
	public boolean isPhone(Context context)
	{
		boolean phone=false;

		if (is7InchTablet(context) == false && is10InchTablet(context) == false) {
			phone = true;
		}
		return phone;
	}

	public void writeToSdcard(Bitmap bitmap, String pathWithFileName)
	{
		try
		{
			File f = new File(Environment.getExternalStorageDirectory()+File.separator+"SC");
			if (!f.isDirectory())
			{
				f.mkdir();
			}

			FileOutputStream fileOutputStream = new FileOutputStream(pathWithFileName);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fileOutputStream);
			fileOutputStream.flush();
			fileOutputStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


}