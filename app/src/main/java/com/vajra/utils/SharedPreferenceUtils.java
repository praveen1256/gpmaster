package com.vajra.utils;

import java.util.Set;

import android.content.Context;

/**
 * Manages all shared preferences keys with getter and setter methods.
 *
 * @author PurpleTalk, Inc.
 */
public class SharedPreferenceUtils {
    private static String Preference_Name = "HmwssbSharedPreferences";
    private static int Preference_Mode = Context.MODE_PRIVATE;

    public static void savePreference(Context context, String key, String value) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().putString(key, value).commit();
    }

    public static String getPreference(Context context, String key, String defaultValue) {
        String toReturn = context.getSharedPreferences(Preference_Name, Preference_Mode).getString(key, defaultValue);
        if (toReturn == null) {
            toReturn = "";
        }
        return toReturn;
    }

    public static void savePreference(Context context, String key, boolean value) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().putBoolean(key, value).commit();
    }

    public static boolean getPreference(Context context, String key, boolean defaultValue) {
        return context.getSharedPreferences(Preference_Name, Preference_Mode).getBoolean(key, defaultValue);
    }

    public static void savePreference(Context context, String key, int value) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().putInt(key, value).commit();
    }

    public static int getPreference(Context context, String key, int defaultValue) {
        int toReturn;
        toReturn = context.getSharedPreferences(Preference_Name, Preference_Mode).getInt(key, defaultValue);
        return toReturn;
    }

    public static void savePreference(Context context, String key, Set<String> value) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().putStringSet(key, value).commit();
    }

    public static Set<String> getPreference(Context context, String key, Set<String> defaultValue) {
        return context.getSharedPreferences(Preference_Name, Preference_Mode).getStringSet(key, defaultValue);
    }

    public static void clear(Context context) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().clear().commit();
    }

    public static void savePreferenceGBCODE(Context context, String key, String value) {
        context.getSharedPreferences(Preference_Name, Preference_Mode).edit().putString(key, value).commit();
    }

    public static String getPreferenceGBCODE(Context context, String key, String defaultValue) {
        String toReturn = context.getSharedPreferences(Preference_Name, Preference_Mode).getString(key, defaultValue);
        if (toReturn == null) {
            toReturn = "";
        }
        return toReturn;
    }

}