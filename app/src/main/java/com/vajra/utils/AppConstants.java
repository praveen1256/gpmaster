package com.vajra.utils;

/**
 */
public interface AppConstants {

    String NO_NETWORK_AVAILABLE = "NO Network Please Connect to Internet";
    String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int LIVE_PORT_NUMBER = 91;
//   int LIVE_PORT_NUMBER = 9235;
//   int LIVE_PORT_NUMBER = 8235;

    String KEY_MOBILENUMBER = "mobileno";
    String KEY_CODE = "code";
    String KEY_GBNAME = "name";

    String KEY_Address = "id_address";
    String KEY_MobileNo = "id_MpbileNo";
    String KEY_NAME = "id_Name";
    String KEY_BORINGDATE = "id_boringDate";
    String KEY_APPLICATIONTYPE = "id_APPLICATIONTYPE";
    String KEY_SANCTIONEDPIPE = "id_SanctionedPipeSize";
    String KEY_ISMETERMANDATORY = "id_IsMeterMandatory";

    String KEY_ISGBACCEPTEDFORGBBILLGENERATION = "ISGBACCEPTEDFORGBBILLGENERATION";
    String KEY_ISGBACCEPTEDFORCANGENERATION = "ISGBACCEPTEDFORCANGENERATION";
    String KEY_ISMETERDETAILSRECEIVED = "ISMETERDETAILSRECEIVED";
    String KEY_ISDISCONNECTIONDETAILSRECEIVED = "ISDISCONNECTIONDETAILSRECEIVED";
    String KEY_ISBUILDINGDETAILSRECEIVED = "ISBUILDINGDETAILSRECEIVED";
    String KEY_ISCUSTOMERFEEDBACKDETAILSRECEIVED = "ISCUSTOMERFEEDBACKDETAILSRECEIVED";
    String KEY_ISBORINGDETAILSRECEIVED = "ISBORINGDETAILSRECEIVED";

    String KEY_TYPE = "connectionType";
    String KEY_FILENO = "id_Fileno";
    String POSITION = "position";
    String GB_DATA = "GB_DATA";
    String ERROR = "Error";
    String STRING_TRUE = "true";
    String STRING_FALSE = "false";

    String ENHANCED_CONNECTION = "E";
    String NEW_CONNECTION = "N";
    int CONECTION_REQUEST_TYPE = 0;
    int BORING_CONNECTION = 1;
    int METER_CONNECTION = 2;
    int CAN_CONNECTION = 5;
    int FINAL_SUBMIT_CONNECTION = 6;
    int other_DETAILS_CONNECTION = 3;
    int DISCONNECTION_DETAILS = 4;
    int REMARKS_DETAILS = 7;
    int CUSTOMER_BUILDING = 8;
    int CUSTOMER_FEEDBACK = 9;

    public interface IBundleConstants {
    }

    public interface ISoapConstants {
        String NAMESPACE = "http://tempuri.org/";
        //		String NAMESPACE = "http://tempuri.org/Imports";
//        String BASE_URL = "http://tempuri.org/IProcreateConnInfoTransferUC_V1/";
        String BASE_URL = "https://test3.hyderabadwater.gov.in:9235/CTL/ERP/EIF/CommonService/IL/IProcreateConnInfoTransferUC/";

        String GET_GB_INFO = "GetGBInfo";
        String GET_GB_PENDING_FILES_INFO = "GetGBPendingFilesInfo";


    }

    public interface IPreferencesConstants {
        String FILENO = "FILENO";
        String UNIQUE_OTP = "UNIQUE_OTP";
        String GB_CODE = "GB_CODE";
        String GB_NAME = "GB_NAME";
        String GB_NUMBER = "GB_NUMBER";
        String SELF_STATUS = "SELF_STATUS";
        String FIRSTTIME = "firsttime";

        String KEY_ITEM = "anyType"; // parent node
        String KEY_GBNO = "Code";
        String KEY_NAME = "Name";
        String KEY_MobileNo = "MobileNo";
        String KEY_SIMSerialNumber = "KEY_SIMSerialNumber";
    }


}
